//
//  ArchiTech_Analyze_SwiftTests.swift
//  ArchiTech Analyze SwiftTests
//
//  Created by Jakob Hain on 12/2/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import XCTest
@testable import ArchiTech_Analyze_Swift

class ArchiTech_Analyze_SwiftTests: XCTestCase {
    let codeUrl: URL = Bundle(for: ArchiTech_Analyze_SwiftTests.self).url(
        forResource: "InitialCode",
        withExtension: ""
    )!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    ///Tests the service class directly (not using XPC)
    func testDirectly() {
        let service = ArchiTech_Analyze_Swift()
        let expectation = validate(service: service)

        wait(for: [expectation], timeout: 0)
    }

    ///Tests the service via the XPC protocol
    func testXPCProtocol() {
        let connection = NSXPCConnection(serviceName: "com.jakobeha.ArchiTech-Analyze-Swift")
        connection.remoteObjectInterface = NSXPCInterface(with: ArchiTech_Analyze_SwiftProtocol.self)
        connection.resume()

        var expectation: XCTestExpectation! = nil
        let service = connection.remoteObjectProxyWithErrorHandler { error in
            XCTFail("Error setting up XPC service: \(error)")
            expectation?.fulfill()
        } as! ArchiTech_Analyze_SwiftProtocol
        expectation = validate(service: service)

        wait(for: [expectation], timeout: 300)

        connection.invalidate()
    }

    func validate(service: ArchiTech_Analyze_SwiftProtocol) -> XCTestExpectation {
        let expectation = XCTestExpectation(description: "Validate XPC service")
        service.analyze(
            url: codeUrl,
            withReply: { result in
                switch ArchiTech_Analyze_SwiftResult(result) {
                case .fail(let error):
                    XCTFail("Error analyzing code: \(error)")
                case .success(let codeData):
                    let decoder = JSONDecoder()
                    let code = try! decoder.decode(
                        Code.self,
                        from: codeData
                    )
                    XCTAssertNotEqual(
                        code,
                        Code(
                            frags: [],
                            duplicates: []
                        )
                    )
                }
                expectation.fulfill()
            }
        )
        return expectation
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
