//
//  GameViewController.swift
//  ArchiTech macOS
//
//  Created by Jakob Hain on 10/19/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Cocoa
import SpriteKit
import GameplayKit
import ArchiTech_Analyze_Swift

class GameViewController: NSViewController {
    private var underlying: GameController!

    override func viewDidLoad() {
        super.viewDidLoad()

        print("Setting up controller...")
        underlying = GameController(view: self.view as! GameView)

        print("Analyzing code...")
        analyzeCode(
            onFail: { error in
                print("Code analysis failed: ", error)
            },
            onSuccess: { codeData in
                print("Code analyis finished.")
                print("Creating initial model...")
                let model = self.initialModel(codeData: codeData)
                print("Updating controller with model...")
                DispatchQueue.main.async {
                    self.underlying.model = model
                    print("Setup finished.")
                    self.underlying.autoPlace()
                }
            }
        )
    }
    
    private func analyzeCode(onFail: @escaping (Error) -> Void, onSuccess: @escaping (Data) -> Void) {
        //Derived from https://matthewminer.com/2018/08/25/creating-an-xpc-service-in-swift.html
        let analyzeSwiftConnection = NSXPCConnection(serviceName: "com.jakobeha.ArchiTech-Analyze-Swift")
        analyzeSwiftConnection.remoteObjectInterface = NSXPCInterface(with: ArchiTech_Analyze_SwiftProtocol.self)
        analyzeSwiftConnection.resume()

        let analyzeSwiftService = analyzeSwiftConnection.remoteObjectProxyWithErrorHandler(onFail)
            as! ArchiTech_Analyze_SwiftProtocol

        analyzeSwiftService.analyze(
            url: Bundle.main.url(forResource: "InitialCode", withExtension: "")!
        ) { result in
            switch ArchiTech_Analyze_SwiftResult(result) {
            case .fail(let error):
                onFail(error)
            case .success(let codeData):
                onSuccess(codeData)
            }
        }
    }
    
    private func initialModel(codeData: Data) -> Game {
        let decoder = JSONDecoder()
        return Initial.game(
            code: try! decoder.decode(
                Code.self,
                from: codeData
            ),
            context: try! decoder.decode(
                World.Context.self,
                from: Data(contentsOf: Bundle.main.url(forResource: "Context", withExtension: "json")!)
            )
        )
    }
}
