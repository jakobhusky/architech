<?xml version="1.0" encoding="UTF-8"?>
<tileset name="TileSet" tilewidth="34" tileheight="34" tilecount="226" columns="0">
 <tileoffset x="-1" y="2"/>
 <grid orientation="isometric" width="1" height="1"/>
 <tile id="0" type="plot">
  <image width="34" height="34" source="Assets/Plots/concrete.png"/>
 </tile>
 <tile id="1" type="plot">
  <image width="34" height="34" source="Assets/Plots/sand.png"/>
 </tile>
 <tile id="2" type="plot">
  <image width="34" height="34" source="Assets/Plots/grass.png"/>
 </tile>
 <tile id="3" type="building">
  <image width="34" height="34" source="Assets/Buildings/park.png"/>
 </tile>
 <tile id="4" type="building">
  <image width="34" height="34" source="Assets/Buildings/storage.png"/>
 </tile>
 <tile id="6" type="building">
  <image width="34" height="34" source="Assets/Buildings/tallGrass.png"/>
 </tile>
 <tile id="7" type="building">
  <image width="34" height="34" source="Assets/Buildings/cargo.png"/>
 </tile>
 <tile id="8" type="building">
  <image width="34" height="34" source="Assets/Buildings/shipHouse.png"/>
 </tile>
 <tile id="9" type="building">
  <image width="34" height="34" source="Assets/Buildings/graveyard.png"/>
 </tile>
 <tile id="10" type="building">
  <image width="34" height="34" source="Assets/Buildings/sewagePlant.png"/>
 </tile>
 <tile id="11" type="building">
  <image width="34" height="34" source="Assets/Buildings/warehouse.png"/>
 </tile>
 <tile id="12" type="building">
  <image width="34" height="34" source="Assets/Buildings/factory.png"/>
 </tile>
 <tile id="13" type="building">
  <image width="34" height="34" source="Assets/Buildings/school.png"/>
 </tile>
 <tile id="14" type="building">
  <image width="34" height="34" source="Assets/Buildings/fireStation.png"/>
 </tile>
 <tile id="15" type="building">
  <image width="34" height="34" source="Assets/Buildings/hospital.png"/>
 </tile>
 <tile id="16" type="building">
  <image width="34" height="34" source="Assets/Buildings/policeStation.png"/>
 </tile>
 <tile id="17" type="building">
  <image width="34" height="34" source="Assets/Buildings/iroquois.png"/>
 </tile>
 <tile id="18" type="building">
  <image width="34" height="34" source="Assets/Buildings/worsker.png"/>
 </tile>
 <tile id="19" type="building">
  <image width="34" height="34" source="Assets/Buildings/islander.png"/>
 </tile>
 <tile id="20" type="building">
  <image width="34" height="34" source="Assets/Buildings/asian.png"/>
 </tile>
 <tile id="21" type="building">
  <image width="34" height="34" source="Assets/Buildings/european.png"/>
 </tile>
 <tile id="22" type="building">
  <image width="34" height="34" source="Assets/Buildings/anatolian.png"/>
 </tile>
 <tile id="23" type="building">
  <image width="34" height="34" source="Assets/Buildings/arabian.png"/>
 </tile>
 <tile id="24" type="building">
  <image width="34" height="34" source="Assets/Buildings/african.png"/>
 </tile>
 <tile id="25" type="road">
  <image width="34" height="34" source="Assets/Roads/roadConcrete4Way.png"/>
 </tile>
 <tile id="26" type="road">
  <image width="34" height="34" source="Assets/Roads/roadConcreteUpLeft.png"/>
 </tile>
 <tile id="27" type="road">
  <image width="34" height="34" source="Assets/Roads/roadConcreteUpRight.png"/>
 </tile>
 <tile id="28" type="road">
  <image width="34" height="34" source="Assets/Roads/roadGrass4Way.png"/>
 </tile>
 <tile id="29" type="road">
  <image width="34" height="34" source="Assets/Roads/roadGrassUpLeft.png"/>
 </tile>
 <tile id="30" type="road">
  <image width="34" height="34" source="Assets/Roads/roadGrassUpRight.png"/>
 </tile>
 <tile id="31" type="road">
  <image width="34" height="34" source="Assets/Roads/highwayUpLeft.png"/>
 </tile>
 <tile id="32" type="building">
  <image width="34" height="34" source="Assets/Buildings/forest.png"/>
 </tile>
 <tile id="33" type="plot">
  <image width="34" height="34" source="Assets/Plots/water.png"/>
 </tile>
 <tile id="34">
  <image width="34" height="34" source="Assets/Buildings/airplane.png"/>
 </tile>
 <tile id="35">
  <image width="34" height="34" source="Assets/Buildings/ship.png"/>
 </tile>
 <tile id="36" type="plot_base">
  <image width="34" height="34" source="Assets/Plots/shape.png"/>
 </tile>
 <tile id="42" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bigPole.png"/>
 </tile>
 <tile id="43" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/ambulance.png"/>
 </tile>
 <tile id="49" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/dirty.png"/>
 </tile>
 <tile id="52" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/lightPole.png"/>
 </tile>
 <tile id="53" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/log.png"/>
 </tile>
 <tile id="55" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/pole.png"/>
 </tile>
 <tile id="56" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/police.png"/>
 </tile>
 <tile id="57" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/stone.png"/>
 </tile>
 <tile id="59" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var0.png"/>
 </tile>
 <tile id="60" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var1.png"/>
 </tile>
 <tile id="61" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var2.png"/>
 </tile>
 <tile id="62" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var3.png"/>
 </tile>
 <tile id="63" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var4.png"/>
 </tile>
 <tile id="64" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var5.png"/>
 </tile>
 <tile id="65" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var6.png"/>
 </tile>
 <tile id="66" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var7.png"/>
 </tile>
 <tile id="67" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var8.png"/>
 </tile>
 <tile id="68" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var9.png"/>
 </tile>
 <tile id="69" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var10.png"/>
 </tile>
 <tile id="70" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var11.png"/>
 </tile>
 <tile id="71" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var12.png"/>
 </tile>
 <tile id="72" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var13.png"/>
 </tile>
 <tile id="73" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var14.png"/>
 </tile>
 <tile id="74" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var15.png"/>
 </tile>
 <tile id="75" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var16.png"/>
 </tile>
 <tile id="76" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var17.png"/>
 </tile>
 <tile id="77" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var18.png"/>
 </tile>
 <tile id="78" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var19.png"/>
 </tile>
 <tile id="79" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var20.png"/>
 </tile>
 <tile id="80" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var21.png"/>
 </tile>
 <tile id="81" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var22.png"/>
 </tile>
 <tile id="82" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var23.png"/>
 </tile>
 <tile id="83" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird1/var24.png"/>
 </tile>
 <tile id="84" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var0.png"/>
 </tile>
 <tile id="85" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var1.png"/>
 </tile>
 <tile id="86" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var2.png"/>
 </tile>
 <tile id="87" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var3.png"/>
 </tile>
 <tile id="88" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var4.png"/>
 </tile>
 <tile id="89" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var5.png"/>
 </tile>
 <tile id="90" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var6.png"/>
 </tile>
 <tile id="91" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var7.png"/>
 </tile>
 <tile id="92" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var8.png"/>
 </tile>
 <tile id="93" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var9.png"/>
 </tile>
 <tile id="94" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var10.png"/>
 </tile>
 <tile id="95" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var11.png"/>
 </tile>
 <tile id="96" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var12.png"/>
 </tile>
 <tile id="97" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var13.png"/>
 </tile>
 <tile id="98" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var14.png"/>
 </tile>
 <tile id="99" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var15.png"/>
 </tile>
 <tile id="100" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var16.png"/>
 </tile>
 <tile id="101" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var17.png"/>
 </tile>
 <tile id="102" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var18.png"/>
 </tile>
 <tile id="103" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var19.png"/>
 </tile>
 <tile id="104" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var20.png"/>
 </tile>
 <tile id="105" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var21.png"/>
 </tile>
 <tile id="106" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var22.png"/>
 </tile>
 <tile id="107" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var23.png"/>
 </tile>
 <tile id="108" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/bird2/var24.png"/>
 </tile>
 <tile id="109" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var0.png"/>
 </tile>
 <tile id="110" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var1.png"/>
 </tile>
 <tile id="111" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var2.png"/>
 </tile>
 <tile id="112" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var3.png"/>
 </tile>
 <tile id="113" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var4.png"/>
 </tile>
 <tile id="114" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var5.png"/>
 </tile>
 <tile id="115" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var6.png"/>
 </tile>
 <tile id="116" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var7.png"/>
 </tile>
 <tile id="117" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var8.png"/>
 </tile>
 <tile id="118" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var9.png"/>
 </tile>
 <tile id="119" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var10.png"/>
 </tile>
 <tile id="120" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var11.png"/>
 </tile>
 <tile id="121" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var12.png"/>
 </tile>
 <tile id="122" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var13.png"/>
 </tile>
 <tile id="123" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var14.png"/>
 </tile>
 <tile id="124" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var15.png"/>
 </tile>
 <tile id="125" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var16.png"/>
 </tile>
 <tile id="126" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var17.png"/>
 </tile>
 <tile id="127" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var18.png"/>
 </tile>
 <tile id="128" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var19.png"/>
 </tile>
 <tile id="129" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var20.png"/>
 </tile>
 <tile id="130" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var21.png"/>
 </tile>
 <tile id="131" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var22.png"/>
 </tile>
 <tile id="132" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/box/var23.png"/>
 </tile>
 <tile id="133" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var0.png"/>
 </tile>
 <tile id="134" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var1.png"/>
 </tile>
 <tile id="135" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var2.png"/>
 </tile>
 <tile id="136" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var3.png"/>
 </tile>
 <tile id="137" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var4.png"/>
 </tile>
 <tile id="138" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var5.png"/>
 </tile>
 <tile id="139" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var6.png"/>
 </tile>
 <tile id="140" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var7.png"/>
 </tile>
 <tile id="141" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var8.png"/>
 </tile>
 <tile id="142" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var9.png"/>
 </tile>
 <tile id="143" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var10.png"/>
 </tile>
 <tile id="144" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var11.png"/>
 </tile>
 <tile id="145" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var12.png"/>
 </tile>
 <tile id="146" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var13.png"/>
 </tile>
 <tile id="147" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var14.png"/>
 </tile>
 <tile id="148" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var15.png"/>
 </tile>
 <tile id="149" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var16.png"/>
 </tile>
 <tile id="150" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var17.png"/>
 </tile>
 <tile id="151" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var18.png"/>
 </tile>
 <tile id="152" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var19.png"/>
 </tile>
 <tile id="153" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var20.png"/>
 </tile>
 <tile id="154" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var21.png"/>
 </tile>
 <tile id="155" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var22.png"/>
 </tile>
 <tile id="156" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/cargo/var23.png"/>
 </tile>
 <tile id="157" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var0.png"/>
 </tile>
 <tile id="158" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var1.png"/>
 </tile>
 <tile id="159" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var2.png"/>
 </tile>
 <tile id="160" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var3.png"/>
 </tile>
 <tile id="161" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var4.png"/>
 </tile>
 <tile id="162" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var5.png"/>
 </tile>
 <tile id="163" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var6.png"/>
 </tile>
 <tile id="164" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var7.png"/>
 </tile>
 <tile id="165" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var8.png"/>
 </tile>
 <tile id="166" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var9.png"/>
 </tile>
 <tile id="167" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var10.png"/>
 </tile>
 <tile id="168" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var11.png"/>
 </tile>
 <tile id="169" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var12.png"/>
 </tile>
 <tile id="170" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var13.png"/>
 </tile>
 <tile id="171" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var14.png"/>
 </tile>
 <tile id="172" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var15.png"/>
 </tile>
 <tile id="173" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var16.png"/>
 </tile>
 <tile id="174" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var17.png"/>
 </tile>
 <tile id="175" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var18.png"/>
 </tile>
 <tile id="176" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var19.png"/>
 </tile>
 <tile id="177" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var20.png"/>
 </tile>
 <tile id="178" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var21.png"/>
 </tile>
 <tile id="179" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var22.png"/>
 </tile>
 <tile id="180" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/corn/var23.png"/>
 </tile>
 <tile id="181" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var0.png"/>
 </tile>
 <tile id="182" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var1.png"/>
 </tile>
 <tile id="183" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var2.png"/>
 </tile>
 <tile id="184" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var3.png"/>
 </tile>
 <tile id="185" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var4.png"/>
 </tile>
 <tile id="186" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var5.png"/>
 </tile>
 <tile id="187" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var6.png"/>
 </tile>
 <tile id="188" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var7.png"/>
 </tile>
 <tile id="189" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var8.png"/>
 </tile>
 <tile id="190" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var9.png"/>
 </tile>
 <tile id="191" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var10.png"/>
 </tile>
 <tile id="192" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/hedge/var11.png"/>
 </tile>
 <tile id="193" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var0.png"/>
 </tile>
 <tile id="194" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var1.png"/>
 </tile>
 <tile id="195" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var2.png"/>
 </tile>
 <tile id="196" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var3.png"/>
 </tile>
 <tile id="197" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var4.png"/>
 </tile>
 <tile id="198" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var5.png"/>
 </tile>
 <tile id="199" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var6.png"/>
 </tile>
 <tile id="200" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var7.png"/>
 </tile>
 <tile id="201" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var8.png"/>
 </tile>
 <tile id="202" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var9.png"/>
 </tile>
 <tile id="203" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var10.png"/>
 </tile>
 <tile id="204" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var11.png"/>
 </tile>
 <tile id="205" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var12.png"/>
 </tile>
 <tile id="206" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var13.png"/>
 </tile>
 <tile id="207" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var14.png"/>
 </tile>
 <tile id="208" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var15.png"/>
 </tile>
 <tile id="209" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var16.png"/>
 </tile>
 <tile id="210" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var17.png"/>
 </tile>
 <tile id="211" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var18.png"/>
 </tile>
 <tile id="212" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var19.png"/>
 </tile>
 <tile id="213" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var20.png"/>
 </tile>
 <tile id="214" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var21.png"/>
 </tile>
 <tile id="215" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var22.png"/>
 </tile>
 <tile id="216" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/plant/var23.png"/>
 </tile>
 <tile id="217" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var0.png"/>
 </tile>
 <tile id="218" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var1.png"/>
 </tile>
 <tile id="219" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var2.png"/>
 </tile>
 <tile id="220" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var3.png"/>
 </tile>
 <tile id="221" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var4.png"/>
 </tile>
 <tile id="222" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var5.png"/>
 </tile>
 <tile id="223" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var6.png"/>
 </tile>
 <tile id="224" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var7.png"/>
 </tile>
 <tile id="225" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var8.png"/>
 </tile>
 <tile id="226" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var9.png"/>
 </tile>
 <tile id="227" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var10.png"/>
 </tile>
 <tile id="228" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var11.png"/>
 </tile>
 <tile id="229" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var12.png"/>
 </tile>
 <tile id="230" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var13.png"/>
 </tile>
 <tile id="231" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var14.png"/>
 </tile>
 <tile id="232" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var15.png"/>
 </tile>
 <tile id="233" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var16.png"/>
 </tile>
 <tile id="234" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var17.png"/>
 </tile>
 <tile id="235" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var18.png"/>
 </tile>
 <tile id="236" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var19.png"/>
 </tile>
 <tile id="237" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var20.png"/>
 </tile>
 <tile id="238" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var21.png"/>
 </tile>
 <tile id="239" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var22.png"/>
 </tile>
 <tile id="240" type="decoration">
  <image width="34" height="34" source="Assets/Decorations/truck/var23.png"/>
 </tile>
</tileset>
