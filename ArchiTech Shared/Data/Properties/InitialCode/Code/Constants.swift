//
//  Constants.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SpriteKit

struct Constants {
    struct Profit {
        struct DocFrag {
            static let description: Int = 50
            static let parameters: Int = 15
            static let returnAndThrow: Int = 10
            static let warningOrConstraint: Int = 25
        }

        struct Test {
            static let fullCoverage: Int = 100
        }

        struct CodeFrag {
            struct Content {
                static let typeAnnotationMultiplier: Ratio = 1.0 / 3.0
                static let functionAnnotationMultiplier: Ratio = 1
            }

            static let perDependent: Int = 5
        }

        struct CodeType {
            static let generic: Int = 5
            static let alias: Int = 10
            static let compound: Int = 0
        }

        struct Function {
            static let bodyMax: Int = 100
        }
    }

    struct Display {
        struct Size {
            struct Tile {
                static let scale: CGFloat = 2
            }
        }
    }

    struct Assets {
        struct Plot {
            static let shapeImage: Image = Image(byReferencing: Bundle.main.url(
                forResource: "Assets/Plots/shape",
                withExtension: "png"
            )!)
            static let shapeTexture: SKTexture = {
                let texture = SKTexture(image: shapeImage)
                texture.filteringMode = .nearest
                return texture
            }()
        }
    }
}
