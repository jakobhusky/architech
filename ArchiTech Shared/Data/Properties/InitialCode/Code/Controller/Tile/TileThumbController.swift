//
//  TileThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Tile, view = TileThumbView, guardModelUpdate
class TileThumbController: AutoModelController {
    let building: BuildingThumbController
    let road: RoadThumbController
    let plot: PlotThumbController
    
    private func updateDownstream() {
        if let modelBuilding = model.building {
            building.model = modelBuilding
            building.view.isHidden = false
        } else {
            building.view.isHidden = true
        }
        if let modelRoad = model.road {
            road.model = modelRoad
            road.view.isHidden = false
        } else {
            road.view.isHidden = true
        }
        plot.model = model.plot
    }

// sourcery:inline:auto:TileThumbController.AutoModelController
    // MARK: - TileThumbController AutoModelController
    typealias Model = Tile
    typealias View = TileThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            if (!blockDownstream && _model == newValue) {
                return
            }

            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view
        self.building = BuildingThumbController(view: view.building)
        self.road = RoadThumbController(view: view.road)
        self.plot = PlotThumbController(view: view.plot)
        self.building = BuildingThumbController(view: view.building)
        self.road = RoadThumbController(view: view.road)
        self.plot = PlotThumbController(view: view.plot)

        building.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.building = self.building.model
            }
        }
        road.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.road = self.road.model
            }
        }
        plot.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.plot = self.plot.model
            }
        }
        building.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.building = self.building.model
            }
        }
        road.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.road = self.road.model
            }
        }
        plot.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.plot = self.plot.model
            }
        }

    }


    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
