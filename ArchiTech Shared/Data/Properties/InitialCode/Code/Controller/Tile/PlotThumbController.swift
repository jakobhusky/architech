//
//  PlotThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Plot, view = PlotThumbView, skipProperties, guardModelUpdate
class PlotThumbController: AutoModelController {
    private static func image(forPlotType plotType: String) -> Image {
        return Image.load(from: Bundle.main.url(
            forResource: "Assets/Plots/\(plotType)",
            withExtension: "png"
        )!)
    }
    
    private func updateDownstream() {
        view.image = PlotThumbController.image(forPlotType: model.type)
        view.color = model.color.map(Color.init)
    }

// sourcery:inline:auto:PlotThumbController.AutoModelController
    // MARK: - PlotThumbController AutoModelController
    typealias Model = Plot
    typealias View = PlotThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            if (!blockDownstream && _model == newValue) {
                return
            }

            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view

    }


    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
