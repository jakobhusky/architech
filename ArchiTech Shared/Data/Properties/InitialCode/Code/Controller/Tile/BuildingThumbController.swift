//
//  BuildingThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Building, view = TileFragThumbView, skipProperties, guardModelUpdate
class BuildingThumbController: AutoModelController {
    private static func image(forBuildingType buildingType: String) -> Image {
        return Image.load(from: Bundle.main.url(
            forResource: "Assets/Buildings/\(buildingType)",
            withExtension: "png"
        )!)
    }
    
    private func updateDownstream() {
        view.image = BuildingThumbController.image(forBuildingType: model.type)
    }

// sourcery:inline:auto:BuildingThumbController.AutoModelController
    // MARK: - BuildingThumbController AutoModelController
    typealias Model = Building
    typealias View = TileFragThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            if (!blockDownstream && _model == newValue) {
                return
            }

            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view

    }


    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
