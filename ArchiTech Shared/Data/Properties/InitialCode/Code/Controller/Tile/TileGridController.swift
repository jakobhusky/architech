//
//  TileGridController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SKTiled
import SpriteKit

class TileGridController {
    private struct TileGroupLayer {
        let skGroupLayer: SKGroupLayer
        
        let children: [SKTileLayer]
        let building: SKTileLayer
        let road: SKTileLayer
        let plot: SKTileLayer
        let plotBase: SKTileLayer

        init(_ skGroupLayer: SKGroupLayer) {
            self.skGroupLayer = skGroupLayer
            children = [SKTileLayer](skGroupLayer.childLayers.dropFirst().map { $0 as! SKTileLayer })
            building = children.first { $0.name == "Buildings" }!
            road = children.first { $0.name == "Roads" }!
            plot = children.first { $0.name == "Plots" }!
            plotBase = children.first { $0.name == "PlotBases" }!
        }
    }
    
    private static func label(ofTilesetData tilesetData: SKTilesetData) -> String {
        return ((tilesetData.source as NSString).lastPathComponent as NSString).deletingPathExtension
    }
    
    var _model: TileGrid! {
        didSet {
            _observers.notify()
        }
    }
    var model: TileGrid {
        get { return _model }
        set {
            _model = newValue
            
            for groupLayer in groupLayers.values {
                clear(groupLayer: groupLayer)
            }
            
            for (layer, tiles) in model.tiles {
                let groupLayer = groupLayers[layer]!
                for (pos, tile) in tiles {
                    add(tile: tile, at: pos, to: groupLayer)
                }
            }
        }
    }
    private let _observers: Observers<TileGrid>.Notifyable
    let tilemap: SKTilemap
    private let groupLayers: [TileLayer:TileGroupLayer]
    private let tileset: SKTileset
    private let buildingGids: [String:Int]
    private let roadGids: [String:Int]
    private let plotGids: [String:Int]
    private let plotBaseGid: Int
    
    var observers: Observers<TileGrid> { return _observers }

    init(tilemap: SKTilemap) {
        self.tilemap = tilemap
        _observers = Observers<TileGrid>.Notifyable()
        groupLayers = [
            .placing: TileGroupLayer(tilemap.getLayers(named: "Placing").first as! SKGroupLayer),
            .regular: TileGroupLayer(tilemap.getLayers(named: "Regular").first as! SKGroupLayer)
        ]
        tileset = tilemap.tilesets.first!
        buildingGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "building"),
            by: TileGridController.label
        ).mapValues { $0.id + 1 }
        roadGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "road"),
            by: TileGridController.label
        ).mapValues { $0.id + 1 }
        plotGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "plot"),
            by: TileGridController.label
        ).mapValues { $0.id + 1 }
        plotBaseGid = tileset.getTileData(ofType: "plot_base").first!.id + 1
        
        tilemap.setScale(Constants.Display.Size.Tile.scale)
    }
    
    func add(tiles: [IntVector:Tile], to layer: TileLayer) {
        for (pos, tile) in tiles {
            add(tile: tile, at: TileLocation(layer: layer, position: pos))
        }
    }
    
    func add(tile: Tile, at location: TileLocation) {
        _model[location] = tile
        add(tile: tile, at: location.position, to: groupLayers[location.layer]!)
    }
    
    func clear(layer: TileLayer) {
        _model.tiles[layer] = nil
        clear(groupLayer: groupLayers[layer]!)
    }
    
    func remove(tileAt location: TileLocation) {
        _model[location] = nil
        remove(tileAt: location.position, groupLayer: groupLayers[location.layer]!)
    }
    
    private func add(tile: Tile, at pos: IntVector, to groupLayer: TileGroupLayer) {
        if let building = tile.building {
            add(
                tileWithGid: buildingGids[building.type]!,
                plotColor: nil,
                at: pos,
                to: groupLayer.building
            )
        }
        if let road = tile.road {
            add(
                tileWithGid: roadGids[road.type]!,
                plotColor: nil,
                at: pos,
                to: groupLayer.road
            )
            fatalError("TODO Roads not yet implemented")
        }
        //TODO Colorize
        add(
            tileWithGid: plotGids[tile.plot.type]!,
            plotColor: tile.plot.color,
            at: pos,
            to: groupLayer.plot
        )
    }
    
    private func add(
        tileWithGid gid: Int,
        plotColor: HSBColor?,
        at pos: IntVector,
        to tileLayer: SKTileLayer
    ) {
        let tile = tileLayer.addTileAt(pos.x, pos.y, gid: gid)!
        //TODO This is a hack - why doesn't texture normally show up?
        let tileImage = SKSpriteNode(texture: tile.texture)
        if let plotColor = plotColor {
            let tileColorImage = SKSpriteNode(texture: Constants.Assets.Plot.shapeTexture)
            tileColorImage.color = Color(plotColor)
            tileColorImage.colorBlendFactor = 1
            tile.addChild(tileColorImage)
        }
        if let tintString = tileLayer.properties["tint"] {
            let tintStringComponents = tintString.split(separator: "|").map(String.init)
            tileImage.color = Color(hexString: tintStringComponents[0])
            tileImage.colorBlendFactor = CGFloat(Ratio(tintStringComponents[1])!)
        }
        tile.zPosition = CGFloat(pos.y + pos.x)
        tile.addChild(tileImage)
    }
    
    private func clear(groupLayer: TileGroupLayer) {
        for tileLayer in groupLayer.children {
            for x in 0..<Int(tileLayer.size.width) {
                for y in 0..<Int(tileLayer.size.height) {
                    let _ = tileLayer.removeTileAt(x, y)
                }
            }
        }
    }
    
    private func remove(tileAt pos: IntVector, groupLayer: TileGroupLayer) {
        for tileLayer in groupLayer.children {
            let _ = tileLayer.removeTileAt(pos.x, pos.y)
        }
    }
}
