//
//  PlacingStructureThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Structure?
class PlacingStructureThumbController: AutoModelController {
    let structure: StructureThumbController
    
    required init(view: PlacingStructureThumbView) {
        self.view = view
        structure = StructureThumbController(view: view.structure)
    }
    
    private func updateDownstream() {
        if let model = model {
            structure.model = model
            view.isHidden = false
        } else {
            view.isHidden = true
        }
    }

// sourcery:inline:auto:PlacingStructureThumbController.AutoModelController
    // MARK: - PlacingStructureThumbController AutoModelController
    typealias Model = Structure?
    typealias View = PlacingStructureThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }



    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
