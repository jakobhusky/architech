//
//  StructureCollectionController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class StructureCollectionController: ModelController, CollectionView.Delegate, ViewDelegate {
    typealias Model = [Structure]
    typealias View = StructureCollectionView
    
    var _model: [Structure]! {
        didSet {
            _observers.notify()
        }
    }
    private let _observers: Observers<[Structure]>.Notifyable
    let view: StructureCollectionView
    weak var delegate: StructureCollectionControllerDelegate? = nil
    private var _items: [StructureThumbController?] = []
    private var _selectedIndex: Int?
    
    var model: [Structure] {
        get { return _model }
        set {
            _model = newValue
            view.reloadData()
        }
    }
    var observers: Observers<[Structure]> { return _observers }
    var items: [StructureThumbController?] { return _items }
    var selectedIndex: Int? { return _selectedIndex }
    var selectedItem: StructureThumbController? {
        return selectedIndex.map { items[$0]! }
    }
    
    required init(view: StructureCollectionView) {
        _observers = Observers<[Structure]>.Notifyable()
        self.view = view
        view.collectionViewDelegate = self
        view.viewDelegate = self
        view.itemIdentifier = CollectionView.Item.Identifier("StructureCollectionViewItem")
    }
    
    func append(_ structure: Structure) {
        let newIndex = model.count
        _model.append(structure)
        view.insertItems(at: [IndexPath(item: newIndex, section: 0)])
    }
    
    ///Skips the delegate
    func remove(_ structureId: Identifier) {
        remove(at: model.firstIndex { $0.id == structureId }!)
    }
    
    ///Skips the delegate
    func remove(at index: Int) {
        if selectedIndex == index {
            _selectedIndex = nil
        }
        _model.remove(at: index)
        _items.remove(at: index)
        view.deleteItems(at: [IndexPath(item: index, section: 0)])
    }
    
    ///Confirms with the delegate
    func tryRemove(at index: Int) {
        let structure = model[index]
        if delegate?.shouldRemove(structure: structure) ?? false {
            remove(at: index)
        }
    }
    
    // MARK: - Collection view delegate
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfItems(inSection section: Int) -> Int {
        return _model?.count ?? 0
    }
    
    func configure(item: CollectionView.Item, at indexPath: IndexPath) {
        let item = item as! StructureCollectionViewItem
        let index = indexPath.item
        
        //Be careful of old controller
        let controller = structureControllerFor(index: index, item: item)
        if (controller.observers[self] == nil || controller.model.id != model[index].id) {
            controller.observers[self] = nil
            controller.model = model[index]
            unowned let unownedController = controller
            controller.observers[self] = { [unowned self] in
                self._model[index] = unownedController.model
            }
        }
    }
    
    private func structureControllerFor(index: Int, item: StructureCollectionViewItem) -> StructureThumbController {
        _items.pad(to: index + 1, with: nil)
        let oldController = _items[index]
        if oldController?.view != item.structureView {
            if let swapIndex = _items.firstIndex(where: { $0?.view == item.structureView }) {
                _items[index] = _items[swapIndex]
                _items[swapIndex] = oldController
                if let oldController = oldController {
                    oldController.observers[self] = nil
                    oldController.model = _items[index]!.model
                    unowned let unownedController = oldController
                    oldController.observers[self] = { [unowned self] in
                        self._model[swapIndex] = unownedController.model
                    }
                }
            } else {
                _items[index] = StructureThumbController(view: item.structureView)
            }
        }
        return _items[index]!
    }
    
    func didSelectItems(at indexPaths: Set<IndexPath>) {
        _selectedIndex = indexPaths.first?.item
    }
    
    // MARK: - View delegate
    
    func handles(key: Key) -> Bool {
        return (key == Key(modifiers: [], characters: String.backspace) && selectedIndex != nil)
    }
    
    func handle(key: Key) {
        if let selectedIndex = selectedIndex,
            key == Key(modifiers: [], characters: String.backspace) {
            tryRemove(at: selectedIndex)
        }
    }
}

protocol StructureCollectionControllerDelegate: AnyObject {
    func shouldRemove(structure: Structure) -> Bool
}
