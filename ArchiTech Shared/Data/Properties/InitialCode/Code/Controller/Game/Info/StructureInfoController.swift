//
//  StructureInfoController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Structure, skipProperty = [location]
class StructureInfoController: AutoModelController {
    // sourcery: substitute = codeFrag
    let codeFragInfo: CodeFragInfoController
    // sourcery: substitute = tile
    let tileInfo: TileInfoController

// sourcery:inline:auto:StructureInfoController.AutoModelController
    // MARK: - StructureInfoController AutoModelController
    typealias Model = Structure
    typealias View = StructureInfoView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view
        self.codeFragInfo = CodeFragInfoController(view: view.codeFragInfo)
        self.tileInfo = TileInfoController(view: view.tileInfo)


    }

    private func updateDownstream() {
    }

    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
