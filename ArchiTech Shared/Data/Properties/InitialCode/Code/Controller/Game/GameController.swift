//
//  GameController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SpriteKit

// sourcery: model = Game
class GameController: AutoModelController, TileSceneDelegate, StructureCollectionControllerDelegate {
    // sourcery: substitute = selectionId
    let selection: StructureInfoController
    // sourcery: substitute = placingStructureId
    let placingStructure: PlacingStructureThumbController

    private func setup() {
        assert(_model == nil)
        selection.observers[self] = {
            if (!self.ignoreDownstream) {
                self._model.selectionId = self.selection.model.id
            }
        }
        placingStructure.observers[self] = {
            if (!self.ignoreDownstream) {
                self._model.placingStructureId = self.placingStructure.model?.id
            }
        }

        world.scene.tileSceneDelegate = self
        world.placedStructures.delegate = self
    }

    func shouldBeginHighlight(coord: IntVector) -> Bool {
        guard var selectedStructure = world.unplacedStructures.selectedItem?.model else {
            return false
        }

        selectedStructure.location = TileLocation(layer: .placing, position: coord)
        placingStructure.model = selectedStructure
        updatePlacingTiles()
        return true
    }

    func didUpdateHighlight(coord: IntVector) {
        placingStructure.model!.location!.position = coord
        updatePlacingTiles()
    }

    func didEndHighlight() {
        tryPlace()
        placingStructure.model = nil
        updatePlacingTiles()
    }

    private func tryPlace() {
        var placedStructure = placingStructure.model!
        placedStructure.location!.layer = .regular
        
        world.performDownstreamOperations {
            let world = self.world
            if world.tileGrid.model[placedStructure.location!] == nil {
                world.placedStructures.append(placedStructure)
                world.unplacedStructures.remove(placedStructure.id)
                world.tileGrid.clear(layer: .placing)
                world.tileGrid.add(tile: placedStructure.tile, at: placedStructure.location!)
            }
        }
    }

    private func updatePlacingTiles() {
        world.performDownstreamOperations {
            let world = self.world
            world.tileGrid.clear(layer: .placing)
            
            if let placingStructure = self.placingStructure.model {
                world.tileGrid.add(tile: placingStructure.tile, at: placingStructure.location!)
            }
        }
    }

    func shouldRemove(structure: Structure) -> Bool {
        var unplacedStructure = structure
        unplacedStructure.location = nil

        world.performDownstreamOperations {
            let world = self.world
            world.tileGrid.remove(tileAt: structure.location!)
            world.unplacedStructures.append(unplacedStructure)
        }
            
        return true
    }

    // sourcery:inline:auto:GameController.AutoModelController
    // MARK: - GameController AutoModelController
    typealias Model = Game
    typealias View = GameView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View
    let code: CodeController
    let world: WorldController

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view
        self.code = CodeController(view: view.code)
        self.world = WorldController(view: view.world)
        self.selection = StructureInfoController(view: view.selection)
        self.placingStructure = PlacingStructureThumbController(view: view.placingStructure)

        code.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.code = self.code.model
            }
        }
        world.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.world = self.world.model
            }
        }

        setup()
    }

    private func updateDownstream() {
        code.model = model.code
        world.model = model.world
        if let modelSelection = model.selection {
            selection.model = modelSelection
            selection.view.isHidden = false
        } else {
            selection.view.isHidden = true
        }
        if let modelPlacingStructure = model.placingStructure {
            placingStructure.model = modelPlacingStructure
            placingStructure.view.isHidden = false
        } else {
            placingStructure.view.isHidden = true
        }
    }

    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
    // sourcery:end
}
