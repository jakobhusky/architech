//
//  WorldController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SKTiled

// sourcery: model = World, skipProperties
class WorldController: AutoModelController {    
    let scene: TileScene
    let unplacedStructures: StructureCollectionController
    let placedStructures: StructureCollectionController
    private(set) var tileGrid: TileGridController!
    
    required init(view: WorldView) {
        self.view = view
        unplacedStructures = StructureCollectionController(view: view.unplacedStructures)
        placedStructures = StructureCollectionController(view: view.placedStructures)
        let sceneView = view.scene!
        scene = TileScene(size: sceneView.bounds.size)
        
        unplacedStructures.observers[self] = updateUpstreamStructures
        placedStructures.observers[self] = updateUpstreamStructures
        scene.scaleMode = .resizeFill
        sceneView.presentScene(scene)
        scene.setup(
            url: Bundle.main.url(forResource: "EmptyMap", withExtension: "tmx")!,
            withTilesets: [SKTileset(fileNamed: "TileSet")],
            ignoreProperties: false,
            loggingLevel: .debug,
            { tilemap in
                print("Loaded scene")
                self.tileGrid = TileGridController(tilemap: tilemap)
                self.tileGrid.observers[self] = {
                    if (!self.ignoreDownstream) {
                        self.model.tileGrid = self.tileGrid.model
                    }
                }
                if let model = self._model {
                    self.tileGrid.model = model.tileGrid
                }
            }
        )
    }
    
    func updateDownstream() {
        let placableStructures = model.placableStructures
        unplacedStructures.model = placableStructures.filter { $0.location == nil }
        placedStructures.model = placableStructures.filter { $0.location != nil }
        
        if let tileGrid = self.tileGrid {
            tileGrid.model = model.tileGrid
        }
    }
    
    private func updateUpstreamStructures() {
        if (!ignoreDownstream) {
            model.structures = model.unplacableStructures + unplacedStructures.model + placedStructures.model
        }
    }

// sourcery:inline:auto:WorldController.AutoModelController
    // MARK: - WorldController AutoModelController
    typealias Model = World
    typealias View = WorldView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }



    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
