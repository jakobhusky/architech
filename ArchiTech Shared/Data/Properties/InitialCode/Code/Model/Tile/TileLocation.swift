//
//  TileLocation.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct TileLocation: Equatable, Codable {
    var layer: TileLayer
    var position: IntVector
}
