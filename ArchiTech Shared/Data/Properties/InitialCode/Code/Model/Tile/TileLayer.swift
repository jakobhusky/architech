//
//  TileLayer.swift
//  ArchiTech iOS
//
//  Created by Jakob Hain on 10/28/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum TileLayer: Equatable, AutoCodable {
    case regular
    case placing
}
