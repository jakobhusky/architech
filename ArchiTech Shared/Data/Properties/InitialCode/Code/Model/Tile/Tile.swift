//
//  Tile.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Tile: Equatable, Codable {
    struct Context: Codable {
        let plots: [String:Plot.Info]
        let buildings: [String:Building.Info]
        let roads: Set<String>
    }
    
    var building: Building?
    var road: Road?
    var plot: Plot
}
