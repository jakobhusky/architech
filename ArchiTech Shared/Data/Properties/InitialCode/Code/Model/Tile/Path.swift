//
//  Path.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///Currently unused
struct Path: Equatable, Codable {
    let sourceId: Identifier
    let destinationId: Identifier
    let path: [IntVector]
}
