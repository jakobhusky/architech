//
//  Building.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Building: Equatable, Codable {
    struct Info: Codable {
        enum Category: AutoCodable {
            case house
            case commercial
            case industry
            case outdoor
        }
        
        let value: Int
        let plotType: String
        let category: Category
    }
    
    let type: String
}
