//
//  TileGrid.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct TileGrid: Equatable, Codable {
    var tiles: [TileLayer:[IntVector:Tile]]
    
    subscript(location: TileLocation) -> Tile? {
        get { return tiles[location.layer]?[location.position] }
        set {
            if tiles[location.layer] == nil {
                tiles[location.layer] = [:]
            }
            tiles[location.layer]![location.position] = newValue
        }
    }
}
