//
//  Structure.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Structure: Codable {
    ///Identifies and internally describes the structure
    var codeFrag: CodeFrag
    ///Displays the structure
    var tile: Tile
    ///Positions the structure in the world any might impact relations to other structures
    var location: TileLocation?
    
    var id: Identifier {
        return codeFrag.id
    }
}
