//
//  Function.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Function: Equatable, Codable {
    struct Signature: Equatable, Codable {
        struct Params: Equatable, Codable {
            let main: [SignatureType]
            let other: [SignatureType]
        }
        
        let params: Params
        let returnType: SignatureType
    }
    
    struct Body: Equatable, Codable {
        let statements: [Statement]
    }
    
    let signature: Signature
    let body: Body
}
