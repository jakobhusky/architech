//
//  SignatureType.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

indirect enum SignatureType: Equatable, AutoCodable {
    case generic(base: SignatureType, params: [SignatureType])
    case concrete(type: IdRef<CodeType>)
    case variable(label: String)
}
