//
//  Code.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Code: Equatable, Codable {
    static let empty: Code = Code(frags: [], duplicates: [])
    
    static func +(_ lhs: Code, _ rhs: Code) -> Code {
        return Code(
            frags: lhs.frags.union(rhs.frags),
            duplicates: lhs.duplicates + rhs.duplicates
        )
    }
    
    static func +=(_ lhs: inout Code, _ rhs: Code) {
        lhs.frags.formUnion(rhs.frags)
        lhs.duplicates += rhs.duplicates
    }
    
    var frags: Set<CodeFrag>
    var duplicates: [CodeDuplicate]
    
    var iddFrags: [IdRef<CodeFrag>:CodeFrag] {
        return Dictionary(keying: Array(frags), by: { $0.id })
    }
}
