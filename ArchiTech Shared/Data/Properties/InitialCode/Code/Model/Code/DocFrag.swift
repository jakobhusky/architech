//
//  DocFrag.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum DocFrag: Equatable, AutoCodable {
    case description
    case parameters
    case returnAndThrow
    case warningOrConstraint
}
