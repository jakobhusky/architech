//
//  World.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct World: Codable {
    struct Context: Codable {
        let tile: Tile.Context
    }
    
    var structures: [Structure]
    var tileGrid: TileGrid
    
    var iddStructures: [Identifier:Structure] {
        return [Identifier:Structure](keying: structures, by: { $0.id })
    }
    var unplacableStructures: [Structure] {
        return structures.filter { $0.codeFrag.isBuiltin }
    }
    var placableStructures: [Structure] {
        return structures.filter { !$0.codeFrag.isBuiltin }
    }
}
