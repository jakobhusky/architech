//
//  Game.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Game: Codable {    
    var code: Code
    var world: World
    var selectionId: Identifier?
    var placingStructureId: Identifier?
    
    var selection: Structure? {
        return selectionId.map { world.iddStructures[$0]! }
    }
    var placingStructure: Structure? {
        return placingStructureId.map { world.iddStructures[$0]! }
    }
}
