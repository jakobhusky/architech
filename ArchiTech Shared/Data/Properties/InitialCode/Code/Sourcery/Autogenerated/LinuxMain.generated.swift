// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

import XCTest

extension ArchiTech_Analyze_SwiftTests {
  static var allTests: [(String, (ArchiTech_Analyze_SwiftTests) -> () throws -> Void)] = [
    ("testDirectly", testDirectly)
    ("testXPCProtocol", testXPCProtocol)
    ("testPerformanceExample", testPerformanceExample)
  ]
}

// swiftlint:disable trailing_comma
XCTMain([
  testCase(ArchiTech_Analyze_SwiftTests.allTests),
])
// swiftlint:enable trailing_comma

