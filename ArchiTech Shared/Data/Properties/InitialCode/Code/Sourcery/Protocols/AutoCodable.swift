//
//  AutoCodable.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol AutoCodable: AutoEncodable, AutoDecodable {}
