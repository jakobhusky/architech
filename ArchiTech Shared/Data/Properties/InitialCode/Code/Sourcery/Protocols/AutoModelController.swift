//
//  AutoModelController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/1/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol AutoModelController: ModelController {}

///(Hacky) substitute for model types which are handled in a special way (optionals)
private protocol AutoModelController_Empty {}
