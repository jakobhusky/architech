//
//  Validate.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Validate {
    ///Whether this fragment is actually connected to source, or e.g. the source was just altered
    static func codeFragIsDetached(_ codeFrag: CodeFrag) -> Bool {
        return codeFrag.region == nil && !codeFrag.isBuiltin
    }
}
