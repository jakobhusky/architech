//
//  Initial.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Initial {
    static let tileGrid: TileGrid = TileGrid(tiles: [:])

    static func game(code: Code, context: World.Context) -> Game {
        return Game(
            code: code,
            world: world(code: code, context: context),
            selectionId: nil,
            placingStructureId: nil
        )
    }

    private static func world(code: Code, context: World.Context) -> World {
        return World(
            structures: code.frags.compactMap {
                structure(
                    codeFrag: $0,
                    code: code,
                    context: context.tile
                )
            },
            tileGrid: tileGrid
        )
    }

    private static func structure(codeFrag: CodeFrag, code: Code, context: Tile.Context) -> Structure? {
        guard let tile = Initial.tile(codeFrag: codeFrag, code: code, context: context) else {
            return nil
        }

        return Structure(codeFrag: codeFrag, tile: tile, location: nil)
    }

    private static func tile(codeFrag: CodeFrag, code: Code, context: Tile.Context) -> Tile? {
        guard let plot = plot(codeFrag: codeFrag, code: code, context: context.plots) else {
            return nil
        }

        return Tile(
            building: building(codeFrag: codeFrag, plot: plot, context: context.buildings),
            road: nil,
            plot: plot
        )
    }

    private static func plot(codeFrag: CodeFrag, code: Code, context: [String:Plot.Info]) -> Plot? {
        switch codeFrag.content {
        case .function(let function):
            return plot(signatureType: function.signature.returnType, code: code, context: context)
        case .type(_):
            return plot(typeLabeled: codeFrag.label, context: context)
        }
    }

    private static func plot(signatureType: SignatureType, code: Code, context: [String:Plot.Info]) -> Plot? {
        switch signatureType {
        case .concrete(let type):
            return plot(
                codeFrag: code.iddFrags[type]!,
                code: code,
                context: context
            )
        case .variable(let label):
            return plot(typeVariable: label, context: context)
        case .generic(let base, _):
            return plot(signatureType: base, code: code, context: context)
        }
    }

    private static func plot(typeLabeled label: Label, context: [String:Plot.Info]) -> Plot {
        return plot(seed: label.seed, kind: .normal, context: context)
    }

    private static func plot(typeVariable: String, context: [String:Plot.Info]) -> Plot {
        return plot(seed: typeVariable.hashValue, kind: .wildcard, context: context)
    }

    private static func plot(seed: Int, kind: Plot.Kind, context: [String:Plot.Info]) -> Plot {
        let possibleTypes = context.filter { $0.value.kind == kind }.sorted { $0.key < $1.key }
        let (key: plotType, value: plotInfo) = possibleTypes[abs(seed) % possibleTypes.count]
        return Plot(
            color: plotInfo.color(alter: ColorAlter(seed: seed)),
            type: plotType
        )
    }

    private static func building(codeFrag: CodeFrag, plot: Plot, context: [String:Building.Info]) -> Building? {
        switch codeFrag.content {
        case .function(_):
            return building(functionLabeled: codeFrag.label, plot: plot, context: context)
        case .type(_):
            return nil
        }
    }

    private static func building(
        functionLabeled label: Label,
        plot: Plot,
        context: [String:Building.Info]
    ) -> Building {
        let possibleBuildings = context.filter { $0.value.plotType == plot.type }.keys.sorted()
        return Building(
            type: possibleBuildings[abs(label.seed) % possibleBuildings.count]
        )
    }
}
