//
//  ColorRange.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct ColorRange: Codable {
    let hue: Range<Ratio>
    let saturation: Range<Ratio>
    let brightness: Range<Ratio>

    func constrain(alterMultiplier: ColorAlter) -> ColorAlter {
        return ColorAlter(
            hue: hue.middle(at: alterMultiplier.hue),
            saturation: saturation.middle(at: alterMultiplier.saturation),
            brightness: brightness.middle(at: alterMultiplier.brightness)
        )
    }
}
