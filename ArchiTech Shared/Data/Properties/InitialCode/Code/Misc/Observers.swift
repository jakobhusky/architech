//
//  Observers.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/1/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class Observers<T> {
    class Notifyable: Observers<T> {
        func notify() {
            for observer in observers.objectEnumerator()! {
                (observer as! CallbackObject).callback()
            }
        }
    }
    
    private var observers: NSMapTable<AnyObject, CallbackObject>
    
    init() {
        observers = NSMapTable.weakToStrongObjects()
    }
    
    subscript(target: AnyObject) -> (() -> Void)? {
        get { return observers.object(forKey: target)?.callback }
        set {
            if let newValue = newValue {
                observers.setObject(CallbackObject(newValue), forKey: target)
            } else {
                observers.removeObject(forKey: target)
            }
        }
    }
}
