//
//  ColorAlter.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation
import CoreImage

struct ColorAlter: Codable {    
    static func +(color: HSBColor, alter: ColorAlter) -> HSBColor {
        return HSBColor(
            hue: color.hue + alter.hue,
            saturation: color.saturation + alter.saturation,
            brightness: color.brightness + alter.brightness
        )
    }
    
    private static func normalize(_ component: Ratio) -> Ratio {
        return abs(component) * component
    }

    let hue: Ratio
    let saturation: Ratio
    let brightness: Ratio

    init(seed: Int) {
        srand48(seed)
        hue = ColorAlter.normalize(Ratio(drand48()) - 0.5)
        saturation = ColorAlter.normalize(Ratio(drand48()) - 0.5)
        brightness = ColorAlter.normalize(Ratio(drand48()) - 0.5)
    }
    
    init(hue: Ratio, saturation: Ratio, brightness: Ratio) {
        self.hue = hue
        self.saturation = saturation
        self.brightness = brightness
    }
}
