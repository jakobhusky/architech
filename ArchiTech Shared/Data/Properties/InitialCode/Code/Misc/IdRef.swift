//
//  IdRef.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///References a value by an identifier - the param is a phantom type
typealias IdRef<T> = Identifier
