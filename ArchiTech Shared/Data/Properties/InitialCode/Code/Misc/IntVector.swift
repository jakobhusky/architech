//
//  IntVector.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct IntVector: LosslessStringConvertible, Hashable, Codable {
    enum ParseError: Error {
        case bad
    }
    
    let x: Int
    let y: Int
    
    var description: String {
        return "\(x),\(y)"
    }
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
    
    init(_ cgPoint: CGPoint) {
        x = Int(cgPoint.x)
        y = Int(cgPoint.y)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        do {
            try self.init(string: try container.decode(String.self))
        } catch {
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Parse error")
        }
    }
    
    init?(_ description: String) {
        do {
            try self.init(string: description)
        } catch {
            return nil
        }
    }
    
    init(string: String) throws {
        let components = string.split(separator: ",")
        guard components.count == 2,
            let x = Int(components[0]),
            let y = Int(components[1]) else {
            throw ParseError.bad
        }
        
        self.x = x
        self.y = y
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(description)
    }
}
