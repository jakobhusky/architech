//
//  IdHashable.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/11/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol IdHashable: Hashable {
    var id: Identifier { get }
}

extension IdHashable {
    static func ==(_ lhs: Self, _ rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    var hashValue: Int {
        return id.hashValue
    }
}
