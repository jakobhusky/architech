//
//  Identifier.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///A unique identifier, like a pointer
typealias Identifier = UUID
