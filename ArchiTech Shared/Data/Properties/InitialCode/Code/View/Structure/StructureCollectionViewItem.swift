//
//  StructureCollectionViewItem.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class StructureCollectionViewItem: CollectionViewItem {
    var structureView: StructureThumbView {
        return view as! StructureThumbView
    }
}
