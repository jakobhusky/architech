//
//  StructureThumbView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

@IBDesignable class StructureThumbView: View, NibLoadable {
    @IBOutlet weak var tile: TileThumbView! = nil
    @IBOutlet weak var labelName: LabelView! = nil
    @IBOutlet weak var labelContext: LabelView! = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFromNib()
    }
}
