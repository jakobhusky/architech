//
//  TileFragThumbView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class TileFragThumbView: View {
    var image: Image? {
        didSet {
            needsDisplay = true
        }
    }
    
    override func draw(_ dirtyRect: NSRect) {
        let context = GraphicsContext.current!
        context.saveGraphicsState()

        context.imageInterpolation = .none
        context.shouldAntialias = false
        image?.draw(in: bounds)
        
        context.restoreGraphicsState()
    }
}
