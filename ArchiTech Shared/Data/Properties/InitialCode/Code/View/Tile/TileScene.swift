//
//  TileScene.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/27/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SKTiled

class TileScene: SKTiledScene {
    weak var tileSceneDelegate: TileSceneDelegate?
    private var _highlightedCoord: IntVector?
    
    var highlightedCoord: IntVector? {
        return _highlightedCoord
    }
    var isHighlighting: Bool {
        return highlightedCoord != nil
    }
    
    private func beginHighlight(event: NSEvent) {
        let coord = IntVector(tilemap.coordinateAtMouseEvent(event: event))
        guard tileSceneDelegate?.shouldBeginHighlight(coord: coord) ?? true else {
            return
        }
        
        _highlightedCoord = coord
    }
    
    private func updateHighlight(event: NSEvent) {
        let coord = IntVector(tilemap.coordinateAtMouseEvent(event: event))
        _highlightedCoord = coord
        tileSceneDelegate?.didUpdateHighlight(coord: coord)
    }
    
    private func endHighlight(event: NSEvent) {
        _highlightedCoord = nil
        tileSceneDelegate?.didEndHighlight()
    }
}

protocol TileSceneDelegate: AnyObject {
    func shouldBeginHighlight(coord: IntVector) -> Bool
    func didUpdateHighlight(coord: IntVector)
    func didEndHighlight()
}

#if os(macOS)
extension TileScene {
    override func mouseDown(with event: NSEvent) {
        super.mouseDown(with: event)
        
        beginHighlight(event: event)
    }
    
    override func mouseMoved(with event: NSEvent) {
        super.mouseMoved(with: event)
        
        if isHighlighting {
            updateHighlight(event: event)
        }
    }
    
    override func mouseDragged(with event: NSEvent) {
        super.mouseMoved(with: event)
        
        if isHighlighting {
            updateHighlight(event: event)
        }
    }
    
    override func mouseUp(with event: NSEvent) {
        super.mouseUp(with: event)
        
        if isHighlighting {
            endHighlight(event: event)
        }
    }
    
    override func mouseEntered(with event: NSEvent) {
        super.mouseEntered(with: event)
        
        if (NSEvent.pressedMouseButtons & 1) != 0 { //Left button pressed
            beginHighlight(event: event)
        }
    }
    
    override func mouseExited(with event: NSEvent) {
        super.mouseUp(with: event)
        
        if isHighlighting {
            endHighlight(event: event)
        }
    }
}
#endif
