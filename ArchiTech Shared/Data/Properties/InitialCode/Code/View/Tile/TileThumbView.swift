//
//  TileThumbView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class TileThumbView: View {
    @IBOutlet weak var building: TileFragThumbView! = nil
    @IBOutlet weak var road: TileFragThumbView! = nil
    @IBOutlet weak var plot: PlotThumbView! = nil
}
