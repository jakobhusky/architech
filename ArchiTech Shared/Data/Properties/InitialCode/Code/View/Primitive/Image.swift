//
//  Image.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

typealias Image = NSImage
#elseif os(iOS)
import UIKit

typealias Image = UIImage
#endif

extension Image {
    private static var cachedUrls: [URL:Image] = [:]
    
    ///Returns a cached image if loaded before from the same URL
    static func load(from url: URL) -> Image {
        return cachedUrls[url] ?? {
            let image = Image(byReferencing: url)
            cachedUrls[url] = image
            return image
        }()
    }
}
