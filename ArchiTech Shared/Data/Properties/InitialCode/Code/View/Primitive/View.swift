//
//  View.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

typealias View = NSView
#endif

protocol ViewDelegate: AnyObject {
#if os(OSX)
    func handles(key: Key) -> Bool
    
    func handle(key: Key)
#endif
}
