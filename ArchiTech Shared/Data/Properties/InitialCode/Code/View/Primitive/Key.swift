//
//  Key.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/10/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit
#endif

struct Key: Equatable {
#if os(OSX)
    typealias Modifiers = NSEvent.ModifierFlags
#endif

    let modifiers: Modifiers
    let characters: String
    
    init(modifiers: Modifiers, characters: String) {
        self.modifiers = modifiers
        self.characters = characters
    }
    
#if os(OSX)
    init(_ event: NSEvent) {
        modifiers = NSEvent.modifierFlags
        characters = event.charactersIgnoringModifiers!
    }
#endif
}
