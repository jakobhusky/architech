//
//  ImageView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

#if os(OSX)
import AppKit

class ImageView: NSView {
    private let imageView: NSImageView
    var image: NSImage? {
        get { return imageView.image }
        set { imageView.image = newValue }
    }
    
    override init(frame frameRect: NSRect) {
        imageView = NSImageView(frame: CGRect(origin: CGPoint.zero, size: frameRect.size))
        super.init(frame: frameRect)
        setupImageView()
    }
    
    required init?(coder decoder: NSCoder) {
        imageView = NSImageView()
        super.init(coder: decoder)
        imageView.frame = bounds
        setupImageView()
    }
    
    private func setupImageView() {
        imageView.autoresizingMask = [.width, .height]
        imageView.imageScaling = .scaleProportionallyUpOrDown
        addSubview(imageView)
    }
}
#endif
