//
//  NibLoadable.swift
//  ArchiTech macOS
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

//From https://stackoverflow.com/questions/9282365/load-view-from-an-external-xib-file-in-storyboard
protocol NibLoadable {
    static var nibName: String { get }
}

extension NibLoadable where Self: View {
    static var nibName: String {
        return String(describing: Self.self) // defaults to the name of the class implementing this protocol.
    }
    
    static var nib: Nib? {
        let bundle = Bundle(for: Self.self)
        return Nib(nibNamed: Self.nibName, bundle: bundle)
    }
    
    func setupFromNib() {
        var views: NSArray? = nil
        guard let nib = Self.nib,
            nib.instantiate(withOwner: self, topLevelObjects: &views),
            let view = views?.first(where: { $0 is View }) as? View else {
            fatalError("Error loading \(self) from nib")
        }
        
        view.frame = bounds
        addSubview(view)
        if autoresizesSubviews {
            view.translatesAutoresizingMaskIntoConstraints = false
#if os(macOS)
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
            view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
#endif
#if os(iOS)
            view.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
            view.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
            view.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
            view.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
#endif
        }
    }
}
