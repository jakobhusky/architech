//
//  Color.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

typealias Color = NSColor

extension Color {
    static var selectedContentBackground: Color {
        if #available(OSX 10.14, *) {
            return Color.selectedContentBackgroundColor
        } else {
            return Color.selectedTextBackgroundColor
        }
    }
    
    static func +(color: Color, alter: ColorAlter) -> Color {
        return Color(
            hue: color.hueComponent + CGFloat(alter.hue),
            saturation: color.saturationComponent + CGFloat(alter.saturation),
            brightness: color.brightnessComponent + CGFloat(alter.brightness),
            alpha: color.alphaComponent
        )
    }
    
    convenience init(_ hsbColor: HSBColor) {
        self.init(
            hue: CGFloat(hsbColor.hue),
            saturation: CGFloat(hsbColor.saturation),
            brightness: CGFloat(hsbColor.brightness),
            alpha: 1
        )
    }
}
#endif
