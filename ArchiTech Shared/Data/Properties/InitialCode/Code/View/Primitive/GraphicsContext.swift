//
//  GraphicsContext.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

typealias GraphicsContext = NSGraphicsContext
#endif
