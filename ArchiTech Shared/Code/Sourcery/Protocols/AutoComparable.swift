//
//  AutoComparable.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/16/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol AutoComparable: Comparable {}

extension AutoComparable where Self: RawRepresentable, Self.RawValue == Int {
    static func <(_ lhs: Self, _ rhs: Self) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
