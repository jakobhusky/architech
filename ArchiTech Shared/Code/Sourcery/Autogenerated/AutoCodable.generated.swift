// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


extension CodeFrag.Content {

    enum CodingKeys: String, CodingKey {
        case type
        case function
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if container.allKeys.contains(.type), try container.decodeNil(forKey: .type) == false {
            var associatedValues = try container.nestedUnkeyedContainer(forKey: .type)
            let associatedValue0 = try associatedValues.decode(CodeType.self)
            self = .type(associatedValue0)
            return
        }
        if container.allKeys.contains(.function), try container.decodeNil(forKey: .function) == false {
            var associatedValues = try container.nestedUnkeyedContainer(forKey: .function)
            let associatedValue0 = try associatedValues.decode(Function.self)
            self = .function(associatedValue0)
            return
        }
        throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case"))
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .type(associatedValue0):
            var associatedValues = container.nestedUnkeyedContainer(forKey: .type)
            try associatedValues.encode(associatedValue0)
        case let .function(associatedValue0):
            var associatedValues = container.nestedUnkeyedContainer(forKey: .function)
            try associatedValues.encode(associatedValue0)
        }
    }

}

extension CodeType {

    enum CodingKeys: String, CodingKey {
        case generic
        case alias
        case compound
        case base
        case params
        case type
        case parts
        case parents
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if container.allKeys.contains(.generic), try container.decodeNil(forKey: .generic) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .generic)
            let base = try associatedValues.decode(IdRef<CodeType>.self, forKey: .base)
            let params = try associatedValues.decode([IdRef<CodeType>].self, forKey: .params)
            self = .generic(base: base, params: params)
            return
        }
        if container.allKeys.contains(.alias), try container.decodeNil(forKey: .alias) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .alias)
            let base = try associatedValues.decode(IdRef<CodeType>?.self, forKey: .base)
            self = .alias(base: base)
            return
        }
        if container.allKeys.contains(.compound), try container.decodeNil(forKey: .compound) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .compound)
            let type = try associatedValues.decode(SpecificType.self, forKey: .type)
            let parts = try associatedValues.decode([Field].self, forKey: .parts)
            let parents = try associatedValues.decode([IdRef<CodeType>].self, forKey: .parents)
            self = .compound(type: type, parts: parts, parents: parents)
            return
        }
        throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case"))
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .generic(base, params):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .generic)
            try associatedValues.encode(base, forKey: .base)
            try associatedValues.encode(params, forKey: .params)
        case let .alias(base):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .alias)
            try associatedValues.encode(base, forKey: .base)
        case let .compound(type, parts, parents):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .compound)
            try associatedValues.encode(type, forKey: .type)
            try associatedValues.encode(parts, forKey: .parts)
            try associatedValues.encode(parents, forKey: .parents)
        }
    }

}

extension CodeType.SpecificType {

    enum CodingKeys: String, CodingKey {
        case record
        case variant
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.record.rawValue: self = .record
        case CodingKeys.variant.rawValue: self = .variant
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .record: try container.encode(CodingKeys.record.rawValue)
        case .variant: try container.encode(CodingKeys.variant.rawValue)
        }
    }

}

extension Corner {

    enum CodingKeys: String, CodingKey {
        case right
        case up
        case left
        case down
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.right.rawValue: self = .right
        case CodingKeys.up.rawValue: self = .up
        case CodingKeys.left.rawValue: self = .left
        case CodingKeys.down.rawValue: self = .down
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .right: try container.encode(CodingKeys.right.rawValue)
        case .up: try container.encode(CodingKeys.up.rawValue)
        case .left: try container.encode(CodingKeys.left.rawValue)
        case .down: try container.encode(CodingKeys.down.rawValue)
        }
    }

}

extension DocFrag {

    enum CodingKeys: String, CodingKey {
        case description
        case parameters
        case returnAndThrow
        case warningOrConstraint
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.description.rawValue: self = .description
        case CodingKeys.parameters.rawValue: self = .parameters
        case CodingKeys.returnAndThrow.rawValue: self = .returnAndThrow
        case CodingKeys.warningOrConstraint.rawValue: self = .warningOrConstraint
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .description: try container.encode(CodingKeys.description.rawValue)
        case .parameters: try container.encode(CodingKeys.parameters.rawValue)
        case .returnAndThrow: try container.encode(CodingKeys.returnAndThrow.rawValue)
        case .warningOrConstraint: try container.encode(CodingKeys.warningOrConstraint.rawValue)
        }
    }

}

extension PhysicalCategory {

    enum CodingKeys: String, CodingKey {
        case house
        case commercial
        case industry
        case outdoor
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.house.rawValue: self = .house
        case CodingKeys.commercial.rawValue: self = .commercial
        case CodingKeys.industry.rawValue: self = .industry
        case CodingKeys.outdoor.rawValue: self = .outdoor
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .house: try container.encode(CodingKeys.house.rawValue)
        case .commercial: try container.encode(CodingKeys.commercial.rawValue)
        case .industry: try container.encode(CodingKeys.industry.rawValue)
        case .outdoor: try container.encode(CodingKeys.outdoor.rawValue)
        }
    }

}

extension Plot.Kind {

    enum CodingKeys: String, CodingKey {
        case normal
        case dynamic
        case wildcard
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.normal.rawValue: self = .normal
        case CodingKeys.dynamic.rawValue: self = .dynamic
        case CodingKeys.wildcard.rawValue: self = .wildcard
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .normal: try container.encode(CodingKeys.normal.rawValue)
        case .dynamic: try container.encode(CodingKeys.dynamic.rawValue)
        case .wildcard: try container.encode(CodingKeys.wildcard.rawValue)
        }
    }

}

extension Side {

    enum CodingKeys: String, CodingKey {
        case upRight
        case upLeft
        case downLeft
        case downRight
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.upRight.rawValue: self = .upRight
        case CodingKeys.upLeft.rawValue: self = .upLeft
        case CodingKeys.downLeft.rawValue: self = .downLeft
        case CodingKeys.downRight.rawValue: self = .downRight
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .upRight: try container.encode(CodingKeys.upRight.rawValue)
        case .upLeft: try container.encode(CodingKeys.upLeft.rawValue)
        case .downLeft: try container.encode(CodingKeys.downLeft.rawValue)
        case .downRight: try container.encode(CodingKeys.downRight.rawValue)
        }
    }

}

extension SignatureType {

    enum CodingKeys: String, CodingKey {
        case generic
        case concrete
        case variable
        case base
        case params
        case type
        case label
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if container.allKeys.contains(.generic), try container.decodeNil(forKey: .generic) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .generic)
            let base = try associatedValues.decode(SignatureType.self, forKey: .base)
            let params = try associatedValues.decode([SignatureType].self, forKey: .params)
            self = .generic(base: base, params: params)
            return
        }
        if container.allKeys.contains(.concrete), try container.decodeNil(forKey: .concrete) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .concrete)
            let type = try associatedValues.decode(IdRef<CodeType>.self, forKey: .type)
            self = .concrete(type: type)
            return
        }
        if container.allKeys.contains(.variable), try container.decodeNil(forKey: .variable) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .variable)
            let label = try associatedValues.decode(String.self, forKey: .label)
            self = .variable(label: label)
            return
        }
        throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case"))
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .generic(base, params):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .generic)
            try associatedValues.encode(base, forKey: .base)
            try associatedValues.encode(params, forKey: .params)
        case let .concrete(type):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .concrete)
            try associatedValues.encode(type, forKey: .type)
        case let .variable(label):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .variable)
            try associatedValues.encode(label, forKey: .label)
        }
    }

}

extension Statement {

    enum CodingKeys: String, CodingKey {
        case access
        case call
        case target
        case field
        case type
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        if container.allKeys.contains(.access), try container.decodeNil(forKey: .access) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .access)
            let target = try associatedValues.decode(IdRef<CodeType>.self, forKey: .target)
            let field = try associatedValues.decode(Field.self, forKey: .field)
            let type = try associatedValues.decode(AccessType.self, forKey: .type)
            self = .access(target: target, field: field, type: type)
            return
        }
        if container.allKeys.contains(.call), try container.decodeNil(forKey: .call) == false {
            let associatedValues = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .call)
            let target = try associatedValues.decode(IdRef<Function>.self, forKey: .target)
            self = .call(target: target)
            return
        }
        throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case"))
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        switch self {
        case let .access(target, field, type):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .access)
            try associatedValues.encode(target, forKey: .target)
            try associatedValues.encode(field, forKey: .field)
            try associatedValues.encode(type, forKey: .type)
        case let .call(target):
            var associatedValues = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .call)
            try associatedValues.encode(target, forKey: .target)
        }
    }

}

extension Statement.AccessType {

    enum CodingKeys: String, CodingKey {
        case get
        case set
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.get.rawValue: self = .get
        case CodingKeys.set.rawValue: self = .set
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .get: try container.encode(CodingKeys.get.rawValue)
        case .set: try container.encode(CodingKeys.set.rawValue)
        }
    }

}

extension TileLayer {

    enum CodingKeys: String, CodingKey {
        case regular
        case placing
    }

    internal init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()

        let enumCase = try container.decode(String.self)
        switch enumCase {
        case CodingKeys.regular.rawValue: self = .regular
        case CodingKeys.placing.rawValue: self = .placing
        default: throw DecodingError.dataCorrupted(.init(codingPath: decoder.codingPath, debugDescription: "Unknown enum case '\(enumCase)'"))
        }
    }

    internal func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        switch self {
        case .regular: try container.encode(CodingKeys.regular.rawValue)
        case .placing: try container.encode(CodingKeys.placing.rawValue)
        }
    }

}
