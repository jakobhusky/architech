// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

//Derived from AutoCases

extension SearchPrecedence {
  static func <(_ lhs: SearchPrecedence, _ rhs: SearchPrecedence) -> Bool {
      if (lhs.category < rhs.category) {
        return true
      }
      if (lhs.category > rhs.category) {
        return false
      }
      if (lhs.match < rhs.match) {
        return true
      }
      return false
  }
}

extension SearchPrecedence.Match {
  static func <(_ lhs: SearchPrecedence.Match, _ rhs: SearchPrecedence.Match) -> Bool {
    switch (lhs, rhs) {
    case (.exact, .exact):
      return false
    case (.exact, _):
      return true
    case (_, .exact):
      return false
    case (.prefix, .prefix):
      return false
    case (.prefix, _):
      return true
    case (_, .prefix):
      return false
    case (.contains, .contains):
      return false
    case (.contains, _):
      return true
    case (_, .contains):
      return false
    case (.containsSubsequence, .containsSubsequence):
      return false
    }
  }
}
extension StructureSearchCollectionController.LocalSearchPrecedenceCategory {
  static func <(_ lhs: StructureSearchCollectionController.LocalSearchPrecedenceCategory, _ rhs: StructureSearchCollectionController.LocalSearchPrecedenceCategory) -> Bool {
    switch (lhs, rhs) {
    case (.name, .name):
      return false
    case (.name, _):
      return true
    case (_, .name):
      return false
    case (.context, .context):
      return false
    case (.context, _):
      return true
    case (_, .context):
      return false
    case (.typeName, .typeName):
      return false
    }
  }
}
