//
//  PlotThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Plot, skipProperty = [id, type, color]
class PlotThumbController: AutoModelController {
    private static func image(forPlotType plotType: String) -> Image {
        return Image.load(from: Bundle.main.url(
            forResource: "Assets/Plots/\(plotType)",
            withExtension: "png"
        )!)
    }
    
    let decorations: Corner.Map<DecorationThumbController>
    
    required init(view: View) {
        self.view = view
        decorations = view.decorations.map { DecorationThumbController(view: $1) }
    }
    
    private func updateDownstream() {
        for corner in Corner.all {
            let decoration = decorations[corner]
            if let modelDecoration = model.decorations[corner] {
                decoration.model = modelDecoration
                decoration.view.isHidden = false
            } else {
                decoration.view.isHidden = true
            }
        }
        
        view.image = PlotThumbController.image(forPlotType: model.type)
        view.color = model.color.map(Color.init)
    }

// sourcery:inline:auto:PlotThumbController.AutoModelController
    // MARK: - PlotThumbController AutoModelController
    typealias Model = Plot
    typealias View = PlotThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }



    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
