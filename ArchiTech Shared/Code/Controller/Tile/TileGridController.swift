//
//  TileGridController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SKTiled
import SpriteKit

class TileGridController {
    private struct TileGroupLayer {
        let skGroupLayer: SKGroupLayer
        
        let children: [SKTileLayer]
        let decorations: Corner.Map<SKTileLayer>
        let building: SKTileLayer
        let road: SKTileLayer
        let plot: SKTileLayer

        init(_ skGroupLayer: SKGroupLayer) {
            self.skGroupLayer = skGroupLayer
            children = [SKTileLayer](skGroupLayer.childLayers.dropFirst().map { $0 as! SKTileLayer })
            decorations = Corner.Map(
                right: children.first { $0.name == "Decorations.Right" }!,
                up: children.first { $0.name == "Decorations.Up" }!,
                left: children.first { $0.name == "Decorations.Left" }!,
                down: children.first { $0.name == "Decorations.Down" }!
            )
            building = children.first { $0.name == "Buildings" }!
            road = children.first { $0.name == "Roads" }!
            plot = children.first { $0.name == "Plots" }!
        }
    }
    
    private static func label(of tilesetData: SKTilesetData) -> String {
        return ((tilesetData.source as NSString).lastPathComponent as NSString).deletingPathExtension
    }
    
    private static func decorationLabel(of tilesetData: SKTilesetData) -> String {
        return tilesetData.source.matchingStrings(regex: "^.+/Decorations/(.*).png$", options: [])[0][1]
    }
    
    var _model: TileGrid! {
        didSet {
            _observers.notify()
        }
    }
    var model: TileGrid {
        get { return _model }
        set {
            _model = newValue
            
            for groupLayer in groupLayers.values {
                clearSk(groupLayer: groupLayer)
            }
            
            for (loc, tile) in model {
                addSk(tile: tile, at: loc)
            }
        }
    }
    private let _observers: Observers<TileGrid>.Notifyable
    let tilemap: SKTilemap
    private let groupLayers: [TileLayer:TileGroupLayer]
    private let tileset: SKTileset
    private let decorationGids: [String:Int]
    private let buildingGids: [String:Int]
    private let roadGids: [String:Int]
    private let plotGids: [String:Int]
    private let plotBaseGid: Int
    private(set) var tilemapHalfLength: Int {
        didSet {
            assert(tilemapHalfLength > oldValue)
            let oldTilemapLength = oldValue * 2
            let diff = tilemapHalfLength - oldValue
            tilemap.size = CGSize(width: tilemapLength, height: tilemapLength)
            tilemap.position.x -= diff * tilemap.tileWidth
            tilemap.position.y -= diff * tilemap.tileWidth
            for layer in tilemap.layers {
                if let tileLayer = layer as? SKTileLayer {
                    var tiles: [SKTile?] = Array(repeating: nil, count: oldTilemapLength * oldTilemapLength)
                    for x in 0..<oldTilemapLength {
                        for y in 0..<oldTilemapLength {
                            tiles[x + (y * oldTilemapLength)] = tileLayer.tileAt(x, y)
                        }
                    }
                    tileLayer.clearLayer()
                    for x in 0..<oldTilemapLength {
                        for y in 0..<oldTilemapLength {
                            if let tile = tiles[x + (y * oldTilemapLength)] {
                                let _ = tileLayer.setTile(x + diff, y + diff, tile: tile)
                                tile.position.x += diff * tilemap.tileWidth
                                tile.position.y += diff * tilemap.tileWidth
                                tileLayer.addChild(tile)
                            }
                        }
                    }
                }
            }
        }
    }
    
    var observers: Observers<TileGrid> { return _observers }
    var tilemapLength: Int {
        return tilemapHalfLength * 2
    }
    
    init(tilemap: SKTilemap) {
        self.tilemap = tilemap
        _observers = Observers<TileGrid>.Notifyable()
        groupLayers = [
            .placing: TileGroupLayer(tilemap.getLayers(named: "Placing").first as! SKGroupLayer),
            .regular: TileGroupLayer(tilemap.getLayers(named: "Regular").first as! SKGroupLayer)
        ]
        tileset = tilemap.tilesets.first!
        decorationGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "decoration"),
            by: TileGridController.decorationLabel
        ).mapValues { $0.id + 1 }
        buildingGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "building"),
            by: TileGridController.label
        ).mapValues { $0.id + 1 }
        roadGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "road"),
            by: TileGridController.label
        ).mapValues { $0.id + 1 }
        plotGids = [String:SKTilesetData](
            keying: tileset.getTileData(ofType: "plot"),
            by: TileGridController.label
        ).mapValues { $0.id + 1 }
        plotBaseGid = tileset.getTileData(ofType: "plot_base").first!.id + 1
        tilemapHalfLength = Int(tilemap.sizeHalved.width)
        
        tilemap.setScale(Constants.Display.Size.Tile.scale)
    }
    
    func add(tiles: [IntVector:Tile], to layer: TileLayer) {
        for (pos, tile) in tiles {
            add(tile: tile, at: TileLocation(layer: layer, position: pos))
        }
    }
    
    func add(tile: Tile, at location: TileLocation) {
        _model[location] = tile
        addSk(tile: tile, at: location)
    }
    
    func clear(layer: TileLayer) {
        _model.tiles[layer] = nil
        clearSk(groupLayer: groupLayers[layer]!)
    }
    
    func remove(tileAt location: TileLocation) {
        _model[location] = nil
        removeSk(tileAt: location.position, groupLayer: groupLayers[location.layer]!)
    }
    
    private func addSk(tile: Tile, at location: TileLocation) {
        let pos = location.position
        resizeTilemapIfNecessary(for: pos)
        let groupLayer = groupLayers[location.layer]!
        //TODO get corners for tile
        for corner in Corner.all {
            if let decoration = tile.plot.decorations[corner] {
                //TODO Color alter
                addSk(
                    tileWithGid: decorationGids[decoration.fullType]!,
                    plotColor: nil,
                    at: pos,
                    to: groupLayer.decorations[corner]
                )
            }
        }
        if let building = tile.building {
            addSk(
                tileWithGid: buildingGids[building.type]!,
                plotColor: nil,
                at: pos,
                to: groupLayer.building
            )
        }
        if let road = tile.road {
            addSk(
                tileWithGid: roadGids[road.type]!,
                plotColor: nil,
                at: pos,
                to: groupLayer.road
            )
            fatalError("TODO Roads not yet implemented")
        }
        //TODO Colorize
        addSk(
            tileWithGid: plotGids[tile.plot.type]!,
            plotColor: tile.plot.color,
            at: pos,
            to: groupLayer.plot
        )
    }
    
    private func addSk(
        tileWithGid gid: Int,
        plotColor: HSBColor?,
        at pos: IntVector,
        to tileLayer: SKTileLayer
    ) {
        let tile = tileLayer.addTileAt(pos.x + tilemapHalfLength, pos.y + tilemapHalfLength, gid: gid)!
        //TODO This is a hack - why doesn't texture normally show up?
        let tileImage = SKSpriteNode(texture: tile.texture)
        if let plotColor = plotColor {
            let tileColorImage = SKSpriteNode(texture: Constants.Assets.Plot.shapeTexture)
            tileColorImage.color = Color(plotColor)
            tileColorImage.colorBlendFactor = 1
            tile.addChild(tileColorImage)
        }
        if let tintString = tileLayer.properties["tint"] {
            let tintStringComponents = tintString.split(separator: "|").map(String.init)
            tileImage.color = Color(hexString: tintStringComponents[0])
            tileImage.colorBlendFactor = CGFloat(Ratio(tintStringComponents[1])!)
        }
        tile.zPosition = CGFloat(pos.y + pos.x)
        tile.addChild(tileImage)
    }
    
    private func clearSk(groupLayer: TileGroupLayer) {
        for tileLayer in groupLayer.children {
            for x in 0..<Int(tileLayer.size.width) {
                for y in 0..<Int(tileLayer.size.height) {
                    let _ = tileLayer.removeTileAt(x, y)
                }
            }
        }
    }
    
    private func removeSk(tileAt pos: IntVector, groupLayer: TileGroupLayer) {
        for tileLayer in groupLayer.children {
            let _ = tileLayer.removeTileAt(pos.x + tilemapHalfLength, pos.y + tilemapHalfLength)
        }
    }
    
    private func resizeTilemapIfNecessary(for pos: IntVector) {
        let minHalfLength = (max(abs(pos.x), abs(pos.y)) + 1).roundUpLog2
        if tilemapHalfLength < minHalfLength {
            tilemapHalfLength = minHalfLength
        }
    }
}
