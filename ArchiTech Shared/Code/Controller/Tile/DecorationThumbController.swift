//
//  DecorationThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/15/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Decoration, view = TileFragThumbView, skipProperties
class DecorationThumbController: AutoModelController {
    private static func image(forDecorationType fullType: String) -> Image {
        return Image.load(from: Bundle.main.url(
            forResource: "Assets/Decorations/\(fullType)",
            withExtension: "png"
        )!)
    }
    
    private func updateDownstream() {
        view.image = DecorationThumbController.image(forDecorationType: model.fullType)
    }
    
    // sourcery:inline:auto:DecorationThumbController.AutoModelController
    // MARK: - DecorationThumbController AutoModelController
    typealias Model = Decoration
    typealias View = TileFragThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view

    }


    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
    // sourcery:end
}
