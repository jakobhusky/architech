//
//  StructureThumbController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// sourcery: model = Structure, view = StructureThumbView, skipProperty = [codeFrag, location], guardModelUpdate
class StructureThumbController: AutoModelController {
    let tile: TileThumbController
    
    private func updateDownstream() {
        if let name = model.codeFrag.label.name {
            view.labelName.stringValue = name
            view.labelName.isHidden = false
        } else {
            view.labelName.isHidden = true
        }
        if let context = model.codeFrag.label.context {
            view.labelContext.stringValue = context
            view.labelContext.isHidden = false
        } else {
            view.labelContext.isHidden = true
        }
        tile.model = model.tile
    }

// sourcery:inline:auto:StructureThumbController.AutoModelController
    // MARK: - StructureThumbController AutoModelController
    typealias Model = Structure
    typealias View = StructureThumbView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            if (!blockDownstream && _model == newValue) {
                return
            }

            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view
        self.tile = TileThumbController(view: view.tile)

        tile.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.tile = self.tile.model
            }
        }

    }


    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
