//
//  StructureSearchCollectionController.swift
//  ArchiTech macOS
//
//  Created by Jakob Hain on 12/16/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class StructureSearchCollectionController: ModelController, StructureCollectionControllerDelegate, SearchBar.Delegate {
    enum LocalSearchPrecedenceCategory: Int, Hashable, AutoComparable {
        case name
        case context
        case typeName
    }
    
    typealias LocalSearchPrecedence = SearchPrecedence<LocalSearchPrecedenceCategory>
    
    typealias Model = [Structure]
    typealias View = StructureSearchCollectionView
    
    var _model: [Structure]! {
        didSet {
            _observers.notify()
        }
    }
    var modelIndicesAllToShown: [Int?]?
    var modelIndicesShownToAll: [Int]?
    private let _observers: Observers<[Structure]>.Notifyable
    weak var delegate: StructureSearchCollectionControllerDelegate? = nil
    private(set) var selectedIndex: Int? {
        didSet {
            delegate?.didSelectItem(oldIndex: oldValue, newIndex: selectedIndex)
        }
    }
    let view: StructureSearchCollectionView
    let structureCollection: StructureCollectionController

    var model: [Structure] {
        get { return _model }
        set {
            _model = newValue
            updateDownstream()
        }
    }
    var searchText: String {
        return view.searchBar.stringValue
    }
    var observers: Observers<[Structure]> { return _observers }
    var selectedItem: Structure? {
        return selectedIndex.map { model[$0] }
    }
    var isSearching: Bool {
        return !searchText.isEmpty
    }
    
    required init(view: StructureSearchCollectionView) {
        _observers = Observers<[Structure]>.Notifyable()
        self.view = view
        self.structureCollection = StructureCollectionController(view: view.structureCollection)
        structureCollection.delegate = self
        view.searchBar.searchBarDelegate = self
    }
    
    func append(_ structure: Structure) {
        if !isSearching {
            _model.append(structure)
            structureCollection.append(structure)
        } else {
            model.append(structure)
        }
    }
    
    ///Skips the delegate
    func remove(_ structureId: Identifier) {
        remove(at: model.firstIndex { $0.id == structureId }!)
    }
    
    ///Skips the delegate
    func remove(at index: Int) {
        if !isSearching {
            _model.remove(at: index)
            structureCollection.remove(at: index)
        } else {
            model.remove(at: index)
        }
    }
    
    private func updateDownstream() {
        if !isSearching {
            //Show all elements
            modelIndicesShownToAll = nil
            modelIndicesAllToShown = nil
            structureCollection.model = model
        } else {
            //Only show elements matching criteria
            let shownIndexedModel = Dictionary(
                grouping: model.enumerated(),
                by: { searchPrecedence(structure: $0.element) }
            ).filter { $0.key != nil }.sorted { $0.key! < $1.key! }.flatMap { $0.value }
            modelIndicesShownToAll = shownIndexedModel.map { $0.offset }
            modelIndicesAllToShown = (0 ..< model.count).map(modelIndicesShownToAll!.firstIndex)
            structureCollection.model = shownIndexedModel.map { $0.element }
        }
    }
    
    private func modelIndexShownToAll(index: Int) -> Int {
        return isSearching ? modelIndicesShownToAll![index] : index
    }
    
    // MARK: - Collection controller delegate
    
    func tryRemove(index: Int) {
        delegate?.tryRemove(index: modelIndexShownToAll(index: index))
    }
    
    func didSelectItem(oldIndex: Int?, newIndex: Int?) {
        selectedIndex = newIndex.map(modelIndexShownToAll)
    }
    
    // MARK: - Search bar delegate
    
    func startSearch() {
        
    }
    
    func updateSearch() {
        updateDownstream()
    }
    
    func stopSearch() {
        
    }
    
    private func searchPrecedence(structure: Structure) -> LocalSearchPrecedence? {
        func tryMatch(category: LocalSearchPrecedenceCategory, content: String?) -> LocalSearchPrecedence? {
            return content.flatMap {
                SearchPrecedence.Match(content: $0, search: searchText)
            }.map { SearchPrecedence(category: category, match: $0) }
        }
        
        return tryMatch(category: .name, content: structure.codeFrag.label.name) ??
            tryMatch(category: .context, content: structure.codeFrag.label.context) ??
            delegate?.typeLabels(for: structure).lazy.compactMap {
                tryMatch(category: .typeName, content: $0.name)
            }.first
    }
}

protocol StructureSearchCollectionControllerDelegate: StructureCollectionControllerDelegate {
    func typeLabels(for structure: Structure) -> [Label]
}
