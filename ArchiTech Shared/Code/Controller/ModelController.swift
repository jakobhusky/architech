//
//  ModelController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/1/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///Autoderives upstream and downstream syncing
protocol ModelController {
    associatedtype Model
    associatedtype View
    
    var model: Model { get }
    var observers: Observers<Model> { get }
    var view: View { get }
    
    init(view: View)
}
