//
//  WorldController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SKTiled

// sourcery: model = World, skipProperties
class WorldController: AutoModelController {
    private class PlacerAdapter: PlacerDelegate {
        private weak var worldController: WorldController!
        
        var tileGrid: TileGrid {
            return worldController.tileGrid.model
        }
        
        init(worldController: WorldController) {
            self.worldController = worldController
        }
        
        func place(structure: Structure) {
            DispatchQueue.main.async {
                self.worldController.place(structure: structure)
            }
        }
        
        func place(tile: Tile, at location: TileLocation) {
            DispatchQueue.main.async {
                self.worldController.place(tile: tile, at: location)
            }
        }
        
        func pause() {
            usleep(Constants.Placer.pauseDelay)
        }
    }
    
    let scene: TileScene
    let unplacedStructures: StructureSearchCollectionController
    let placedStructures: StructureSearchCollectionController
    private(set) var tileGrid: TileGridController!
    private var placerAdapter: PlacerAdapter!
    
    required init(view: WorldView) {
        self.view = view
        unplacedStructures = StructureSearchCollectionController(view: view.unplacedStructures)
        placedStructures = StructureSearchCollectionController(view: view.placedStructures)
        let sceneView = view.scene!
        scene = TileScene(size: sceneView.bounds.size)
        placerAdapter = PlacerAdapter(worldController: self)
        
        //Assumes unplaced and placed structures are kept in sync, so
        //  if one is removed from unplaced, it's added to placed
        //unplacedStructures.observers[self] = updateUpstreamStructures
        //placedStructures.observers[self] = updateUpstreamStructures
        scene.scaleMode = .resizeFill
        sceneView.presentScene(scene)
        scene.setup(
            url: Bundle.main.url(forResource: "EmptyMap", withExtension: "tmx")!,
            withTilesets: [SKTileset(fileNamed: "TileSet")],
            ignoreProperties: false,
            loggingLevel: .debug,
            { tilemap in
                print("Loaded scene")
                self.tileGrid = TileGridController(tilemap: tilemap)
                self.tileGrid.observers[self] = {
                    if (!self.ignoreDownstream) {
                        self.model.tileGrid = self.tileGrid.model
                    }
                }
                if let model = self._model {
                    self.tileGrid.model = model.tileGrid
                }
            }
        )
    }
    
    func autoPlace() {
        DispatchQueue.global(qos: .background).async {
            let placer = VoronoiPlacer()
            placer.place(structures: self.model.placableStructures, delegate: self.placerAdapter)
        }
    }
    
    func tryPlace(structure: Structure) {
        if Validate.canPlace(structure: structure, in: tileGrid.model) {
            place(structure: structure)
        }
    }
    
    func place(structure: Structure) {
        performDownstreamOperations {
            self.placedStructures.append(structure)
            self.unplacedStructures.remove(structure.id)
            var tile = structure.tile
            if !tile.isBase {
                let base = self.tileGrid.model[structure.location!]!
                tile.plot.decorations = base.plot.decorations
            }
            self.tileGrid.add(tile: tile, at: structure.location!)
        }
    }
    
    func place(tile: Tile, at location: TileLocation) {
        performDownstreamOperations {
            self.tileGrid.add(tile: tile, at: location)
        }
    }
    
    func tryRemovePlaced(index: Int) {
        var unplacedStructure = placedStructures.model[index]
        let oldLocation = unplacedStructure.location!
        unplacedStructure.location = nil
        
        performDownstreamOperations {
            if unplacedStructure.tile.isBase {
                self.tileGrid.remove(tileAt: oldLocation)
            } else {
                var tile = self.tileGrid.model[oldLocation]!
                self.tileGrid.remove(tileAt: oldLocation)
                tile.building = nil
                self.tileGrid.add(tile: tile, at: oldLocation)
            }
            self.unplacedStructures.append(unplacedStructure)
            self.placedStructures.remove(at: index)
            
            if unplacedStructure.tile.isBase,
                let overlappingIndex = self.placedStructures.model.firstIndex(where: {
                    $0.location == oldLocation
                }) {
                self.tryRemovePlaced(index: overlappingIndex)
            }
        }
    }
    
    func setPlacingStructure(_ placingStructure: Structure?) {
        performDownstreamOperations {
            self.tileGrid.clear(layer: .placing)
            
            if let placingStructure = placingStructure {
                self.tileGrid.add(tile: placingStructure.tile, at: placingStructure.location!)
            }
        }
    }
    
    func typeLabels(for structure: Structure) -> [Label] {
        guard let typeId = structure.tile.plot.id else {
            return []
        }
        
        return Array(model.iddStructures[typeId].map { $0.codeFrag.label })
    }
    
    private func updateDownstream() {
        let placableStructures = model.placableStructures
        unplacedStructures.model = placableStructures.filter { $0.location == nil }
        placedStructures.model = placableStructures.filter { $0.location != nil }
        
        if let tileGrid = self.tileGrid {
            tileGrid.model = model.tileGrid
        }
    }

// sourcery:inline:auto:WorldController.AutoModelController
    // MARK: - WorldController AutoModelController
    typealias Model = World
    typealias View = WorldView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View

    var observers: Observers<Model> { return _observers }



    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
// sourcery:end
}
