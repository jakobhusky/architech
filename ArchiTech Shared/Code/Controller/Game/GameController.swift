//
//  GameController.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SpriteKit

// sourcery: model = Game
class GameController: AutoModelController {
    private class WorldTileSceneAdapter: TileSceneDelegate {
        private weak var gameController: GameController!
        
        init(gameController: GameController) {
            self.gameController = gameController
        }
        
        func shouldBeginHighlight(coord: IntVector) -> Bool {
            return gameController.shouldBeginHighlight(coord: coord)
        }
        
        func didUpdateHighlight(coord: IntVector) {
            gameController.didUpdateHighlight(coord: coord)
        }
        
        func didEndHighlight() {
            gameController.didEndHighlight()
        }
    }
    
    private class UnplacedStructuresAdapter: StructureSearchCollectionControllerDelegate {
        private weak var gameController: GameController!
        
        init(gameController: GameController) {
            self.gameController = gameController
        }
        
        func typeLabels(for structure: Structure) -> [Label] {
            return gameController.typeLabels(for: structure)
        }
        
        func tryRemove(index: Int) {
            //Ignore
        }
        
        func didSelectItem(oldIndex: Int?, newIndex: Int?) {
            //Ignore
        }
    }
    
    private class PlacedStructuresAdapter: StructureSearchCollectionControllerDelegate {
        private weak var gameController: GameController!
        
        init(gameController: GameController) {
            self.gameController = gameController
        }
        
        func typeLabels(for structure: Structure) -> [Label] {
            return gameController.typeLabels(for: structure)
        }
        
        func tryRemove(index: Int) {
            return gameController.tryRemovePlaced(index: index)
        }
        
        func didSelectItem(oldIndex: Int?, newIndex: Int?) {
            //Ignore
        }
    }
    
    // sourcery: substitute = selectionId
    let selection: StructureInfoController
    // sourcery: substitute = placingStructureId
    let placingStructure: PlacingStructureThumbController
    private var worldTileSceneAdapter: WorldTileSceneAdapter!
    private var unplacedStructuresAdapter: UnplacedStructuresAdapter!
    private var placedStructuresAdapter: PlacedStructuresAdapter!

    func autoPlace() {
        world.autoPlace()
    }
    
    private func setup() {
        assert(_model == nil)
        worldTileSceneAdapter = WorldTileSceneAdapter(gameController: self)
        unplacedStructuresAdapter = UnplacedStructuresAdapter(gameController: self)
        placedStructuresAdapter = PlacedStructuresAdapter(gameController: self)
        
        selection.observers[self] = {
            if (!self.ignoreDownstream) {
                self._model.selectionId = self.selection.model.id
            }
        }
        placingStructure.observers[self] = {
            if (!self.ignoreDownstream) {
                self._model.placingStructureId = self.placingStructure.model?.id
            }
        }

        world.scene.tileSceneDelegate = worldTileSceneAdapter
        world.unplacedStructures.delegate = unplacedStructuresAdapter
        world.placedStructures.delegate = placedStructuresAdapter
    }
    
    // MARK: - World tile scene delegate

    func shouldBeginHighlight(coord: IntVector) -> Bool {
        guard var selectedStructure = world.unplacedStructures.selectedItem else {
            return false
        }
        
        selectedStructure.location = TileLocation(layer: .placing, position: coord)
        placingStructure.model = selectedStructure
        updatePlacingTiles()
        return true
    }
    
    func didUpdateHighlight(coord: IntVector) {
        placingStructure.model!.location!.position = coord
        updatePlacingTiles()
    }

    func didEndHighlight() {
        tryPlace()
        placingStructure.model = nil
        updatePlacingTiles()
    }

    private func tryPlace() {
        var placedStructure = placingStructure.model!
        placedStructure.location!.layer = .regular
        
        world.tryPlace(structure: placedStructure)
    }

    private func updatePlacingTiles() {
        world.setPlacingStructure(placingStructure.model)
    }

    // MARK: - Unplaced and placed structures delegate
    
    func typeLabels(for structure: Structure) -> [Label] {
        return world.typeLabels(for: structure)
    }
    
    // MARK: - Placed structures delegate

    func tryRemovePlaced(index: Int) {
        world.tryRemovePlaced(index: index)
    }

    // sourcery:inline:auto:GameController.AutoModelController
    // MARK: - GameController AutoModelController
    typealias Model = Game
    typealias View = GameView

    private var _model: Model! {
        didSet {
            _observers.notify()
        }
    }
    var model: Model {
        get { return _model }
        set {
            _model = newValue;
            if (!blockDownstream) {
                directlyPerformDownstreamOperations(updateDownstream)
            }
        }
    }
    private var ignoreDownstream: Bool = false
    private var blockDownstream: Bool = false
    private let _observers: Observers<Model>.Notifyable = Observers<Model>.Notifyable()
    let view: View
    let code: CodeController
    let world: WorldController

    var observers: Observers<Model> { return _observers }

    required init(view: View) {
        self.view = view
        self.code = CodeController(view: view.code)
        self.world = WorldController(view: view.world)
        self.selection = StructureInfoController(view: view.selection)
        self.placingStructure = PlacingStructureThumbController(view: view.placingStructure)

        code.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.code = self.code.model
            }
        }
        world.observers[self] = { [unowned self] in
            if (!self.ignoreDownstream) {
                self._model.world = self.world.model
            }
        }

        setup()
    }

    private func updateDownstream() {
        code.model = model.code
        world.model = model.world
        if let modelSelection = model.selection {
            selection.model = modelSelection
            selection.view.isHidden = false
        } else {
            selection.view.isHidden = true
        }
        if let modelPlacingStructure = model.placingStructure {
            placingStructure.model = modelPlacingStructure
            placingStructure.view.isHidden = false
        } else {
            placingStructure.view.isHidden = true
        }
    }

    private func directlyPerformDownstreamOperations(_ operations: () -> Void) {
        ignoreDownstream = true
        operations()
        ignoreDownstream = false
    }

    func performDownstreamOperations(_ operations: () -> Void) {
        blockDownstream = true
        operations()
        blockDownstream = false
    }
    // sourcery:end
}
