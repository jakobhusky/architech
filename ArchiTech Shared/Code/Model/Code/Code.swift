//
//  Code.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Code: Equatable, AutoADT {
    private struct Base {
        // sourcery: derives = iddFrags, dependsOn = frags
        fileprivate static func createIddFrags(frags: Set<CodeFrag>) -> [IdRef<CodeFrag>:CodeFrag] {
            return Dictionary(keying: Array(frags), by: { $0.id })
        }
        
        var frags: Set<CodeFrag>
        var duplicates: [CodeDuplicate]
    }
    
    static let empty: Code = Code(frags: [], duplicates: [])
    
    static func +(_ lhs: Code, _ rhs: Code) -> Code {
        return Code(
            frags: lhs.frags.union(rhs.frags),
            duplicates: lhs.duplicates + rhs.duplicates
        )
    }
    
    static func +=(_ lhs: inout Code, _ rhs: Code) {
        lhs.frags.formUnion(rhs.frags)
        lhs.duplicates += rhs.duplicates
    }

// sourcery:inline:auto:Code.AutoADT
    // MARK: - Code AutoADT
    enum CodingKeys: CodingKey {
        case frags
        case duplicates
    }


    var frags: Set<CodeFrag> {
        didSet {
            iddFrags = Base.createIddFrags(
                frags: frags
            )
        }
    }
    var duplicates: [CodeDuplicate]

    private(set) var iddFrags: [IdRef<CodeFrag>:CodeFrag]


    init(
        frags: Set<CodeFrag>, 
        duplicates: [CodeDuplicate]
    ) {
        self.frags = frags
        self.duplicates = duplicates

        iddFrags = Base.createIddFrags(
            frags: frags
        )
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        frags = try container.decode(Set<CodeFrag>.self, forKey: .frags)
        duplicates = try container.decode([CodeDuplicate].self, forKey: .duplicates)

        iddFrags = Base.createIddFrags(
            frags: frags
        )
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(frags, forKey: .frags)
        try container.encode(duplicates, forKey: .duplicates)
    }
// sourcery:end
}
