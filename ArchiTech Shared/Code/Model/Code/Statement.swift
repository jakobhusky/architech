//
//  Statement.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum Statement: Equatable, AutoCodable {
    enum AccessType: Equatable, AutoCodable {
        case get
        case set
    }
    
    case access(target: IdRef<CodeType>, field: Field, type: AccessType)
    case call(target: IdRef<Function>)
}
