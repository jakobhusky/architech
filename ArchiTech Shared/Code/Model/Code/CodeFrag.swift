//
//  CodeFrag.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct CodeFrag: IdHashable, Codable {
    static let universalBuiltins: [CodeFrag] = [.unknownType]
    static let unknownType: CodeFrag = CodeFrag(
        label: Label(
            id: Identifier(uuidString: "815a3fdd-876c-4ce5-9e5d-293ba06bf822")!,
            name: "#unknown",
            context: nil
        ),
        region: nil,
        docs: [],
        tests: [],
        isBuiltin: true,
        content: .type(CodeType.unknown)
    )
    
    enum Content: Equatable, AutoCodable {
        case type(CodeType)
        case function(Function)
    }
    
    let label: Label
    let region: SourceRegion?
    let docs: [DocFrag]
    let tests: [Test]
    let isBuiltin: Bool
    let content: Content
    
    var id: Identifier {
        return label.id
    }
}
