//
//  Label.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Label: IdHashable, Codable, SeedSerializable {
    static let unknownContext: String = "<unknown>"

    let id: Identifier
    var name: String?
    var context: String?
    
    var seedData: Data {
        return Seed((name, context)).data
    }
}
