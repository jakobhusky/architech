//
//  CodeType.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum CodeType: Equatable, AutoCodable {
    static let unknown: CodeType = .alias(base: nil)
    static let primitive: CodeType = .alias(base: nil)
    ///"True" void type - 0 instances exist, a function which returns this void never returns at all, it always hangs or crashes
    static let void: CodeType = .compound(type: .variant, parts: [], parents: [])
    ///"Fake" void type - 1 instance exists, what most functions mean when they "return void"
    static let unit: CodeType = .compound(type: .record, parts: [], parents: [])

    enum SpecificType: Equatable, AutoCodable {
        case record
        case variant
    }
    
    case generic(base: IdRef<CodeType>, params: [IdRef<CodeType>])
    case alias(base: IdRef<CodeType>?)
    case compound(type: SpecificType, parts: [Field], parents: [IdRef<CodeType>])
}
