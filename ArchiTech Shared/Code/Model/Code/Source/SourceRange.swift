//
//  SourceRange.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct SourceRange: Equatable, Codable {
    enum CodingKeys: CodingKey {
        case fileUrl
        case start
        case end
    }
    
    let fileUrl: URL
    let start: SourcePos
    let end: SourcePos
}
