//
//  World.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct World: AutoADT {
    struct Context: Codable {
        let tile: Tile.Context
    }
    
    private struct Base {
        // sourcery: derives = iddStructures, dependsOn = structures
        fileprivate static func createIddStructures(structures: [Structure]) -> [Identifier:Structure] {
            return [Identifier:Structure](keying: structures, by: { $0.id })
        }
        
        // sourcery: derives = unplacableStructures, dependsOn = structures
        fileprivate static func createUnplacableStructures(structures: [Structure]) -> [Structure] {
            return structures.filter { !isPlacable(structure: $0, allStructures: structures) }
        }
        
        // sourcery: derives = placableStructures, dependsOn = structures
        fileprivate static func createPlacableStructures(structures: [Structure]) -> [Structure] {
            return structures.filter { isPlacable(structure: $0, allStructures: structures) }
        }
        
        private static func isPlacable(structure: Structure, allStructures: [Structure]) -> Bool {
            return
                !structure.codeFrag.isBuiltin ||
                (structure.tile.isBase && allStructures.contains {
                    guard !$0.codeFrag.isBuiltin,
                        let plotId = $0.tile.plot.id else {
                        return false
                    }
                    
                    return structure.tile.plot.fits(plotId: plotId)
                })
        }
        
        var structures: [Structure]
        var tileGrid: TileGrid
    }

// sourcery:inline:auto:World.AutoADT
    // MARK: - World AutoADT
    enum CodingKeys: CodingKey {
        case structures
        case tileGrid
    }


    var structures: [Structure] {
        didSet {
            iddStructures = Base.createIddStructures(
                structures: structures
            )
            unplacableStructures = Base.createUnplacableStructures(
                structures: structures
            )
            placableStructures = Base.createPlacableStructures(
                structures: structures
            )
        }
    }
    var tileGrid: TileGrid

    private(set) var iddStructures: [Identifier:Structure]
    private(set) var unplacableStructures: [Structure]
    private(set) var placableStructures: [Structure]


    init(
        structures: [Structure], 
        tileGrid: TileGrid
    ) {
        self.structures = structures
        self.tileGrid = tileGrid

        iddStructures = Base.createIddStructures(
            structures: structures
        )
        unplacableStructures = Base.createUnplacableStructures(
            structures: structures
        )
        placableStructures = Base.createPlacableStructures(
            structures: structures
        )
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        structures = try container.decode([Structure].self, forKey: .structures)
        tileGrid = try container.decode(TileGrid.self, forKey: .tileGrid)

        iddStructures = Base.createIddStructures(
            structures: structures
        )
        unplacableStructures = Base.createUnplacableStructures(
            structures: structures
        )
        placableStructures = Base.createPlacableStructures(
            structures: structures
        )
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(structures, forKey: .structures)
        try container.encode(tileGrid, forKey: .tileGrid)
    }
// sourcery:end
}
