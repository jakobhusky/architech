//
//  Decoration.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Decoration: Codable {
    typealias OptCornerMap = Corner.CodableMap<Decoration?>
    
    struct Info: Codable {
        let value: Int
        let corners: [Corner]
        let plotTypes: [String]
        let category: PhysicalCategory
        let numVarieties: Int?
    }
    
    static func buildingShownCorners(plotPos: IntVector?) -> [Corner] {
        guard let plotPos = plotPos else {
            return Corner.all
        }
        
        var random = Random(seed: Seed(plotPos))
        //Assumes these are assigned in sequence (they are)
        let probabilities = Corner.Map { _ in random.nextRatio() }
        return Corner.all.filter { probabilities[$0] < Constants.Decoration.buildingShownRatio }
    }
    
    let type: String
    let variety: Int?
    
    var fullType: String {
        if let variety = variety {
            return "\(type)/var\(String(variety))"
        } else {
            return type
        }
    }
}
