//
//  Tile.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Tile: Codable {
    struct Context: Codable {
        let plots: [String:Plot.Info]
        let buildings: [String:Building.Info]
        let roads: Set<String>
        let decorations: [String:Decoration.Info]
    }
    
    var building: Building?
    var road: Road?
    var plot: Plot
    
    var isBase: Bool {
        return building == nil
    }
    
    var base: Tile {
        var result = self
        result.building = nil
        return result
    }
}
