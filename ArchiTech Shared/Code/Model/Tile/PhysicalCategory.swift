//
//  PhysicalCategory.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum PhysicalCategory: AutoCodable {
    case house
    case commercial
    case industry
    case outdoor
}
