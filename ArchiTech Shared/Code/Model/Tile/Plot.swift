//
//  Plot.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Plot: Codable {
    enum Kind: AutoCodable {
        case normal
        case dynamic
        case wildcard
    }

    struct Info: Codable {
        let kind: Kind
        let baseColor: HSBColor?
        let colorAlterMax: ColorRange?

        func color(alter: ColorAlter) -> HSBColor? {
            return baseColor.map { $0 + colorAlterMax!.constrain(alterMultiplier: alter) }
        }
    }

    var id: Identifier?
    let type: String
    let color: HSBColor?
    var decorations: Decoration.OptCornerMap
    
    func fits(plotId: Identifier) -> Bool {
        return id == nil || id! == plotId
    }
}
