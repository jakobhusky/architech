//
//  TileGrid.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct TileGrid: Collection, Codable {
    struct Index: Hashable, Comparable {
        static func <(_ lhs: TileGrid.Index, _ rhs: TileGrid.Index) -> Bool {
            if lhs.layer != rhs.layer {
                return lhs.layer < rhs.layer
            }
            //nil position index is max
            guard let lhsPosition = lhs.position else {
                return false
            }
            guard let rhsPosition = rhs.position else {
                return true
            }
            
            return lhsPosition < rhsPosition
        }
        
        let layer: Dictionary<TileLayer, [IntVector:Tile]>.Index
        let position: Dictionary<IntVector, Tile>.Index?
    }
    
    struct Iterator: IteratorProtocol {
        typealias Element = TileGrid.Element
        private struct LayerIterator {
            var layer: TileLayer
            var positionIterator: Dictionary<IntVector, Tile>.Iterator
            
            init(raw: (key: TileLayer, value: [IntVector:Tile])) {
                layer = raw.key
                positionIterator = raw.value.makeIterator()
            }
        }
        
        private var layerIterator: Dictionary<TileLayer, [IntVector:Tile]>.Iterator
        private var curLayer: LayerIterator?
        
        init(tiles: [TileLayer:[IntVector:Tile]]) {
            layerIterator = tiles.makeIterator()
            curLayer = layerIterator.next().map(LayerIterator.init)
        }
        
        mutating func next() -> Element? {
            if curLayer == nil {
                return nil
            }
            
            if let next = curLayer!.positionIterator.next() {
                return (
                    location: TileLocation(layer: curLayer!.layer, position: next.key),
                    tile: next.value
                )
            } else {
                curLayer = layerIterator.next().map(LayerIterator.init)
                return next()
            }
        }
    }
    
    typealias Element = (location: TileLocation, tile: Tile)
    
    var tiles: [TileLayer:[IntVector:Tile]]
    
    var startIndex: Index {
        return Index(
            layer: tiles.startIndex,
            position: tiles.first?.value.startIndex
        )
    }
    
    var endIndex: Index {
        return Index(
            layer: tiles.endIndex,
            position: nil
        )
    }
    
    subscript(location: TileLocation) -> Tile? {
        get { return tiles[location.layer]?[location.position] }
        set {
            if tiles[location.layer] == nil {
                tiles[location.layer] = [:]
            }
            tiles[location.layer]![location.position] = newValue
        }
    }
    
    subscript(index: Index) -> Element {
        let (key: layer, value: layerTiles) = tiles[index.layer]
        let (key: position, value: tile) = layerTiles[index.position!]
        return (location: TileLocation(layer: layer, position: position), tile: tile)
    }
    
    func fullPlot(at pos: TileLocation) -> [IntVector:Tile]? {
        guard let baseTile = self[pos] else {
            return nil
        }
        guard let plotId = baseTile.plot.id else {
            return [IntVector.zero: baseTile]
        }
        
        var exclude: Set<IntVector> = []
        return Dictionary(uniqueKeysWithValues: fullPlot(
            plotId: plotId,
            base: pos,
            offset: IntVector.zero,
            exclude: &exclude
        ))
    }
    
    private func fullPlot(
        plotId: Identifier,
        base: TileLocation,
        offset: IntVector,
        exclude: inout Set<IntVector>
    ) -> [(IntVector, Tile)] {
        guard exclude.insert(offset).inserted,
            let curTile = self[base + offset],
            curTile.plot.fits(plotId: plotId) else {
            return []
        }
        
        return [(offset, curTile)] + offset.adjacents.flatMap {
            fullPlot(plotId: plotId, base: base, offset: $0, exclude: &exclude)
        }
    }
    
    func plotPos(absPos: TileLocation) -> IntVector? {
        return fullPlot(at: absPos)?.filter { $0.value.building == nil }.keys.min {
            return (CGPoint($0).magnitude <~< CGPoint($1).magnitude) ?? ($0.y < $1.y)
        }
    }
    
    func index(after i: Index) -> Index {
        guard let iPosition = i.position else {
            fatalError("Tried to get index past end (\(i)) in a TileGrid:\n\(self)")
        }
        
        let layerTiles = tiles[i.layer].value
        if iPosition == layerTiles.endIndex {
            let nextLayerIndex = tiles.index(after: i.layer)
            return Index(
                layer: nextLayerIndex,
                position: (nextLayerIndex == tiles.endIndex) ? nil : tiles[nextLayerIndex].value.startIndex
            )
        } else {
            return Index(
                layer: i.layer,
                position: layerTiles.index(after: iPosition)
            )
        }
    }
    
    func makeIterator() -> Iterator {
        return Iterator(tiles: tiles)
    }
}
