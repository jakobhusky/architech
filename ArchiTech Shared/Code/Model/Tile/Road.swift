//
//  Road.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Road: Codable {
    struct Sides: OptionSet, Codable {
        let rawValue: Int
        
        static let center = Sides(rawValue: 1 << 0)
        static let up = Sides(rawValue: 1 << 1)
        static let down = Sides(rawValue: 1 << 2)
        static let left = Sides(rawValue: 1 << 3)
        static let right = Sides(rawValue: 1 << 4)
    }
    
    let sides: Sides
    let type: String
}
