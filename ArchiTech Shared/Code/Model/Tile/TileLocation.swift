//
//  TileLocation.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/26/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct TileLocation: Equatable, Codable {
    static func +(_ location: TileLocation, _ offset: IntVector) -> TileLocation {
        return TileLocation(
            layer: location.layer,
            position: location.position + offset
        )
    }
    
    var layer: TileLayer
    var position: IntVector
    
    var adjacents: [TileLocation] {
        return position.adjacents.map {
            TileLocation(
                layer: layer,
                position: $0
            )
        }
    }
}
