//
//  RawSeedSerializable.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/17/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///Only inherit if the type doesn't have any references (e.g. it's a primitive structure)
protocol RawSeedSerializable: SeedSerializable { }

extension RawSeedSerializable {
    var seedData: Data {
        var copy = self
        //From https://stackoverflow.com/questions/28680589/how-to-convert-an-int-into-nsdata-in-swift
        return Data(bytes: &copy, count: MemoryLayout<Self>.stride)
    }
}

extension Int: RawSeedSerializable { }

extension Float: RawSeedSerializable { }

extension IntVector: RawSeedSerializable { }
