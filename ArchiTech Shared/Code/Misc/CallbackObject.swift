//
//  CallbackObject.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/1/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///Wraps a callback into a class type
class CallbackObject {
    let callback: () -> Void
    
    init(_ callback: @escaping () -> Void) {
        self.callback = callback
    }
}
