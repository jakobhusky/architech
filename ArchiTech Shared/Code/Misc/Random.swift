//
//  Random.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/17/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import GameplayKit

struct Random {
    private let source: GKARC4RandomSource
    
    init(seed: Seed) {
        source = GKARC4RandomSource(seed: seed.data)
    }
    
    mutating func nextInt() -> Int {
        return source.nextInt()
    }
    
    mutating func nextInt(upperBound: Int) -> Int {
        return source.nextInt(upperBound: upperBound)
    }
    
    mutating func nextRatio() -> Ratio {
        return source.nextUniform()
    }
    
    mutating func nextBool() -> Bool {
        return source.nextBool()
    }
    
    mutating func nextElem<T: Collection>(_ elems: T) -> T.Element {
        return elems[elems.index(elems.startIndex, offsetBy: nextInt(upperBound: elems.count))]
    }
    
    mutating func nextColorAlter() -> ColorAlter {
        return ColorAlter(
            hue: nextRatio(),
            saturation: nextRatio(),
            brightness: nextRatio()
        )
    }
    
    mutating func drop(count: Int) {
        source.dropValues(count)
    }
}
