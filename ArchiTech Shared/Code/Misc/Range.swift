//
//  Range.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Range<T: Numeric & Codable>: Codable {
    static func *(_ lhs: Range<T>, _ rhs: T) -> Range<T> {
        return Range(min: lhs.min * rhs, max: lhs.max * rhs)
    }
    
    let min: T
    let max: T
}

extension Range where T == Ratio {
    func middle(at ratio: Ratio) -> Ratio {
        return ((max - min) * ratio) + min
    }
}
