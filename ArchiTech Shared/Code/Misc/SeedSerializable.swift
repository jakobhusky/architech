//
//  SeedSerializable.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/17/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol SeedSerializable {
    var seedData: Data { get }
}

extension String: SeedSerializable {
    var seedData: Data {
        return data(using: .utf8)!
    }
}

extension Optional: SeedSerializable where Wrapped: SeedSerializable {
    var seedData: Data {
        return self?.seedData ?? Data()
    }
}
