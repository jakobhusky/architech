//
//  Corner.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum Corner: AutoCodable {
    struct Map<T>: CornerMap {
        typealias Element = T
        
        var right: T
        var up: T
        var left: T
        var down: T
        
        init(right: T, up: T, left: T, down: T) {
            self.right = right
            self.up = up
            self.left = left
            self.down = down
        }
    }
    
    struct CodableMap<T: Codable>: CornerMap, Codable {
        typealias Element = T
        
        var right: T
        var up: T
        var left: T
        var down: T
        
        init(right: T, up: T, left: T, down: T) {
            self.right = right
            self.up = up
            self.left = left
            self.down = down
        }
    }
    
    struct EquatableCodableMap<T: Equatable & Codable>: CornerMap, Equatable, Codable {
        typealias Element = T
        
        var right: T
        var up: T
        var left: T
        var down: T
        
        init(right: T, up: T, left: T, down: T) {
            self.right = right
            self.up = up
            self.left = left
            self.down = down
        }
    }
    
    struct Set: OptionSet {
        static let right = Set(rawValue: 1 << 0)
        static let up    = Set(rawValue: 1 << 1)
        static let left  = Set(rawValue: 1 << 2)
        static let down  = Set(rawValue: 1 << 3)

        static let all: Set = [.right, .up, .left, .down]

        let rawValue: Int
        
        init(rawValue: Int) {
            self.rawValue = rawValue
        }
        
        init(_ corner: Corner) {
            switch corner {
            case .right:
                rawValue = Set.right.rawValue
            case .up:
                rawValue = Set.up.rawValue
            case .left:
                rawValue = Set.left.rawValue
            case .down:
                rawValue = Set.down.rawValue
            }
        }
    }
    
    static let all: [Corner] = [.right, .up, .left, .down]
    
    case right
    case up
    case left
    case down
}
