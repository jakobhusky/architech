//
//  CornerMap.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol CornerMap {
    associatedtype Element
    
    var right: Element { get set }
    var up: Element { get set }
    var left: Element { get set }
    var down: Element { get set }
    
    init(right: Element, up: Element, left: Element, down: Element)
}

extension CornerMap {
    init(valueGetter: (Corner) throws -> Element) rethrows {
        self.init(
            right: try valueGetter(.right),
            up: try valueGetter(.up),
            left: try valueGetter(.left),
            down: try valueGetter(.down)
        )
    }
    
    subscript(_ corner: Corner) -> Element {
        get {
            switch corner {
            case .right:
                return right
            case .up:
                return up
            case .left:
                return left
            case .down:
                return down
            }
        } set {
            switch corner {
            case .right:
                right = newValue
            case .up:
                up = newValue
            case .left:
                left = newValue
            case .down:
                down = newValue
            }
        }
    }
    
    func map<CornerMap2: CornerMap, Element2>(
        _ transformer: (Corner, Element) throws -> Element2
    ) rethrows -> CornerMap2 where CornerMap2.Element == Element2 {
        return try CornerMap2 { try transformer($0, self[$0]) }
    }
}
