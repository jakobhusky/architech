//
//  ADT.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/13/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

///A very simple structure. Has auto-generated derived properties and protocol conformance
protocol ADT: Codable {}

