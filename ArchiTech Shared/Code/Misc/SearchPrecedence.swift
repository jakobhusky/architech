//
//  SearchPrecedence.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/16/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct SearchPrecedence<Category: Hashable & Comparable>: Hashable, AutoComparable {
    enum Match: Hashable, AutoComparable {
        case exact
        case prefix
        case contains
        case containsSubsequence
        
        init?(content: String, search: String) {
            if content == search {
                self = .exact
            } else if content.hasPrefix(search) {
                self = .prefix
            } else if content.contains(search) {
                self = .contains
            } else if content.containsSubsequence(search) {
                self = .containsSubsequence
            } else {
                return nil
            }
        }
    }
    
    let category: Category
    let match: Match
}
