//
//  Seed.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/17/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Seed: SeedSerializable {
    let data: Data
    
    var seedData: Data {
        return data
    }
    
    init(_ value: SeedSerializable) {
        data = value.seedData
    }
    
    init<T: Sequence>(_ sequence: T) where T.Element: SeedSerializable {
        var curData = Data()
        for elem in sequence {
            curData.append(elem.seedData)
        }
        data = curData
    }

    init(_ tuple2: (SeedSerializable, SeedSerializable)) {
        data = tuple2.0.seedData + tuple2.1.seedData
    }
}
