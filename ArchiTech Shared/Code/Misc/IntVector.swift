//
//  IntVector.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct IntVector: LosslessStringConvertible, Hashable, Codable {
    enum ParseError: Error {
        case bad
    }
    
    static let zero: IntVector = IntVector(x: 0, y: 0)
    
    static func +(_ lhs: IntVector, _ rhs: IntVector) -> IntVector {
        return IntVector(
            x: lhs.x + rhs.x,
            y: lhs.y + rhs.y
        )
    }
    
    ///Coordinates for all points on the border of a box centered at the origin with the given size,
    ///generated in a "vortex" pattern
    static func boxBorder(halfSize: Int) -> [[IntVector]] {
        assert(halfSize >= 0)
        if halfSize == 0 {
            return [[IntVector.zero]]
        } else {
            var result: [[IntVector]] = []
            result.append([
                IntVector(x: -halfSize, y: halfSize),
                IntVector(x: halfSize, y: -halfSize)
            ])
            result += ((-halfSize + 1)...(halfSize - 1)).map { [
                IntVector(x: $0, y: halfSize),
                IntVector(x: halfSize, y: $0),
                IntVector(x: -$0, y: -halfSize),
                IntVector(x: -halfSize, y: -$0)
            ] }
            result.append([
                IntVector(x: -halfSize, y: -halfSize),
                IntVector(x: halfSize, y: halfSize)
            ])
            return result
        }
    }
    
    let x: Int
    let y: Int
    
    var adjacents: [IntVector] {
        return [
            IntVector(x: x + 1, y: y),
            IntVector(x: x, y: y + 1),
            IntVector(x: x - 1, y: y),
            IntVector(x: x, y: y - 1)
        ]
    }
    
    var description: String {
        return "\(x),\(y)"
    }
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
    
    init(_ cgPoint: CGPoint) {
        x = Int(cgPoint.x)
        y = Int(cgPoint.y)
    }

    init(_ cgSize: CGSize) {
        x = Int(cgSize.width)
        y = Int(cgSize.height)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        do {
            try self.init(string: try container.decode(String.self))
        } catch {
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Parse error")
        }
    }
    
    init?(_ description: String) {
        do {
            try self.init(string: description)
        } catch {
            return nil
        }
    }
    
    init(string: String) throws {
        let components = string.split(separator: ",")
        guard components.count == 2,
            let x = Int(components[0]),
            let y = Int(components[1]) else {
            throw ParseError.bad
        }
        
        self.x = x
        self.y = y
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(description)
    }
}
