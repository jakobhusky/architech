//
//  Collection.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/10/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension Collection {
    //From https://stackoverflow.com/questions/25329186/safe-bounds-checked-array-lookup-in-swift-through-optional-bindings
    ///Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
