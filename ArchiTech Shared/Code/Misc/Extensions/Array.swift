//
//  Array.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/10/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension Array {
    init(_ optional: Element?) {
        if let value = optional {
            self.init(value)
        } else {
            self.init()
        }
    }
    
    mutating func pad(to newLength: Int, with element: Element) {
        while count < newLength {
            append(element)
        }
    }
}
