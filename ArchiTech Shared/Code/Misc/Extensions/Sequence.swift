//
//  Sequence.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/16/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension Sequence where Element: Equatable {
    ///Contains all elements of the other sequence in order, but not neccessarily connected
    func containsSubsequence<T: Collection>(_ subsequence: T) -> Bool where T.Element == Element {
        var found: T.Index = subsequence.startIndex
        for elem in self {
            if subsequence[found] == elem {
                found = subsequence.index(after: found)
                if found == subsequence.endIndex {
                    return true
                }
            }
        }
        return false
    }
}
