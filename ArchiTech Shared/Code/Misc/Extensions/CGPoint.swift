//
//  CGPoint.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension CGPoint {
    static func +(_ base: CGPoint, _ offset: CGSize) -> CGPoint {
        return CGPoint(x: base.x + offset.width, y: base.y + offset.height)
    }
    
    static func -(_ base: CGPoint, _ offset: CGSize) -> CGPoint {
        return CGPoint(x: base.x - offset.width, y: base.y - offset.height)
    }
    
    var magnitude: CGFloat {
        return hypot(x, y)
    }
    
    init(_ intPoint: IntVector) {
        self.init(x: intPoint.x, y: intPoint.y)
    }
}
