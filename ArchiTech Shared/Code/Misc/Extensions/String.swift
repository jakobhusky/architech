//
//  String.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/10/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension String {
    static let backspace: String = "\u{7F}"
    
    ///If the string has the prefix, removes it
    func strip(prefix: String) -> String? {
        if hasPrefix(prefix) {
            return String(self[index(startIndex, offsetBy: prefix.count)...])
        } else {
            return nil
        }
    }
    
    //Copied from Lars Blumberg at https://stackoverflow.com/questions/27880650/swift-extract-regex-matches
    func matchingStrings(regex: String, options: NSRegularExpression.Options) -> [[String]] {
        guard let regex = try? NSRegularExpression(pattern: regex, options: options) else { return [] }
        let nsString = self as NSString
        let results  = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
        return results.map { result in
            (0..<result.numberOfRanges).map {
                result.range(at: $0).location != NSNotFound
                    ? nsString.substring(with: result.range(at: $0))
                    : ""
            }
        }
    }
}
