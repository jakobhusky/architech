//
//  Dictionary.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension Dictionary {
    ///Fails if there are duplicate keys
    static func +(_ lhs: [Key:Value], _ rhs: [Key:Value]) -> [Key:Value] {
        var res = lhs
        for (key, value) in rhs {
            assert(res[key] == nil, "Duplicate key in merge: \(String(describing: key))")
            res[key] = value
        }
        return res
    }
    
    init(keys: Set<Key>, valueGetter: (Key) throws -> Value?) rethrows {
        self.init()
        for key in keys {
            self[key] = try valueGetter(key)
        }
    }
    
    ///Like grouping, but asserts every key is unique, and values aren't stored in arrays
    init(keying elems: [Value], by keyGetter: (Value) throws -> Key) rethrows {
        self.init()
        for elem in elems {
            let key = try keyGetter(elem)
            assert(self[key] == nil, "Multiple values for key \(key):\n\(self[key]!)\n\(elem)")
            self[key] = elem
        }
    }
    
    ///Like keying, but each value can have multiple (or no) keys
    init(keying elems: [Value], byMulti keysGetter: (Value) throws -> [Key]) rethrows {
        self.init()
        for elem in elems {
            for key in try keysGetter(elem) {
                assert(self[key] == nil)
                self[key] = elem
            }
        }
    }
    
    ///Mapped keys must be unique
    func mapKeys<Key2: Hashable>(_ transformer: (Key) throws -> Key2) rethrows -> [Key2:Value] {
        return try [Key2:Value](uniqueKeysWithValues: map { (try transformer($0.key), $0.value) })
    }
    
    ///Creates the value and sets it if and only if an existing value isn't defined
    mutating func getOrAdd(key: Key, default getDefault: @autoclosure () throws -> Value) rethrows -> Value {
        if let old = self[key] {
            return old
        } else {
            let new = try getDefault()
            self[key] = new
            return new
        }
    }
}
