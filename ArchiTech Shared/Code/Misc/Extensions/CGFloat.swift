//
//  CGFloat.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

infix operator =~=: ComparisonPrecedence
infix operator <~<: ComparisonPrecedence

extension CGFloat {
    static let lowTolerance: CGFloat = 0.001
    
    static func =~=(_ lhs: CGFloat, _ rhs: CGFloat) -> Bool {
        return (lhs - rhs).magnitude < CGFloat.lowTolerance
    }
    
    ///Returns nil if the lhs and rhs are almost equal, so they could just differ by floating-point approximation
    static func <~<(_ lhs: CGFloat, _ rhs: CGFloat) -> Bool? {
        if lhs =~= rhs {
            return nil
        } else {
            return lhs < rhs
        }
    }
}
