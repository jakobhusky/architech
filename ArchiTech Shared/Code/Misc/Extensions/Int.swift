//
//  Int.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/14/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

extension Int {
    static func pow(_ base: Int, _ power: Int) -> Int {
        assert(power >= 0, "raising integer by negative power yields fraction - convert to floating-point")
        if power == 0 {
            return 1
        } else {
            return base * Int.pow(base, power - 1)
        }
    }
    
    var roundUpLog2: Int {
        assert(self > 0)
        var result: Int = 1
        while result < self {
            result *= 2
        }
        return result
    }
    
    func isRoot(_ power: Int) -> Bool {
        for i in 0... {
            let test = Int.pow(i, power)
            if test == self {
                return true
            } else if test > self {
                return false
            }
        }
        fatalError("Ran out of natural numbers")
    }
}
