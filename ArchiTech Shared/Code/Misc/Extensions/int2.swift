//
//  int2.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/18/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import GameplayKit

extension int2 {
    init(_ intVector: IntVector) {
        self.init(x: intVector.x, y: intVector.y)
    }

    init(x: Int, y: Int) {
        self.init(x: Int32(x), y: Int32(y))
    }
}
