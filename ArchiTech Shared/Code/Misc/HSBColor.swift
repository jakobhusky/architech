//
//  HSBColor.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/29/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct HSBColor: Equatable, Codable {
    let hue: Ratio
    let saturation: Ratio
    let brightness: Ratio
}
