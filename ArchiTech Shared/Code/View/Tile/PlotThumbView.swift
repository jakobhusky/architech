//
//  PlotThumbView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/30/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class PlotThumbView: View {
    @IBOutlet weak var rightDecoration: TileFragThumbView! = nil
    @IBOutlet weak var upDecoration: TileFragThumbView! = nil
    @IBOutlet weak var leftDecoration: TileFragThumbView! = nil
    @IBOutlet weak var downDecoration: TileFragThumbView! = nil
    var image: Image? {
        didSet {
            needsDisplay = true
        }
    }
    var color: Color? {
        didSet {
            needsDisplay = true
        }
    }
    
    var decorations: Corner.Map<TileFragThumbView> {
        return Corner.Map(
            right: rightDecoration,
            up: upDecoration,
            left: leftDecoration,
            down: downDecoration
        )
    }
    
    override func draw(_ dirtyRect: NSRect) {
        let context = GraphicsContext.current!
        context.saveGraphicsState()
        
        context.imageInterpolation = .none
        context.shouldAntialias = false
        if let color = color {
            color.setFill()
            context.cgContext.clip(
                to: bounds,
                mask: Constants.Assets.Plot.shapeImage.cgImage(
                    forProposedRect: &bounds,
                    context: context,
                    hints: nil
                )!
            )
            context.cgContext.fill(bounds)
        }
        image?.draw(in: bounds)
            
        context.restoreGraphicsState()
    }
}
