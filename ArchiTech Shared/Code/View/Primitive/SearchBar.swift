//
//  SearchBar.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/16/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

class SearchBar: NSSearchField {
    private class DelegateAdapter: NSObject, NSSearchFieldDelegate {
        weak var searchBarDelegate: SearchBarDelegate!
        
        init(searchBarDelegate: SearchBarDelegate) {
            self.searchBarDelegate = searchBarDelegate
        }
        
        func searchFieldDidStartSearching(_ sender: NSSearchField) {
            searchBarDelegate.startSearch()
        }
        
        func controlTextDidChange(_ obj: Notification) {
            searchBarDelegate.updateSearch()
        }
        
        func searchFieldDidEndSearching(_ sender: NSSearchField) {
            searchBarDelegate.stopSearch()
        }
    }
    
    typealias Delegate = SearchBarDelegate
    
    var itemIdentifier: CollectionView.Item.Identifier!
    private var delegateAdapter: DelegateAdapter? {
        didSet {
            delegate = delegateAdapter
        }
    }
    weak var searchBarDelegate: SearchBarDelegate? {
        didSet {
            delegateAdapter = searchBarDelegate.map(DelegateAdapter.init)
        }
    }
    weak var viewDelegate: ViewDelegate?
    
    override func performKeyEquivalent(with event: NSEvent) -> Bool {
        return viewDelegate?.handles(key: Key(event)) ?? super.performKeyEquivalent(with: event)
    }
    
    override func keyDown(with event: NSEvent) {
        super.keyDown(with: event)
        viewDelegate?.handle(key: Key(event))
    }

}
#endif

protocol SearchBarDelegate: AnyObject {
    func startSearch()
    func updateSearch()
    func stopSearch()
}
