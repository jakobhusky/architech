//
//  CollectionView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

class CollectionView: NSCollectionView {
    private class DelegateAdapter: NSObject, NSCollectionViewDataSource, NSCollectionViewDelegate {
        weak var collectionViewDelegate: CollectionViewDelegate!
        
        init(collectionViewDelegate: CollectionViewDelegate) {
            self.collectionViewDelegate = collectionViewDelegate
        }
        
        func numberOfSections(in collectionView: NSCollectionView) -> Int {
            return collectionViewDelegate.numberOfSections
        }
        
        func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
            return collectionViewDelegate.numberOfItems(inSection: section)
        }
        
        func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
            let collectionView = collectionView as! CollectionView
            let item = collectionView.makeItem(
                withIdentifier: collectionView.itemIdentifier,
                for: indexPath
            )
            
            collectionViewDelegate.configure(item: item, at: indexPath)
            
            return item
        }
        
        func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
            collectionViewDelegate.didSelectItems(at: indexPaths)
        }
    }
    
    typealias Delegate = CollectionViewDelegate
    typealias Item = NSCollectionViewItem

    var itemIdentifier: CollectionView.Item.Identifier!
    private var delegateAdapter: DelegateAdapter? {
        didSet {
            dataSource = delegateAdapter
            delegate = delegateAdapter
        }
    }
    weak var collectionViewDelegate: CollectionViewDelegate? {
        didSet {
            delegateAdapter = collectionViewDelegate.map(DelegateAdapter.init)
        }
    }
    weak var viewDelegate: ViewDelegate?
    
    override func performKeyEquivalent(with event: NSEvent) -> Bool {
        return viewDelegate?.handles(key: Key(event)) ?? super.performKeyEquivalent(with: event)
    }
    
    override func keyDown(with event: NSEvent) {
        super.keyDown(with: event)
        viewDelegate?.handle(key: Key(event))
    }
}

extension CollectionView.Item {
    typealias Identifier = NSUserInterfaceItemIdentifier
}
#endif

protocol CollectionViewDelegate: AnyObject {
    var numberOfSections: Int { get }
    
    func numberOfItems(inSection section: Int) -> Int
    func configure(item: CollectionView.Item, at indexPath: IndexPath)
    func didSelectItems(at indexPaths: Set<IndexPath>)
}
