//
//  Nib.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

typealias Nib = NSNib
#endif

#if os(iOS)
import UIKit

typealias Nib = UINib
#endif
