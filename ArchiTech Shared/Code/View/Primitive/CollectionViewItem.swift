//
//  CollectionViewItem.swift
//  ArchiTech
//
//  Created by Jakob Hain on 11/10/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

#if os(OSX)
import AppKit

class CollectionViewItem: NSCollectionViewItem {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            if isViewLoaded {
                view.layer?.cornerRadius = cornerRadius
            }
        }
    }
    override var isSelected: Bool {
        didSet {
            if isSelected {
                view.layer?.backgroundColor = Color.selectedContentBackground.cgColor
            } else {
                view.layer?.backgroundColor = nil
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view.layer?.cornerRadius = cornerRadius
    }
}
#endif
