//
//  WorldView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SpriteKit

class WorldView: View {
    @IBOutlet weak var unplacedStructures: StructureSearchCollectionView! = nil
    @IBOutlet weak var placedStructures: StructureSearchCollectionView! = nil
    @IBOutlet weak var scene: SKView! = nil
}
