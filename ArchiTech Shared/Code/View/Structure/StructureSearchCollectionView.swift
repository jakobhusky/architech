//
//  StructureSearchCollectionView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/16/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

@IBDesignable class StructureSearchCollectionView: View, NibLoadable {
    @IBOutlet weak var searchBar: SearchBar! = nil
    @IBOutlet weak var structureCollection: StructureCollectionView! = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFromNib()
    }
}
