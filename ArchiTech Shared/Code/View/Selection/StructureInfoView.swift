//
//  StructureInfoView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class StructureInfoView: View {
    @IBOutlet weak var codeFragInfo: CodeFragInfoView! = nil
    @IBOutlet weak var tileInfo: TileInfoView! = nil
}
