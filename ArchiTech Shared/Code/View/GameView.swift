//
//  GameView.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/21/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class GameView: View {
    @IBOutlet weak var code: CodeView! = nil
    @IBOutlet weak var world: WorldView! = nil
    @IBOutlet weak var selection: StructureInfoView! = nil
    @IBOutlet weak var placingStructure: PlacingStructureThumbView! = nil
}
