//
//  Dependency.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Dependency {
    static func codeFrag(
        _ codeFrag: CodeFrag,
        dependsOn other: IdRef<CodeFrag>,
        context: [IdRef<CodeFrag>:CodeFrag]
    ) -> Bool {
        return Dependency.codeFrag(
            codeFrag,
            dependsOn: other,
            context: (
                frags: context,
                tested: []
            )
        )
    }
    
    private static func codeFrag(
        _ codeFrag: CodeFrag,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
    ) -> Bool {
        var tested = context.tested
        guard tested.insert(codeFrag.id).inserted else {
            return false
        }
        
        return codeFragContent(
            codeFrag.content,
            dependsOn: other,
            context: (frags: context.frags, tested: tested)
        )
    }
    
    static func codeFragContent(
        _ codeFragContent: CodeFrag.Content,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
    ) -> Bool {
        switch codeFragContent {
        case .type(let type):
            return codeType(type, dependsOn: other, context: context)
        case .function(let function):
            return
                signature(function.signature, dependsOn: other, context: context) ||
                body(function.body, dependsOn: other, context: context)
        }
    }
    
    static func codeType(
        _ codeType: CodeType,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
    ) -> Bool {
        switch codeType {
        case .generic(let base, let params):
            return
                codeFrag(context.frags[base]!, dependsOn: other, context: context) ||
                params.contains { codeFrag(context.frags[$0]!, dependsOn: other, context: context) }
        case .alias(let base):
            guard let base = base else {
                return false
            }
            return codeFrag(context.frags[base]!, dependsOn: other, context: context)
        case .compound(_, let parts, let parents):
            return parts.contains {
                codeFrag(context.frags[$0.type]!, dependsOn: other, context: context)
            } || parents.contains {
                codeFrag(context.frags[$0]!, dependsOn: other, context: context)
            }
        }
    }
    
    static func signature(
        _ signature: Function.Signature,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
    ) -> Bool {
        return signature.params.main.contains {
            signatureType($0, dependsOn: other, context: context)
        } || signature.params.other.contains {
            signatureType($0, dependsOn: other, context: context)
        } || signatureType(signature.returnType, dependsOn: other, context: context)
    }
    
    static func signatureType(
        _ signatureType: SignatureType,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
        ) -> Bool {
        switch signatureType {
        case .generic(let base, let params):
            return
                Dependency.signatureType(base, dependsOn: other, context: context) ||
                params.contains { Dependency.signatureType($0, dependsOn: other, context: context) }
        case .concrete(let type):
            return codeFrag(context.frags[type]!, dependsOn: other, context: context)
        case .variable(_):
            return false
        }
    }
    
    static func body(
        _ body: Function.Body,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
    ) -> Bool {
        return body.statements.contains {
            statement($0, dependsOn: other, context: context)
        }
    }
    
    static func statement(
        _ statement: Statement,
        dependsOn other: IdRef<CodeFrag>,
        context: (frags: [IdRef<CodeFrag>:CodeFrag], tested: Set<IdRef<CodeFrag>>)
    ) -> Bool {
        switch statement {
        case .access(let target, let field, _):
            return
                codeFrag(context.frags[target]!, dependsOn: other, context: context) ||
                codeFrag(context.frags[field.type]!, dependsOn: other, context: context)
        case .call(let target):
            return codeFrag(context.frags[target]!, dependsOn: other, context: context)
        }
    }
}
