//
//  Validate.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Validate {
    static func canPlace(structure: Structure, in tileGrid: TileGrid) -> Bool {
        return canHave(structure: structure, in: tileGrid, isPlacing: true)
    }

    static func canKeep(structure: Structure, in tileGrid: TileGrid) -> Bool {
        return canHave(structure: structure, in: tileGrid, isPlacing: false)
    }

    private static func canHave(
        structure: Structure,
        in tileGrid: TileGrid,
        isPlacing: Bool
    ) -> Bool {
        guard let location = structure.location else {
            return false
        }
        
        let prev = tileGrid[location]
        
        if structure.tile.isBase {
            return prev == nil
        } else {
            guard let base = prev,
                !isPlacing || base.isBase,
                structure.tile.plot.fits(plotId: base.plot.id!) else {
                return false
            }
            
            return true
        }
    }
    
    ///Whether this fragment is actually connected to source, or e.g. the source was just altered
    static func codeFragIsDetached(_ codeFrag: CodeFrag) -> Bool {
        return codeFrag.region == nil && !codeFrag.isBuiltin
    }
}
