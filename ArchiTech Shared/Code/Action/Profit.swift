//
//  Profit.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Profit {
    static func of(codeFrag: CodeFrag, in code: Code) -> Int {
        let plain = plainOf(codeFrag: codeFrag)
        let dependentBonus = code.frags.map {
            Dependency.codeFrag($0, dependsOn: codeFrag.id, context: code.iddFrags)
        }.count * Constants.Profit.CodeFrag.perDependent
        let dupReduction = 1 - code.duplicates.filter {
            $0.targets.contains(codeFrag.id)
        }.map { $0.shared }.reduce(0, +) //Could do .reduce(1, -) instead
        return Int(Float(plain + dependentBonus) * dupReduction)
    }
    
    private static func plainOf(codeFrag: CodeFrag) -> Int {
        return Int(
            Float(codeFrag.docs.map(of).reduce(0, +) + codeFrag.tests.map(of).reduce(0, +)) *
            annotationMultiplierOf(codeFragContent: codeFrag.content)
        ) + of(codeFragContent: codeFrag.content)
    }
    
    private static func of(docFrag: DocFrag) -> Int {
        switch docFrag {
        case .description:
            return Constants.Profit.DocFrag.description
        case .parameters:
            return Constants.Profit.DocFrag.parameters
        case .returnAndThrow:
            return Constants.Profit.DocFrag.returnAndThrow
        case .warningOrConstraint:
            return Constants.Profit.DocFrag.warningOrConstraint
        }
    }
    
    private static func of(test: Test) -> Int {
        return Int(test.coverage * Float(Constants.Profit.Test.fullCoverage))
    }
    
    private static func annotationMultiplierOf(codeFragContent: CodeFrag.Content) -> Ratio {
        switch codeFragContent {
        case .type(_):
            return Constants.Profit.CodeFrag.Content.typeAnnotationMultiplier
        case .function(_):
            return Constants.Profit.CodeFrag.Content.functionAnnotationMultiplier
        }
    }
    
    private static func of(codeFragContent: CodeFrag.Content) -> Int {
        switch codeFragContent {
        case .type(let codeType):
            return of(codeType: codeType)
        case .function(let function):
            return of(functionBody: function.body)
        }
    }
    
    private static func of(codeType: CodeType) -> Int {
        switch codeType {
        case .generic(_, _):
            return Constants.Profit.CodeType.generic
        case .alias(_):
            return Constants.Profit.CodeType.alias
        case .compound(_):
            return Constants.Profit.CodeType.compound
        }
    }
    
    private static func of(functionBody: Function.Body) -> Int {
        return Int(Constants.Profit.Function.bodyMax / (functionBody.statements.count + 1))
    }
}
