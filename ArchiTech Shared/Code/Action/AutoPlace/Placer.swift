//
//  Placer.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/18/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol Placer {
    func place(structures: [Structure], delegate: PlacerDelegate)
}
