//
//  PlacerDelegate.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/18/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol PlacerDelegate {
    var tileGrid: TileGrid { get }
    
    func place(structure: Structure)
    func place(tile: Tile, at location: TileLocation)
    ///Sometimes called while placing, for bufferring and/or cool animations
    func pause()
}
