//
//  VoronoiPlacer.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/18/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import GameplayKit

class VoronoiPlacer: Placer {
    private static let noiseFrequency: Double = 0.125

    private static func createSource() -> GKVoronoiNoiseSource {
        return GKVoronoiNoiseSource(
            frequency: VoronoiPlacer.noiseFrequency,
            displacement: 1,
            distanceEnabled: false,
            seed: Int32.random(in: Int32.min...Int32.max)
        )
    }
    
    let noise: GKNoise
    
    init() {
        let noiseSource = VoronoiPlacer.createSource()
        noise = GKNoise(noiseSource)
    }
    
    func place(structures: [Structure], delegate: PlacerDelegate) {
        print("Starting to place tiles...")
        var currentSize: Int = 32
        var noiseMap = getNoiseMap(size: currentSize)
        var unplacedTypes = structures.map { $0.tile }.filter { $0.isBase }
        var placedTypes: [Float:Tile] = [:]
        var placedAType = false
        for i in 0... {
            placedAType = false
            if i >= currentSize {
                currentSize *= 2
                noiseMap = getNoiseMap(size: currentSize)
            }
            
            for coords in IntVector.boxBorder(halfSize: i) {
                for coord in coords {
                    let loc = TileLocation(layer: .regular, position: coord)
                    let noiseValue = noiseMap.value(at: int2(coord))
                    guard let typeTile = placedTypes[noiseValue] ?? { () -> Tile? in
                        if unplacedTypes.isEmpty {
                            return nil
                        }
                        
                        let nextType = unplacedTypes.removeFirst()
                        var nextPlacedType = nextType
                        nextPlacedType.plot.decorations = Decoration.OptCornerMap { _ in nil }
                        placedAType = true
                        placedTypes[noiseValue] = nextPlacedType
                        return nextType
                    }() else {
                        continue
                    }
                    
                    delegate.place(tile: typeTile, at: loc)
                }
                delegate.pause()
            }
            
            if unplacedTypes.isEmpty && !placedAType {
                break
            }
        }
        print("Done placing tiles.")
    }
    
    private func getNoiseMap(size: Int) -> GKNoiseMap {
        return GKNoiseMap(
            noise,
            size: double2(x: Double(size), y: Double(size)),
            origin: double2(),
            sampleCount: int2(x: size, y: size),
            seamless: false
        )
    }
}
