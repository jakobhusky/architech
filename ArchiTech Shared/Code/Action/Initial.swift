//
//  Initial.swift
//  ArchiTech
//
//  Created by Jakob Hain on 10/20/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct Initial {
    static let tileGrid: TileGrid = TileGrid(tiles: [:])

    static func game(code: Code, context: World.Context) -> Game {
        return Game(
            code: code,
            world: world(code: code, context: context),
            selectionId: nil,
            placingStructureId: nil
        )
    }

    private static func world(code: Code, context: World.Context) -> World {
        return World(
            structures: code.frags.compactMap {
                structure(
                    codeFrag: $0,
                    code: code,
                    context: context.tile
                )
            },
            tileGrid: tileGrid
        )
    }

    private static func structure(codeFrag: CodeFrag, code: Code, context: Tile.Context) -> Structure? {
        guard let tile = Initial.tile(codeFrag: codeFrag, code: code, context: context) else {
            return nil
        }

        return Structure(codeFrag: codeFrag, tile: tile, location: nil)
    }

    private static func tile(codeFrag: CodeFrag, code: Code, context: Tile.Context) -> Tile? {
        guard let plot = plot(
            codeFrag: codeFrag,
            code: code,
            context: (context.plots, context.decorations)
        ) else {
            return nil
        }

        return Tile(
            building: building(codeFrag: codeFrag, plot: plot, context: context.buildings),
            road: nil,
            plot: plot
        )
    }

    private static func plot(
        codeFrag: CodeFrag,
        code: Code,
        context: (plots: [String:Plot.Info], decorations: [String:Decoration.Info])
    ) -> Plot? {
        switch codeFrag.content {
        case .function(let function):
            return plot(signatureType: function.signature.returnType, code: code, context: context)
        case .type(_):
            return plot(typeLabeled: codeFrag.label, context: context)
        }
    }

    private static func plot(
        signatureType: SignatureType,
        code: Code,
        context: (plots: [String:Plot.Info], decorations: [String:Decoration.Info])
    ) -> Plot? {
        switch signatureType {
        case .concrete(let type):
            return plot(
                codeFrag: code.iddFrags[type] ?? {
                    print("ERROR: No idd frag for type: \(type)")
                    return CodeFrag.unknownType
                }(),
                code: code,
                context: context
            )
        case .variable(let label):
            return plot(typeVariable: label, context: context)
        case .generic(let base, _):
            return plot(signatureType: base, code: code, context: context)
        }
    }

    private static func plot(
        typeLabeled label: Label,
        context: (plots: [String:Plot.Info], decorations: [String:Decoration.Info])
    ) -> Plot {
        var random = Random(seed: Seed(label))
        return plot(id: label.id, random: &random, kind: .normal, context: context)
    }

    private static func plot(
        typeVariable: String,
        context: (plots: [String:Plot.Info], decorations: [String:Decoration.Info])
    ) -> Plot {
        var random = Random(seed: Seed(typeVariable))
        return plot(id: nil, random: &random, kind: .wildcard, context: context)
    }

    private static func plot(
        id: Identifier?,
        random: inout Random,
        kind: Plot.Kind,
        context: (plots: [String:Plot.Info], decorations: [String:Decoration.Info])
    ) -> Plot {
        let possibleTypes = context.plots.filter { $0.value.kind == kind }.sorted { $0.key < $1.key }
        let (key: plotType, value: plotInfo) = random.nextElem(possibleTypes)
        return Plot(
            id: id,
            type: plotType,
            color: plotInfo.color(alter: random.nextColorAlter()),
            decorations: decorations(random: &random, plotType: plotType, context: context.decorations)
        )
    }
    
    private static func decorations(
        random: inout Random,
        plotType: String,
        context: [String:Decoration.Info]
    ) -> Decoration.OptCornerMap {
        return Decoration.OptCornerMap {
            decoration(random: &random, corner: $0, plotType: plotType, context: context)
        }
    }
        
    private static func decoration(
        random: inout Random,
        corner: Corner,
        plotType: String,
        context: [String:Decoration.Info]
    ) -> Decoration? {
        if (random.nextRatio() > Constants.Decoration.generalShownRatios[corner]) {
            return nil
        }
        
        let possibleTypes = context.filter {
            $0.value.corners.contains(corner) && $0.value.plotTypes.contains(plotType)
        }.sorted { $0.key < $1.key }
        if possibleTypes.isEmpty {
            return nil
        }
        
        let (key: decorationType, value: decorationInfo) = random.nextElem(possibleTypes)
        return Decoration(
            type: decorationType,
            variety: decorationInfo.numVarieties.map { random.nextInt(upperBound: $0) }
        )
    }

    private static func building(codeFrag: CodeFrag, plot: Plot, context: [String:Building.Info]) -> Building? {
        switch codeFrag.content {
        case .function(_):
            return building(functionLabeled: codeFrag.label, plot: plot, context: context)
        case .type(_):
            return nil
        }
    }

    private static func building(
        functionLabeled label: Label,
        plot: Plot,
        context: [String:Building.Info]
    ) -> Building {
        var random = Random(seed: Seed(label))
        let possibleBuildings = context.filter { $0.value.plotType == plot.type }.keys.sorted()
        return Building(
            type: random.nextElem(possibleBuildings)
        )
    }
}
