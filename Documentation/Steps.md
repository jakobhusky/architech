1. Implement code model ✓
2. Implement physical model ✓
3. Implement code / physical connection ✓
4. Implement placement
5. Implement tiles
6. Implement views
7. Implement controller
   - Ability to edit code model (currently JSON text editor)
   - Stores unplaced structures and places
   - Ability to place a structure, picking a location and roads, and if
     it's valid removes the unplaced structure and adds a place
   - Ability to remove a place, adding back the unplaced structure
   - Ability to set a structure's mold to one of the possibilities
   - Ability to see details about a structure
