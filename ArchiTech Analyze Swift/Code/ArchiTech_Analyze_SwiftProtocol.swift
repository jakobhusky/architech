//
//  ArchiTech_Analyze_SwiftProtocol.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/3/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// The protocol that this service will vend as its API. This header file will also need to be visible to the process hosting the service.
@objc protocol ArchiTech_Analyze_SwiftProtocol {
    func analyze(url: URL, withReply reply: @escaping (ArchiTech_Analyze_SwiftResult.ObjC) -> Void)
}

/*
 To use the service from an application or other process, use NSXPCConnection to establish a connection to the service by doing something like this:

     _connectionToService = [[NSXPCConnection alloc] initWithServiceName:@"com.jakobeha.ArchiTech-Analyze-Swift"];
     _connectionToService.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(ArchiTech_Analyze_SwiftProtocol)];
     [_connectionToService resume];

Once you have a connection to the service, you can use it like this:

     [[_connectionToService remoteObjectProxy] upperCaseString:@"hello" withReply:^(NSString *aString) {
         // We have received a response. Update our text field, but do it on the main thread.
         NSLog(@"Result string was: %@", aString);
     }];

 And, when you are finished with the service, clean up the connection like this:

     [_connectionToService invalidate];
*/
