//
//  main.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/3/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

// Create the delegate for the service.
let delegate = ServiceDelegate()

// Set up the one NSXPCListener for this service. It will handle all incoming connections.
let listener = NSXPCListener.service()
listener.delegate = delegate

// Resuming the serviceListener starts this service. This method does not return.
listener.resume()
