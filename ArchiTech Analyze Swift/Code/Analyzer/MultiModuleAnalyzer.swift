//
//  MultiModuleAnalyzer.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/9/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class MultiModuleAnalyzer {
    static let coreModule: String = "core"
    
    static func analyze(imported: Code, subAnalyzers: [Analyzer]) throws -> Code {
        let analyzer = try MultiModuleAnalyzer(subAnalyzers: subAnalyzers)
        try analyzer.importCode(imported)
        try analyzer.findDeclarations()
        try analyzer.findCode()
        return analyzer.code
    }
    
    let subAnalyzers: [Analyzer]
    let imports: [String]
    private(set) var declarations: [String?:[String:Label]]
    private(set) var code: Code
    
    init(subAnalyzers: [Analyzer]) throws {
        var imports: [String] = []
        for subAnalyzer in subAnalyzers {
            try subAnalyzer.findImports()
            imports += subAnalyzer.imports
        }
        self.imports = imports
        //Topological sort - TODO is there an O(n) algorithm?
        self.subAnalyzers = subAnalyzers.sorted { (lhs, rhs) in
            return lhs.context.module == nil || (
                rhs.context.module != nil &&
                !lhs.imports.contains(rhs.context.module!)
            )
        }
        declarations = [:]
        code = Code.empty
    }
    
    func importCode(_ code: Code) throws {
        self.code += code
        for subAnalyzer in subAnalyzers {
            try subAnalyzer.importCode(code)
        }
    }
    
    func findDeclarations() throws {
        for subAnalyzer in subAnalyzers {
            for subImport in [nil, MultiModuleAnalyzer.coreModule] + Array(subAnalyzer.imports) as [String?] {
                if let importedDeclarations = declarations[subImport] {
                    try mergeDeclarationsWithOverload(
                        &subAnalyzer.declarations,
                        new: importedDeclarations
                    )
                }
            }
            try subAnalyzer.findDeclarations()
        }
    }
    
    func findCode() throws {
        for subAnalyzer in subAnalyzers {
            try subAnalyzer.findCode()
            code += subAnalyzer.code
        }
    }
}
