//
//  DirectoryAnalyzer.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/8/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class DirectoryAnalyzer: Analyzer {
    let context: AnalyzerContext
    let url: URL
    let files: [FileAnalyzer]
    private(set) var imports: Set<String>
    var declarations: [String:Label] {
        didSet {
            for file in files {
                file.declarations = declarations
            }
        }
    }
    private(set) var code: Code
    
    required init(context: AnalyzerContext, url: URL) throws {
        self.context = context
        self.url = url
        //Enumerate files from https://stackoverflow.com/questions/25285016/iterate-through-files-in-a-folder-and-its-subfolders-using-swifts-filemanager
        var enumError: Error? = nil
        let children = FileManager.default.enumerator(
            at: url,
            includingPropertiesForKeys: [.isDirectoryKey],
            options: [],
            errorHandler: {
                enumError = $1
                return false
            }
        )!
        if let enumError = enumError {
            throw enumError
        }
        files = try children.map { $0 as! URL }.filter {
            try $0.pathExtension == "swift" &&
            !$0.resourceValues(forKeys: [.isDirectoryKey]).isDirectory!
        }.map { try FileAnalyzer(context: context, url: $0) }
        imports = []
        declarations = [:]
        code = Code.empty
    }
    
    func importCode(_ code: Code) throws {
        self.code += code
        for frag in code.frags {
            if let name = frag.label.name {
                if declarations[name] == nil {
                    declarations[name] = frag.label
                }
            }
        }
    }
    
    func findImports() throws {
        for file in files {
            try file.findImports()
            imports.formUnion(file.imports)
        }
    }
    
    func findDeclarations() throws {
        for file in files {
            try file.findDeclarations()
            try mergeDeclarationsWithOverload(&declarations, new: file.declarations)
        }
    }
    
    func findCode() throws {
        for file in files {
            try file.findCode()
            code += file.code
        }
    }
}
