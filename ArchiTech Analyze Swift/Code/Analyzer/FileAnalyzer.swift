//
//  FileAnalyzer.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/8/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SourceKittenFramework

class FileAnalyzer: Analyzer {
    let context: AnalyzerContext
    let url: URL
    let text: String
    let lines: [Substring]
    let structure: Structure
    private(set) var imports: Set<String>
    var declarations: [String:Label]
    private var declarationOverloadCount: [String:Int]
    private(set) var code: Code
    
    required init(context: AnalyzerContext, url: URL) throws {
        self.context = context
        self.url = url
        text = try String(contentsOf: url)
        lines = text.split(separator: "\n")
        structure = try Structure(file: File(contents: text))
        imports = []
        declarations = [:]
        declarationOverloadCount = [:]
        code = Code.empty
    }
    
    func importCode(_ code: Code) throws {
        self.code += code
        for frag in code.frags {
            if let name = frag.label.name {
                if declarations[name] == nil {
                    declarations[name] = frag.label
                }
            }
        }
    }
    
    func findImports() throws {
        //TODO Better way to get imports
        imports = Set(text.matchingStrings(regex: "^import (\\w+)", options: [.anchorsMatchLines]).map { $0[1] })
    }
    
    func findDeclarations() throws {
        for newDeclarations in try structure.getSubstructure().map({
            try analyzeRecDeclarations(dict: $0, parents: [])
        }) {
            try mergeDeclarationsWithOverload(&declarations, new: newDeclarations)
        }
    }
    
    func findCode() throws {
        code.frags.formUnion(try structure.getSubstructure().flatMap({
            try analyzeRecFrags(dict: $0, parents: [])
        }))
    }
    
    private func analyzeRecDeclarations(dict: SourceKitDictionary, parents: [String]) throws -> [String:Label] {
        var declarations: [String:Label] = [:]
        if let (indivName, indivLabel) = try analyzeIndivDeclaration(dict: dict, parents: parents) {
            declarations[indivName] = indivLabel
        }
        try mergeDeclarationsWithOverload(
            &declarations,
            new: try analyzeSpecificSubDeclarations(dict: dict, parents: parents)
        )
        if let indivName = try dict.getOptName(),
            try containsGeneralSubDeclarations(dict: dict) {
            let subParents = parents + [indivName]
            for newDeclarations in try dict.getSubstructure().map({
                try analyzeRecDeclarations(dict: $0, parents: subParents)
            }) {
                try mergeDeclarationsWithOverload(&declarations, new: newDeclarations)
            }
        }
        return declarations
    }
    
    private func analyzeSpecificSubDeclarations(
        dict: SourceKitDictionary,
        parents: [String]
    ) throws -> [String:Label] {
        switch try dict.getKind() {
        case .decl(.enum):
            return try analyzeEnumSubDeclarations(dict: dict, parents: parents)
        case .decl(.class),
             .decl(.struct),
             .decl(.protocol),
             .decl(.extension),
             .decl(.typealias),
             .decl(.associatedtype),
             .decl(.enumCase),
             .decl(.enumElement),
             .decl(.genericTypeParam),
             .decl(.var(_)),
             .decl(.function(_)),
             .elem(_),
             .expr(_),
             .stmt(_),
             .structure(_),
             .syntaxType(_),
             .import(_):
            return [:]
        }
    }
    
    private func containsGeneralSubDeclarations(dict: SourceKitDictionary) throws -> Bool {
        switch try dict.getKind() {
        case .decl(.class),
             .decl(.struct),
             .decl(.protocol),
             .decl(.extension),
             .decl(.enum):
            return true
        case .decl(.typealias),
             .decl(.associatedtype),
             .decl(.enumCase),
             .decl(.enumElement),
             .decl(.genericTypeParam),
             .decl(.var(_)),
             .decl(.function(_)),
             .elem(_),
             .expr(_),
             .stmt(_),
             .structure(_),
             .syntaxType(_),
             .import(_):
            return false
        }
    }
    
    private func analyzeRecFrags(dict: SourceKitDictionary, parents: [String]) throws -> [CodeFrag] {
        var frags: [CodeFrag] = []
        if let indivFrag = try analyzeIndivFrag(dict: dict, parents: parents) {
            frags.append(indivFrag)
        }
        frags += try analyzeSpecificSubFrags(dict: dict, parents: parents)
        if let indivName = try dict.getOptName() {
            let subParents = parents + [indivName]
            frags += try dict.getSubstructure().flatMap {
                try analyzeRecFrags(dict: $0, parents: subParents)
            }
        }
        return frags
    }
    
    private func analyzeIndivDeclaration(dict: SourceKitDictionary, parents: [String]) throws -> (String, Label)? {
        guard try dict.getKind() != .decl(.extension),
            let localName = try dict.getOptName() else {
            return nil
        }
        
        let name = globalizeName(local: localName, parents: parents)
        return (name, analyzeNewLabel(name: name))
    }
    
    private func analyzeIndivFrag(dict: SourceKitDictionary, parents: [String]) throws -> CodeFrag? {
        guard let content = try analyzeContent(dict: dict, parents: parents) else {
            return nil
        }
        
        let name = globalizeName(local: try dict.getName(), parents: parents)
        return try CodeFrag(
            label: analyzeExistingLabel(name: name),
            region: analyzeRegion(dict: dict),
            docs: [], //TODO
            tests: [], //TODO
            isBuiltin: context.isBuiltin,
            content: content
        )
    }
    
    private func analyzeSpecificSubFrags(dict: SourceKitDictionary, parents: [String]) throws -> [CodeFrag] {
        switch try dict.getKind() {
        case .decl(.enum):
            return try analyzeEnumSubFrags(dict: dict, parents: parents)
        case .decl(.class),
             .decl(.struct),
             .decl(.protocol),
             .decl(.extension),
             .decl(.typealias),
             .decl(.associatedtype),
             .decl(.enumCase),
             .decl(.enumElement),
             .decl(.genericTypeParam),
             .decl(.var(_)),
             .decl(.function(_)),
             .elem(_),
             .expr(_),
             .stmt(_),
             .structure(_),
             .syntaxType(_),
             .import(_):
            return []
        }
    }
    
    private func analyzeNewLabel(name: String) -> Label {
        return Label(
            id: Identifier(),
            name: name,
            context: context.module
        )
    }
    
    private func analyzeExistingLabel(name: String) -> Label {
        if let baseLabel = declarations[name] {
            return baseLabel
        } else {
            let overloadIndex = (declarationOverloadCount[name] ?? 0) + 1
            if let overloadLabel = declarations[getOverloadName(base: name, index: overloadIndex)] {
                declarationOverloadCount[name] = overloadIndex
                return overloadLabel
            } else {
                print("Enountered unknown label: \(name)")
                let label = Label(
                    id: Identifier(),
                    name: name,
                    context: Label.unknownContext
                )
                declarations[name] = label
                return label
            }
        }
    }
    
    private func analyzeRegion(dict: SourceKitDictionary) throws -> SourceRegion {
        let startIdx = try dict.getOffset()
        let endIdx = try startIdx + dict.getLength()
        return SourceRegion(
            ranges: [SourceRange(
                fileUrl: url,
                start: try analyzePosition(index: startIdx),
                end: try analyzePosition(index: endIdx)
            )]
        )
    }
    
    private func analyzePosition(index: Int) throws -> SourcePos {
        if (index < text.count) {
            let strIndex = text.index(text.startIndex, offsetBy: index)
            let lineIdx = Array(lines.enumerated()).firstIndex { entry in
                let (element: line, offset: lineIdx) = entry
                let localIdx = lines[0..<lineIdx].map { $0.count }.reduce(0, +)
                return localIdx < line.count
            }!
            let line = lines[lineIdx]
            return SourcePos(
                line: lineIdx,
                column: text.distance(from: line.startIndex, to: strIndex),
                index: index
            )
        } else {
            return SourcePos(
                line: -1,
                column: -1,
                index: index
            )
        }
    }
    
    private func analyzeContent(dict: SourceKitDictionary, parents: [String]) throws -> CodeFrag.Content? {
        switch try dict.getKind() {
        case .decl(.class),
             .decl(.struct),
             .decl(.protocol),
             .decl(.extension):
            return .type(try analyzeClass(dict: dict))
        case .decl(.enum):
            return .type(try analyzeEnum(dict: dict))
        case .decl(.typealias):
            return .type(try analyzeTypealias(dict: dict))
        case .decl(.associatedtype):
            return .type(.alias(base: nil))
        case .decl(.function(.free)),
             .decl(.function(.method(.class))),
             .decl(.function(.method(.static))):
            return .function(try analyzeStaticFunction(dict: dict))
        case .decl(.function(.subscript)),
             .decl(.function(.method(.instance))):
            return .function(try analyzeMethod(dict: dict, parents: parents))
        case .decl(.enumCase),
             .decl(.enumElement),
             .decl(.genericTypeParam),
             .decl(.var(_)),
             .elem(_),
             .expr(_),
             .stmt(_),
             .structure(_),
             .syntaxType(_),
             .import(_):
            return nil
        }
    }
    
    private func analyzeClass(dict: SourceKitDictionary) throws -> CodeType {
        return try .compound(
            type: .record,
            parts: dict.getInstanceVars().map(analyzeField),
            parents: dict.getInheritedTypes().map {
                try analyzeReference(dict: $0) ?? CodeFrag.unknownType.id
            }
        )
    }
    
    private func analyzeEnum(dict: SourceKitDictionary) throws -> CodeType {
        return try .compound(
            type: .variant,
            parts: dict.getEnumCases().map(analyzeField),
            parents: dict.getInheritedTypes().map {
                try analyzeReference(dict: $0) ?? CodeFrag.unknownType.id
            }
        )
    }
    
    private func analyzeTypealias(dict: SourceKitDictionary) throws -> CodeType {
        return .alias(base: CodeFrag.unknownType.id) //TODO Try to find typealias value
    }
    
    private func analyzeEnumSubDeclarations(
        dict: SourceKitDictionary,
        parents: [String]
    ) throws -> [String:Label] {
        let enumName = globalizeName(local: try dict.getName(), parents: parents)
        return try [String:Label](
            dict.getEnumCases().enumerated().map {
                try analyzeEnumCaseAsDeclaration(dict: $0.element, offset: $0.offset, enumName: enumName)
            },
            uniquingKeysWith: { throw AnalyzerFormatError.ambiguousName($0, $1) }
        )
    }
    
    private func analyzeEnumSubFrags(dict: SourceKitDictionary, parents: [String]) throws -> [CodeFrag] {
        let enumName = globalizeName(local: try dict.getName(), parents: parents)
        return try dict.getEnumCases().enumerated().map {
            try analyzeEnumCaseAsFrag(dict: $0.element, offset: $0.offset, enumName: enumName)
        }
    }

    private func analyzeStaticFunction(dict: SourceKitDictionary) throws -> Function {
        return try analyzeFunction(
            dict: dict,
            mainParams: [],
            customReturnType: nil
        )
    }
    
    private func analyzeMethod(dict: SourceKitDictionary, parents: [String]) throws -> Function {
        let instanceName = globalizeName(components: parents)
        let instance = SignatureType.concrete(type: try analyzeReference(typename: instanceName))
        let isConstructor = try dict.getOptName()?.starts(with: "init(") ?? false
        return try analyzeFunction(
            dict: dict,
            mainParams: isConstructor ? [] : [instance],
            customReturnType: isConstructor ? instance : nil
        )
    }

    private func analyzeFunction(
        dict: SourceKitDictionary,
        mainParams: [SignatureType],
        customReturnType: SignatureType?
    ) throws -> Function {
        return try Function(
            signature: Function.Signature(
                params: Function.Signature.Params(
                    main: [],
                    other: dict.getParameters().map(analyzeParameter)
                ),
                returnType: customReturnType ?? .concrete(
                    //TODO Manually parse return type, like how sourcery does it
                    type: dict.getOptTypename().map(analyzeReference) ??
                        AnalyzerBuiltin.voidTypeFrag.id
                )
            ),
            body: Function.Body(
                //TODO Analyze statements
                statements: []
            )
        )
    }
    
    private func analyzeField(dict: SourceKitDictionary) throws -> Field {
        return try Field(
            name: dict.getName(),
            type: dict.getOptTypename().map(analyzeReference) ?? CodeFrag.unknownType.id
        )
    }
    
    private func analyzeParameter(dict: SourceKitDictionary) throws -> SignatureType {
        return try .concrete(
            type: dict.getOptTypename().map {
                try analyzeReference(typename: extractParameterTypename($0))
            } ?? CodeFrag.unknownType.id
        )
    }
    
    private func analyzeEnumCaseAsDeclaration(
        dict: SourceKitDictionary,
        offset: Int,
        enumName: String
    ) throws -> (String, Label) {
        let name = getEnumCaseName(enumName: enumName, caseName: try dict.getOptName(), offset: offset)
        return (name, analyzeNewLabel(name: name))
    }
    
    private func analyzeEnumCaseAsFrag(
        dict: SourceKitDictionary,
        offset: Int,
        enumName: String
    ) throws -> CodeFrag {
        let name = getEnumCaseName(enumName: enumName, caseName: try dict.getOptName(), offset: offset)
        return try CodeFrag(
            label: analyzeExistingLabel(name: name),
            region: analyzeRegion(dict: dict),
            docs: [], //TODO
            tests: [], //TODO
            isBuiltin: context.isBuiltin,
            content: .type(.compound(
                type: .record,
                parts: dict.getEnumElements().map(analyzeField),
                parents: [declarations[enumName]!.id]
            ))
        )
    }
    
    private func analyzeReference(dict: SourceKitDictionary) throws -> Identifier? {
        return declarations[try dict.getName()]?.id
    }
    
    private func analyzeReference(typename: String) throws -> Identifier {
        return declarations[typename]?.id ?? CodeFrag.unknownType.id
    }
    
    private func globalizeName(local: String, parents: [String]) -> String {
        return globalizeName(components: parents + [local])
    }
    
    private func globalizeName(components: [String]) -> String {
        return components.joined(separator: ".")
    }
    
    private func getEnumCaseName(enumName: String, caseName: String?, offset: Int) -> String {
        return "\(enumName).\(caseName ?? String(offset))"
    }
    
    private func extractParameterTypename(_ typename: String) -> String {
        return
            typename.strip(prefix: "in ") ??
            typename.strip(prefix: "inout ") ??
            typename.strip(prefix: "@noescape ") ??
            typename.strip(prefix: "@escaping ") ??
            typename.strip(prefix: "@autoclosure ") ??
            typename
    }
}
