//
//  AnalyzerFormatError.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/8/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum AnalyzerFormatError: Error {
    case ambiguousName(Label, Label)
}
