//
//  AnalyzerContext.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/8/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct AnalyzerContext {
    static let local: AnalyzerContext = AnalyzerContext(
        module: nil,
        isBuiltin: false
    )
    
    let module: String?
    let isBuiltin: Bool
}
