//
//  AnalyzerBuiltin.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/8/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

struct AnalyzerBuiltin {
    static let code: Code = Code(
        frags: Set(allFrags),
        duplicates: []
    )
    static let allFrags: [CodeFrag] = createAllFrags(
        singleTypes: [
            typeFrag(name: "Optional", module: nil, type: .compound(
                type: .variant,
                parts: [
                    Field(name: "Optional.none", type: voidTypeFrag.id),
                    Field(name: "Optional.some", type: optionalWrappedTypeFrag.id)
                ],
                parents: []
            ))
        ],
        mutableBridges: [
            "String",
            "Array",
            "Dictionary",
            "Set",
            "Data"
        ],
        immutableBridges: [
            "Range",
            "Error",
            "URL",
            "URLRequest",
            "URLComponents",
            "URLQueryItem",
            "UUID",
            "Notification",
            "CharacterSet",
            "IndexPath",
            "IndexSet",
            "AffineTransform",
            "DateComponents",
            "DateInterval",
            "Calendar",
            "Date",
            "Locale",
            "TimeZone",
            "Measurement",
            "PersonNameComponents"
        ],
        otherCoreTypes: [
            "Any",
            "AnyObject",
            "AnyClass",
            "AnyHashable",
            "Bool",
            "Int",
            "Int8",
            "Int16",
            "Int32",
            "Int64",
            "UInt",
            "UInt8",
            "UInt16",
            "UInt32",
            "UInt64",
            "Float",
            "Float80",
            "Double",
            "Decimal",
            "Chararacter",
            "Unicode.Scalar.Properties",
            "StringProtocol",
            "String.UTF8View",
            "String.UTF16View",
            "String.UnicodeScalarView",
            "Substring",
            "UTF32.Parser",
            "UTF16.ReverseParser",
            "UTF16.CodeView",
            "UTF8.ReverseParser",
            "UTF8.ReverseParser",
            "Equatable",
            "Comparable",
            "Encodable",
            "Decodable",
            "Codable",
            "Hashable",
            "EncodingError",
            "DecodingError",
            "POSIXErrorCode",
            "MachErrorCode",
            "Progress",
            "MeasurementFormatter",
            "UndoManager",
            "FileManager",
            "UnsafeBufferPointer",
            "UnsafeMutableBufferPointer",
            "UnsafeMutableRawBufferPointer",
            "NSObject",
            "NSTextCheckingResult.CheckingType",
            "NSRectEdge",
            "CFError",
            "CGFloat",
            "CGRectEdge",
            "CVaListPointer",
            "CVarArg",
            "CBool",
            "CChar",
            "CChar16",
            "CChar32",
            "CDouble",
            "CLongDouble",
            "CFloat",
            "CInt",
            "CLong",
            "CLongLong",
            "CShort",
            "CSignedChar",
            "CUnsignedChar",
            "CUnsignedInt",
            "CUnsignedLong",
            "CUnsignedLongLong",
            "CUnsignedShort",
            "CWideChar",
            "_SwiftNewtypeWrapper",
            "__swift_stdlib_UErrorCode",
            "_HashTable.Bucket",
            "NSOrderedSet",
            "NSCoder",
            "NSKeyedArchiver",
            "NSKeyedUnarchiver",
            "NSItemProvider",
            "NSExpression",
            "NSSortDescriptor",
            "NSPredicate",
            "NSNumber",
            "NSEnumerator"
        ]
    )
    static let extraSpecificBuiltins: [CodeFrag] = [
        voidTypeFrag,
        optionalWrappedTypeFrag
    ]
    static let voidTypeFrag: CodeFrag = typeFrag(
        name: "Void",
        module: nil,
        type: CodeType.unit,
        stableIdentifier: Identifier(uuidString: "d094db3b-e1a7-4c65-ac46-3fe7e13aebcf")
    )
    static let optionalWrappedTypeFrag: CodeFrag = typeFrag(
        name: "Optional.Wrapped",
        module: nil,
        type: .alias(base: nil),
        stableIdentifier: Identifier(uuidString: "0cd2d119-96ef-4b1a-bdda-827c26f26d61")
    )
    
    private static let allBuiltinClassNames: [(String, String)] = {
        return []
        
        //From Martin R at https://stackoverflow.com/questions/19298553/get-list-of-all-native-classes
        var classNames: [(String, String)] = []
        
        var numClasses = Int(objc_getClassList(nil, 0))
        let ptr = UnsafeMutablePointer<AnyClass>.allocate(capacity: numClasses)
        let allClasses = AutoreleasingUnsafeMutablePointer<AnyClass>(ptr)
        numClasses = Int(objc_getClassList(allClasses, Int32(numClasses)))
        
        for i in 0 ..< numClasses {
            let curClass: AnyClass = allClasses[i]
            let className = NSStringFromClass(curClass)
            
            if className.hasPrefix("NS") {
                if className != "NSViewServiceApplication" && (
                    Bundle(for: curClass).bundleIdentifier == "com.apple.Foundation" ||
                    Bundle(for: curClass).bundleIdentifier == "com.apple.CoreFoundation"
                ) {
                    print(className)
                    classNames.append((className, "Foundation"))
                }
            } else if className.hasPrefix("CF") {
                print(className)
                classNames.append((className, "CoreFoundation"))
            } else if className.hasPrefix("CG") {
                print(className)
                classNames.append((className, "CoreGraphics"))
            } else if (
                !className.hasPrefix("_") &&
                !className.hasPrefix("OS_") &&
                Bundle(for: curClass).bundleIdentifier == nil
            ) {
                print(className)
                classNames.append((className, "nully"))
            }
        }
        
        ptr.deinitialize(count: numClasses)
        ptr.deallocate()
        print(classNames.count)
        return classNames
    }()
    
    private static func createAllFrags(
        singleTypes: [CodeFrag],
        mutableBridges: [String],
        immutableBridges: [String],
        otherCoreTypes: [String]
    ) -> [CodeFrag] {
        let bridgesSet = Set(
            mutableBridges.map { "NSMutable\($0)" } +
            (mutableBridges + immutableBridges).map { "NS\($0)" }
        )
        let otherBuiltinClasses = allBuiltinClassNames.filter { !bridgesSet.contains($0.0) }
        
        return
            CodeFrag.universalBuiltins +
            extraSpecificBuiltins +
            singleTypes +
            mutableBridges.flatMap({ bridgedTypeFrags(name: $0, mutable: true) }) +
            immutableBridges.flatMap({ bridgedTypeFrags(name: $0, mutable: false) }) +
            otherCoreTypes.map({ typeFrag(name: $0, module: nil, type: CodeType.primitive) }) +
            otherBuiltinClasses.map({ typeFrag(name: $0.0, module: $0.1, type: CodeType.primitive) })
    }

    static func typeFrag(
        name: String,
        module: String?,
        type: CodeType,
        stableIdentifier: Identifier? = nil
    ) -> CodeFrag {
        let identifier = stableIdentifier ?? Identifier()
        return CodeFrag(
            label: Label(id: identifier, name: name, context: module),
            region: nil,
            docs: [],
            tests: [],
            isBuiltin: true,
            content: .type(type)
        )
    }
    
    static func bridgedTypeFrags(name: String, mutable: Bool) -> [CodeFrag] {
        var frags: [CodeFrag] = []
        let base = typeFrag(name: name, module: nil, type: CodeType.primitive)
        frags.append(base)
        frags.append(foundationTypeFrag(name: name, bridge: base.id))
        if mutable {
            frags.append(foundationTypeFrag(name: "Mutable\(name)", bridge: base.id))
        }
        return frags
    }
    
    static func foundationTypeFrag(name: String, bridge: IdRef<CodeFrag>?) -> CodeFrag {
        return typeFrag(name: "NS\(name)", module: "Foundation", type: .alias(base: bridge))
    }

    static func coreGraphicsTypeFrag(name: String) -> CodeFrag {
        return typeFrag(name: "CG\(name)", module: "CoreGraphics", type: .alias(base: nil))
    }
}
