//
//  BaseAnalyzer.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/8/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

protocol Analyzer: AnyObject {
    var context: AnalyzerContext { get }
    var url: URL { get }
    var imports: Set<String> { get }
    var declarations: [String:Label] { get set }
    var code: Code { get }
    
    init(context: AnalyzerContext, url: URL) throws
    
    func importCode(_ code: Code) throws
    func findImports() throws
    func findDeclarations() throws
    func findCode() throws
}

extension Analyzer {
    static func analyze(imported: Code, context: AnalyzerContext, url: URL) throws -> Code {
        let analyzer = try Self(context: context, url: url)
        try analyzer.importCode(imported)
        try analyzer.findImports()
        try analyzer.findDeclarations()
        try analyzer.findCode()
        return analyzer.code
    }
}

func mergeDeclarationsWithOverload(
    _ declarations: inout [String:Label],
    new newDeclarations: [String:Label]
) throws {
    for (baseName, baseLabel) in newDeclarations {
        if var prevLabel = declarations[baseName] {
            if prevLabel.id != baseLabel.id {
                var nextLabel = baseLabel

                var overloadIndex = 1
                while declarations[getOverloadName(base: baseName, index: overloadIndex)] != nil {
                    overloadIndex += 1
                }
                
                let prevName = getOverloadName(base: baseName, index: overloadIndex)
                prevLabel.name = prevName
                overloadIndex += 1
                let nextName = getOverloadName(base: baseName, index: overloadIndex)
                nextLabel.name = nextName
                declarations[baseName] = nil
                declarations[prevName] = prevLabel
                declarations[nextName] = nextLabel
            }
        } else {
            declarations[baseName] = baseLabel
        }
    }
}

func getOverloadName(base: String, index: Int) -> String {
    return "\(base)+\(index)"
}
