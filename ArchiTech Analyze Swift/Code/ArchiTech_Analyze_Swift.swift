//
//  ArchiTech_Analyze_Swift.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/3/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

class ArchiTech_Analyze_Swift: ArchiTech_Analyze_SwiftProtocol {
    static let stdlibAnalysis: Code = {
        var cacheURL: URL! = nil
        do {
            cacheURL = try FileManager.default.url(
                for: .cachesDirectory,
                in: [.userDomainMask],
                appropriateFor: nil,
                create: true
            ).appendingPathComponent("ArchiTech/\(stdlibAnalysisSubpath)")
        } catch {
            print("Error getting cached stdlib URL, will generate and not cache: \(error)")
            return buildStdlibAnalysis()
        }
        
        if ProcessInfo.processInfo.environment["RECACHE"] == nil {
            do {
                let decoder = JSONDecoder()
                return try decoder.decode(Code.self, from: Data(contentsOf: cacheURL))
            } catch {
                print("Error reading cached stdlib, will generate and update cache: \(error)")
            }
        } else {
            print("Recache enabled, will generate and update cached stdlib")
        }
        
        let analysis = buildStdlibAnalysis()
        let encoder = JSONEncoder()
        do {
            try FileManager.default.createDirectory(
                at: cacheURL.deletingLastPathComponent(),
                withIntermediateDirectories: true,
                attributes: nil
            )
            try encoder.encode(analysis).write(to: cacheURL, options: [])
        } catch {
            print("Error writing cached stdlib: \(error)")
        }
        return analysis
    }()
    
    private static let stdlibAnalysisSubpath = "StdlibAnalysis/Swift.json"
    
    private static func buildStdlibAnalysis() -> Code {
        let stdlibUrl = Bundle(for: ArchiTech_Analyze_Swift.self).url(forResource: "Stdlib", withExtension: nil)!
        let moduleUrls = try! FileManager.default.contentsOfDirectory(
            at: stdlibUrl,
            includingPropertiesForKeys: [],
            options: FileManager.DirectoryEnumerationOptions.skipsSubdirectoryDescendants
        )
        
        let subAnalyzers: [DirectoryAnalyzer] = moduleUrls.map { symModuleUrl in
            let moduleUrl = symModuleUrl.resolvingSymlinksInPath()
            let module: String? = moduleUrl.lastPathComponent
            return try! DirectoryAnalyzer(
                context: AnalyzerContext(
                    module: module,
                    isBuiltin: true
                ), url: moduleUrl
            )
        }
        return try! MultiModuleAnalyzer.analyze(imported: AnalyzerBuiltin.code, subAnalyzers: subAnalyzers)
    }
    
    func analyze(url: URL, withReply reply: @escaping (ArchiTech_Analyze_SwiftResult.ObjC) -> Void) {
        let encoder = JSONEncoder()
        do {
            let res = try analyze(url: url)
            reply(ArchiTech_Analyze_SwiftResult.success(try! encoder.encode(res)).toObjC)
        } catch let error {
            reply(ArchiTech_Analyze_SwiftResult.fail(error).toObjC)
        }
    }
    
    private func analyze(url: URL) throws -> Code {
        return try DirectoryAnalyzer.analyze(
            imported: ArchiTech_Analyze_Swift.stdlibAnalysis,
            context: AnalyzerContext.local,
            url: url
        )
    }    
}
