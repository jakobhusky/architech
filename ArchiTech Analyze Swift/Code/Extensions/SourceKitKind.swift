//
//  SourceKitKind.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum SourceKitKind: RawRepresentable {
    enum Decl: RawRepresentable {
        enum Var: String {
            case instance
            case `class`
            case `static`
            case local
            case parameter
            case global
        }
        
        enum Function: RawRepresentable {
            enum Method: String {
                case instance
                case `class`
                case `static`
            }
            
            typealias RawValue = String
        
            case free
            case `subscript`
            case method(Method)
            
            var rawValue: String {
                switch self {
                case .free:
                    return "free"
                case .subscript:
                    return "subscript"
                case .method(let method):
                    return "method.\(method.rawValue)"
                }
            }
            
            init?(rawValue: String) {
                switch rawValue {
                case "free":
                    self = .free
                case "subscript":
                    self = .subscript
                default:
                    if let method = rawValue.strip(prefix: "method.") {
                        if let method = Method(rawValue: method) {
                            self = .method(method)
                        } else {
                            return nil
                        }
                    } else {
                        return nil
                    }
                }
            }
        }
        
        typealias RawValue = String
        
        case `class`
        case `struct`
        case `protocol`
        case `extension`
        case `enum`
        case `typealias`
        case `associatedtype`
        case enumCase
        case enumElement
        case genericTypeParam
        case `var`(Var)
        case function(Function)
        
        var rawValue: String {
            switch self {
            case .class:
                return "class"
            case .struct:
                return "struct"
            case .protocol:
                return "protocol"
            case .extension:
                return "extension"
            case .enum:
                return "enum"
            case .typealias:
                return "typealias"
            case .associatedtype:
                return "associatedtype"
            case .enumCase:
                return "enumcase"
            case .enumElement:
                return "enumelement"
            case .genericTypeParam:
                return "generic_type_param"
            case .var(let `var`):
                return "var.\(`var`.rawValue)"
            case .function(let function):
                return "function.\(function.rawValue)"
            }
        }
        
        init?(rawValue: String) {
            switch rawValue {
            case "class":
                self = .class
            case "struct":
                self = .struct
            case "protocol":
                self = .protocol
            case "extension":
                self = .extension
            case "enum":
                self = .enum
            case "typealias":
                self = .typealias
            case "associatedtype":
                self = .associatedtype
            case "enumcase":
                self = .enumCase
            case "enumelement":
                self = .enumElement
            case "generic_type_param":
                self = .genericTypeParam
            default:
                if let `var` = rawValue.strip(prefix: "var.") {
                    if let `var` = Var(rawValue: `var`) {
                        self = .var(`var`)
                    } else {
                        return nil
                    }
                } else if let function = rawValue.strip(prefix: "function.") {
                    if let function = Function(rawValue: function) {
                        self = .function(function)
                    } else {
                        return nil
                    }
                } else {
                    return nil
                }
            }
        }
    }
    
    enum Expr: String {
        case call
        case argument
        case tuple
        case array
        case dictionary
        case closure
    }
    
    enum Elem: String {
        case expr
    }
    
    enum Stmt: String {
        case `if`
        case `guard`
        case `switch`
        case `while`
        case repeatWhile = "repeatwhile"
        case `for`
        case foreach
        case brace
    }
    
    enum Structure: RawRepresentable {
        enum Elem: String {
            case typeref
        }
        
        case elem(Elem)
        
        var rawValue: String {
            switch self {
            case .elem(let elem):
                return "elem.\(elem.rawValue)"
            }
        }
        
        init?(rawValue: String) {
            if let elem = rawValue.strip(prefix: "elem.") {
                if let elem = Elem(rawValue: elem) {
                    self = .elem(elem)
                } else {
                    return nil
                }
            } else {
                return nil
            }
        }
    }
    
    enum SyntaxType: RawRepresentable {
        enum Comment: String {
            case mark
        }
        
        case comment(Comment)
        
        var rawValue: String {
            switch self {
            case .comment(let comment):
                return "comment.\(comment.rawValue)"
            }
        }
        
        init?(rawValue: String) {
            if let comment = rawValue.strip(prefix: "comment.") {
                if let comment = Comment(rawValue: comment) {
                    self = .comment(comment)
                } else {
                    return nil
                }
            } else {
                return nil
            }
        }
    }
    
    enum Import: RawRepresentable {
        enum Module: String {
            case swift
            case clang
        }
        
        case module(Module)
        
        var rawValue: String {
            switch self {
            case .module(let module):
                return "module.\(module.rawValue)"
            }
        }
        
        init?(rawValue: String) {
            if let module = rawValue.strip(prefix: "module.") {
                if let module = Module(rawValue: module) {
                    self = .module(module)
                } else {
                    return nil
                }
            } else {
                return nil
            }
        }
    }
    
    typealias RawValue = String

    case decl(Decl)
    case expr(Expr)
    case elem(Elem)
    case stmt(Stmt)
    case structure(Structure)
    case syntaxType(SyntaxType)
    case `import`(Import)

    var rawValue: String {
        switch self {
        case .decl(let decl):
            return "source.lang.swift.decl.\(decl.rawValue)"
        case .expr(let expr):
            return "source.lang.swift.expr.\(expr.rawValue)"
        case .elem(let elem):
            return "source.lang.swift.elem.\(elem.rawValue)"
        case .stmt(let stmt):
            return "source.lang.swift.stmt.\(stmt.rawValue)"
        case .structure(let structure):
            return "source.lang.swift.structure.\(structure.rawValue)"
        case .syntaxType(let syntaxType):
            return "source.lang.swift.syntaxtype.\(syntaxType.rawValue)"
        case .import(let `import`):
            return "source.lang.swift.import.\(`import`.rawValue)"
        }
    }
    
    init?(rawValue: String) {
        if let decl = rawValue.strip(prefix: "source.lang.swift.decl.") {
            if let decl = Decl(rawValue: decl) {
                self = .decl(decl)
            } else {
                return nil
            }
        } else if let expr = rawValue.strip(prefix: "source.lang.swift.expr.") {
            if let expr = Expr(rawValue: expr) {
                self = .expr(expr)
            } else {
                return nil
            }
        } else if let elem = rawValue.strip(prefix: "source.lang.swift.elem.") {
            if let elem = Elem(rawValue: elem) {
                self = .elem(elem)
            } else {
                return nil
            }
        } else if let stmt = rawValue.strip(prefix: "source.lang.swift.stmt.") {
            if let stmt = Stmt(rawValue: stmt) {
                self = .stmt(stmt)
            } else {
                return nil
            }
        } else if let structure = rawValue.strip(prefix: "source.lang.swift.structure.") {
            if let structure = Structure(rawValue: structure) {
                self = .structure(structure)
            } else {
                return nil
            }
        } else if let syntaxType = rawValue.strip(prefix: "source.lang.swift.syntaxtype.") {
            if let syntaxType = SyntaxType(rawValue: syntaxType) {
                self = .syntaxType(syntaxType)
            } else {
                return nil
            }
        } else if let `import` = rawValue.strip(prefix: "source.lang.swift.import.") {
            if let `import` = Import(rawValue: `import`) {
                self = .`import`(`import`)
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
}
