//
//  SourceKitAccessibility.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum SourceKitAccessibility: String {
    case `public` = "source.lang.swift.accessibility.public"
    case `internal` = "source.lang.swift.accessibility.internal"
    case `fileprivate` = "source.lang.swift.accessibility.fileprivate"
    case `private` = "source.lang.swift.accessibility.private"
}
