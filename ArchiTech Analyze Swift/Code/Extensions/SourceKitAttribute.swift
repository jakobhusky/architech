//
//  SourceKitAttribute.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SourceKittenFramework

typealias SourceKitAttribute = SwiftDeclarationAttributeKind
