//
//  Structure.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SourceKittenFramework

extension Structure {
    func getSubstructure() throws -> [SourceKitDictionary] {
        return try (dictionary["key.substructure"].map {
            try SourceKitFormatError.assertCast(
                key: "substructure",
                value: $0
            ) as [SourceKitRepresentable]
        } ?? []).map {
            try SourceKitFormatError.assertParse(
                key: SourceKitFormatError.arrayKey,
                value: $0
            )
        }
    }
}
