//
//  Request.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/9/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SourceKittenFramework

extension Request {
    static func index(url: URL, text: String) -> Request {
        return Request.customRequest(request: [
            "key.request": "source.request.indexsource" as UID,
            "key.compilerargs": [url.path] as [String],
            "key.sourcefile": url.path,
            "key.sourcetext": text
        ])
    }
    
    func sendAndParse() throws -> SourceKitDictionary {
        return try SourceKitFormatError.assertParse(key: SourceKitFormatError.rootKey, value: try send())
    }
}
