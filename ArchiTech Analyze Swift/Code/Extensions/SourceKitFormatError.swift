//
//  SourceKitFormatError.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SourceKittenFramework

enum SourceKitFormatError: Error {
    static let rootKey: String = "<root>"
    static let arrayKey: String = "<array item>"
    
    static func assertExists<T>(key: String, value: T?) throws -> T {
        if let value = value {
            return value
        } else {
            throw SourceKitFormatError.missingProperty(key: key)
        }
    }
    
    static func assertCast<T>(key: String, value: SourceKitRepresentable) throws -> T {
        if let value = value as? T {
            return value
        } else {
            throw SourceKitFormatError.badTypedProperty(key: key, expectedType: T.self, actualValue: value)
        }
    }
    
    static func assertParse<T: RawRepresentable, Prim>(
        key: String,
        value: SourceKitRepresentable
    ) throws -> T where T.RawValue == Prim {
        if let prim = value as? Prim,
            let value = T(rawValue: prim) {
            return value
        } else {
            throw SourceKitFormatError.badTypedProperty(key: key, expectedType: T.self, actualValue: value)
        }
    }
    
    static func assertExistsAndCast<T>(key: String, value: SourceKitRepresentable?) throws -> T {
        return try assertCast(key: key, value: try assertExists(key: key, value: value))
    }
    
    static func assertExistsAndParse<T: RawRepresentable, Prim>(
        key: String,
        value: SourceKitRepresentable?
    ) throws -> T where T.RawValue == Prim {
        return try assertParse(key: key, value: try assertExists(key: key, value: value))
    }
    
    case missingProperty(key: String)
    case badTypedProperty(key: String, expectedType: Any.Type, actualValue: SourceKitRepresentable)
}
