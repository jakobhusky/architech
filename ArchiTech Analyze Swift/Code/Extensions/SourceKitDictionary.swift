//
//  SourceKitDictionary.swift
//  ArchiTech
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import SourceKittenFramework

struct SourceKitDictionary: RawRepresentable {
    typealias RawValue = [String:SourceKitRepresentable]
    
    let rawValue: [String:SourceKitRepresentable]
    
    func getName() throws -> String {
        return try get("name")
    }
    
    func getOptName() throws -> String? {
        return try getOpt("name")
    }
    
    func getKind() throws -> SourceKitKind {
        return try getParsed("kind")
    }
    
    func getTypename() throws -> String {
        return try get("typename")
    }
    
    func getOptTypename() throws -> String? {
        return try getOpt("typename")
    }
    
    func getInheritedTypes() throws -> [SourceKitDictionary] {
        return try getParsedArray("inheritedTypes")
    }
    
    func getSubstructure() throws -> [SourceKitDictionary] {
        return try getParsedArray("substructure")
    }
    
    func getInstanceVars() throws -> [SourceKitDictionary] {
        return try getSubstructure().filter { try $0.getKind() == .decl(.var(.instance)) }
    }
    
    func getParameters() throws -> [SourceKitDictionary] {
        return try getSubstructure().filter { try $0.getKind() == .decl(.var(.parameter)) }
    }
    
    func getEnumCases() throws -> [SourceKitDictionary] {
        //TODO Fix by returning first enumelement if that's where the name is
        return try getSubstructure().filter { try $0.getKind() == .decl(.enumCase) && $0.getOptName() != nil }
    }
    
    func getEnumElements() throws -> [SourceKitDictionary] {
        return try getSubstructure().filter { try $0.getKind() == .decl(.enumElement) }
    }
    
    func getAccessibility() throws -> SourceKitAccessibility {
        return try getParsed("accessibility")
    }
    
    func getAttributes() throws -> [SourceKitAttribute] {
        return try getParsedArray("attributes")
    }
    
    func getOffset() throws -> Int {
        return Int(try get("offset") as Int64)
    }
    
    func getLength() throws -> Int {
        return Int(try get("length") as Int64)
    }
    
    func getDependencies() throws -> [SourceKitDictionary] {
        return try getParsedArray("dependencies")
    }
    
    func getImports() throws -> [String] {
        return try getDependencies().map { try $0.getName() }
    }
    
    private func get<T>(_ key: String) throws -> T {
         return try SourceKitFormatError.assertExistsAndCast(key: key, value: rawValue["key.\(key)"])
    }
    
    private func getParsed<T: RawRepresentable, Prim>(_ key: String) throws -> T where T.RawValue == Prim {
        return try SourceKitFormatError.assertExistsAndParse(key: key, value: rawValue["key.\(key)"])
    }
    
    private func getParsedArray<T: RawRepresentable, Prim>(_ key: String) throws -> [T] where T.RawValue == Prim {
        guard let value = rawValue["key.\(key)"] else {
            return []
        }
        
        return try (SourceKitFormatError.assertCast(key: key, value: value) as [SourceKitRepresentable]).map {
            try SourceKitFormatError.assertParse(
                key: SourceKitFormatError.arrayKey,
                value: $0
            )
        }
    }
    
    private func getOpt<T>(_ key: String) throws -> T? {
        return try rawValue["key.\(key)"].map { try SourceKitFormatError.assertCast(key: key, value: $0) }
    }
}
