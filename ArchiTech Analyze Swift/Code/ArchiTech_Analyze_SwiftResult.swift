//
//  ArchiTech_Analyze_SwiftResult.swift
//  ArchiTech Analyze Swift
//
//  Created by Jakob Hain on 12/6/18.
//  Copyright © 2018 Jakobeha. All rights reserved.
//

import Foundation

enum ArchiTech_Analyze_SwiftResult {
    typealias ObjC = NSObject
    
    case fail(Error)
    case success(Data)
    
    var toObjC: ObjC {
        switch self {
        case .fail(let error):
            return error as NSObject
        case .success(let data):
            return data as NSObject
        }
    }
    
    init(_ asObjC: ObjC) {
        if let data = asObjC as? Data {
            self = .success(data)
        } else if let error = asObjC as? Error {
            self = .fail(error)
        } else {
            fatalError("Invalid ObjC result: \(asObjC)")
        }
    }
}
