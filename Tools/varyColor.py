# From https://stackoverflow.com/questions/7274221/changing-image-hue-with-python-pil

import os
import re
import sys

import numpy
from PIL import Image


def rgb_to_hsv(rgb):
    # Translated from source of colorsys.rgb_to_hsv
    # r,g,b should be a numpy arrays with values between 0 and 255
    # rgb_to_hsv returns an array of floats between 0.0 and 1.0.
    rgb = rgb.astype('float')
    hsv = numpy.zeros_like(rgb)
    # in case an RGBA array was passed, just copy the A channel
    hsv[..., 3:] = rgb[..., 3:]
    r, g, b = rgb[..., 0], rgb[..., 1], rgb[..., 2]
    maxc = numpy.max(rgb[..., :3], axis=-1)
    minc = numpy.min(rgb[..., :3], axis=-1)
    hsv[..., 2] = maxc
    mask = maxc != minc
    hsv[mask, 1] = (maxc - minc)[mask] / maxc[mask]
    rc = numpy.zeros_like(r)
    gc = numpy.zeros_like(g)
    bc = numpy.zeros_like(b)
    rc[mask] = (maxc - r)[mask] / (maxc - minc)[mask]
    gc[mask] = (maxc - g)[mask] / (maxc - minc)[mask]
    bc[mask] = (maxc - b)[mask] / (maxc - minc)[mask]
    hsv[..., 0] = numpy.select(
        [r == maxc, g == maxc], [bc - gc, 2.0 + rc - bc], default=4.0 + gc - rc)
    hsv[..., 0] = (hsv[..., 0] / 6.0) % 1.0
    return hsv


def hsv_to_rgb(hsv):
    # Translated from source of colorsys.hsv_to_rgb
    # h,s should be a numpy arrays with values between 0.0 and 1.0
    # v should be a numpy array with values between 0.0 and 255.0
    # hsv_to_rgb returns an array of uints between 0 and 255.
    rgb = numpy.empty_like(hsv)
    rgb[..., 3:] = hsv[..., 3:]
    h, s, v = hsv[..., 0], hsv[..., 1], hsv[..., 2]
    i = (h * 6.0).astype('uint8')
    f = (h * 6.0) - i
    p = v * (1.0 - s)
    q = v * (1.0 - s * f)
    t = v * (1.0 - s * (1.0 - f))
    i = i % 6
    conditions = [s == 0.0, i == 1, i == 2, i == 3, i == 4, i == 5]
    rgb[..., 0] = numpy.select(conditions, [v, q, p, p, t, v], default=v)
    rgb[..., 1] = numpy.select(conditions, [v, v, v, q, p, p], default=t)
    rgb[..., 2] = numpy.select(conditions, [v, p, t, v, v, q], default=p)
    return rgb.astype('uint8')

HUE_TARGET_TOLERANCE = 0.0625

def shift_hue(img, hue_shift, hue_target):
    hsv = rgb_to_hsv(numpy.array(img))
    if hue_target != None:
        for x in range(len(hsv)):
            for y in range(len(hsv[x])):
                hue_offset = abs(hsv[x, y, 0] - hue_target)
                if hue_offset < HUE_TARGET_TOLERANCE:
                    hsv[x, y, 0] += hue_shift
    else:
        hsv[..., 0] += hue_shift
    rgb = hsv_to_rgb(hsv)
    return Image.fromarray(rgb, "RGBA")


def desaturate(img):
    hsv = rgb_to_hsv(numpy.array(img))
    hsv[..., 1] = 0
    rgb = hsv_to_rgb(hsv)
    return Image.fromarray(rgb, "RGBA")

PARAMS_REGEXP = re.compile(r"""
    ^(?P<name>\w+)\+
    (?:
        hue(?P<hue_count>[0-9]+)
        (?:-(?P<hue_limit>[0-9]+))?
        (?:@(?P<hue_target>[0-9]+))?
    )?
    (?P<grey>grey)?
    \.png$
""", re.VERBOSE)

if __name__ == '__main__':
    path = sys.argv[1]
    assert len(path) > 0
    for file_name in os.listdir(path):
        params = PARAMS_REGEXP.match(file_name)
        if params:
            name = params.group("name")
            num_hues = params.group("hue_count")
            if num_hues != None:
                num_hues = int(num_hues)
            hue_limit = params.group("hue_limit")
            if hue_limit != None:
                hue_limit = int(hue_limit)
            hue_target = params.group("hue_target")
            if hue_target != None:
                hue_target = float(hue_target) / 360.0
            grey = params.group("grey")

            old_path = os.path.join(path, file_name)
            old_image = Image.open(old_path).convert('RGBA')
            new_dir_path = os.path.join(path, name)
            if os.path.isdir(new_dir_path):
                for obsolete_file_name in os.listdir(new_dir_path):
                    obsolete_path = os.path.join(new_dir_path, obsolete_file_name)
                    os.remove(obsolete_path)
            else:
                os.mkdir(new_dir_path)

            new_images = []
            for i in range(num_hues):
                hue = float(i) / float(num_hues)
                if hue_limit:
                    hue = (hue - 0.5) / float(hue_limit)
                new_images.append(shift_hue(old_image, hue, hue_target))
            if grey:
                new_images.append(desaturate(old_image))
            for i in range(len(new_images)):
                new_image = new_images[i]
                new_file_name = "var" + str(i) + ".png"
                new_path = os.path.join(new_dir_path, new_file_name)
                new_image.save(new_path)
